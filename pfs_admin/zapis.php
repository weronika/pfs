<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'zapisy') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
include_once ('funkcje_zapisy.php');

$max_ilosc_ruchow = 25;
$id               = $_GET['id'];
$plansza_kolory   = array ();
$kolory           = array (
    red         => array (0, 7, 14, 105, 119, 210, 217, 224),
    lightred    => array (16, 28, 32, 42, 48, 56, 64, 70, 112, 154, 160, 168, 176, 182, 192, 196, 208),
    blue        => array (20, 24, 76, 80, 84, 88, 136, 140, 144, 148, 200, 204),
    lightblue   => array (3, 11, 36, 38, 45, 52, 59, 92, 96, 98, 102, 108, 116, 122, 126, 128, 132, 165, 172, 179, 186, 188, 213, 221),
);

foreach ($kolory as $kolor => $numery) {
    foreach ($numery as $numer) {
        $plansza_kolory[$numer] = $kolor;
    }
}

if ($_POST['plansza']){
    $plansza = aktualizujPlansze ($_POST['plansza'], $_POST['blank1'], $_POST['blank2'], $_POST['pole'], $_POST['uwagi']);
    $refresh = 1;
}

else if ($_POST['gospodarz']) {
    aktualizujGracza ($id, 1, $_POST['litery1'], $_POST['wyrazy1'], $_POST['punkty1'], $_POST['ruch1'], $_POST['suma1'], $max_ilosc_ruchow);
    $refresh = 1;
}

else if ($_POST['gosc']) {
    aktualizujGracza ($id, 2, $_POST['litery2'], $_POST['wyrazy2'], $_POST['punkty2'], $_POST['ruch2'], $_POST['suma2'], $max_ilosc_ruchow);
    $refresh = 1;
}

if ($refresh) pfs_refresh (array (id => $id));

$output = '';
$game   = pfs_select_one (array (
    table   => $DB_TABLES[games],
    where   => array ( id => $id )
));

$game->plansza = preg_split ('/(?<!^)(?!$)/u', $game->plansza);
?>

<html>
<head>
    <title>Zapis partii turniejowej</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css">
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <script>
        function navigation (e) {
            var focused = $(document.activeElement);
            row = $(focused).closest ('tr').index () - 1;
            col = $(focused).closest ('td').index () - 1;

            switch (e.keyCode) {
                case 8:  //backspace
                    if (focused.val () != '') {
                        focused.empty ();
                    }
                    else {
                        focused.closest ('td').prev ().children ('input').first (). empty () . focus ();
                    }
                    break;
                case 37:  //lewa
                    focused.closest ('td').prev ().children ('input').first (). focus ();
                    break;
                case 38:  //gora
                    focused.closest ('tr').prev ().children ('td:nth-child(' + (col + 2) + ')') .children ('input').first (). focus ();
                    break;
                case 39:  //prawa
                    focused.closest ('td').next ().children ('input').first (). focus ();
                    break;
                case 40:  //dol
                    focused.closest ('tr').next ().children ('td:nth-child(' + (col + 2) + ')') .children ('input').first (). focus ();
                    break;
                default:
                    break;
            }
        }

        function writing (e) {
            if (e.keyCode < 65 || e.keyCode > 90) {
                return;
            }

            var focused = document.activeElement;
            var row     = $(focused).closest ('tr').index () - 1;
            var col     = $(focused).closest ('td').index () - 1;
            var dir     = $('input[name="direction"]:checked').val ();

            if (dir == 'h') {
                $(focused).closest ('td').next ().children ('input').first (). focus ();
            }
            else if (dir == 'v') {
                $(focused).closest ('tr').next ().children ('td:nth-child(' + (col + 2) + ')') .children ('input').first (). focus ();
            }
        }

        $(document).ready (function () {
            $("input").attr ("autocomplete", "off");
            $('table#plansza input')
                .attr ('autocomplete', 'off')
                .bind ('keydown', navigation)
                .bind ('keyup', writing);
            $( ".button, ul.menu li a" ).button();
            $( "#direction" ).buttonset ();

            $( "#instrukcja" ).dialog({
                autoOpen:   false,
                show:       "blind",
                hide:       "explode",
                width:      '900px',
                title:      'Krótka instrukcja do zapisów partii',
                resizable: false
            });

            $( "#opener" ).click(function() {
                $( "#instrukcja" ).dialog( "open" );
                return false;
            });
        });
    </script>
</head>

<body>
<div id='header'>
    <h1>Zapis partii turniejowej - <? print $game->nazwa; ?></h1>
    <ul class='menu'>
<? if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='main.php'>Menu</a></li>"; } ?>
        <li><a href='zapisy_lista.php'>Plansze</a></li>
        <li><a href='http://www.pfs.org.pl/zapis.php?id=<? print $game->id; ?>' target="_blank">Strona planszy</a></li>
        <li><a id='opener'>Instrukcja</a></li>
    </ul>
</div>

<div id="content" style="width:1300px;">
    <div id='instrukcja' style='display:none;'>
        1. <b>BLANKI</b> należy wpisywać na planszy jako normalną literę, po czym oznaczyć nad planszą jego współrzędne (np. G8).<br>
        2. Aby <b>wykasować z planszy</b> mylnie pisaną literę (lub współrzędne blanka), należy w jej miejsce wpisać znak "-" (minus) i zapisać planszę.<br>
        3. Aby w kolumnę wyrazy wpisać coś małymi literami (np. strata, wymiana, pas) należy przed tekstem wstawić znak "-" (minus).<br>
        4. Blanki w zapisie należy oznaczać znakiem gwiazdki "*" (nie dotyczy to blanków na planszy).
    </div>

    <div class="kolumna" style="width:330px;">
        <form action='zapis.php?id=<? print $game->id; ?>' method='post'>
            <input type='hidden' name='gospodarz' value='<? print $game->id; ?>'>
            <?php tabelkaZapisu ($game->id, 1, $game->gospodarz, $max_ilosc_ruchow); ?>
        </form>
    </div>

    <div class='kolumna' style="width:640px;">
        <form action='zapis.php?id=<?php print $game->id; ?>' method='post'>
        <input type='hidden' name='plansza' value='<?php print $game->id; ?>'>
        <table class='zapis'>
            <tr>
                <th class='blanki'>Blanki:</th>
                <th>Kierunek wpisywania:</th>
            </tr>
            <tr>
                <td class='blanki'>
                    <input type='text' name='blank1' maxlength='3' class='medium' value='<?php print $game->blank1; ?>'>
                    <input type='text' name='blank2' maxlength='3' class='medium' value='<?php print $game->blank2; ?>'>
                </td>
                <td id='direction' style="text-align:center;padding-bottom:10px;">
                    <input type='radio' id='direction1' name='direction' value='h' class='radio' checked><label for='direction1'>poziomo</label>
                    <input type='radio' id='direction2' name='direction' value='v' class='radio'><label for='direction2'>pionowo</label>
                </td>
            </tr>
        </table>

        <div style='width:640px;margin:0 auto 8px auto;text-align:center;'>
            <input type='submit' value='Zapisz planszę' class='button'>
        </div>

        <table id='plansza'>
            <tr>
                <th>&nbsp;</th>
<?php
    for ($c = 1; $c < 16; $c++) {
        print '<th>' . $c . '</th>';
    }
    print '<th>&nbsp;</th></tr>';

    $bl1 = strlen($game->blank1) > 2
        ? (ord($game->blank1[0]) - 65) * 15 + intval($game->blank1[1].$game->blank1[2])
        : (ord($game->blank1[0]) - 65) * 15 + intval($game->blank1[1]);

    $bl2 = strlen($game->blank2) > 2
        ? (ord($game->blank2[0]) - 65) * 15 + intval($game->blank2[1].$game->blank2[2])
        : (ord($game->blank2[0]) - 65) * 15 + intval($game->blank2[1]);

    for ($col = 0; $col < 15; $col++) {
        $output .= '<tr><th>' . chr(65 + $col) . '</th>';

        for ($wiersz = 0; $wiersz < 15; $wiersz++) {
            $index   = $col * 15 + $wiersz;
            $output .= '<td class="' . $plansza_kolory[$index] . '">
                <input type="text" name="pole[]" maxlength="1" ';

            if ($game->plansza[$index] != '0') {
                $output .= 'value="' . ($game->plansza[$index]) . '" ';
            }

            if (($index + 1) == $bl1 || ($index + 1) == $bl2) {
                $output .= 'class="blank"';
            }

            $output .= '></td>';
        }
        $output .= '<th>&nbsp;</th></tr>';
    }

    print $output;

    print '<tr>';
    for ($c = 0; $c < 17; $c++) {
        print '<th>&nbsp;</th>';
    }
    print '</tr>';
?>
        </table>
        <textarea name="uwagi" style="width:600px;margin:30px 20px;" rows="8"><? print $game->uwagi; ?></textarea>
        </form>
    </div>

    <div class='kolumna' style="width:320px;">
        <form action='zapis.php?id=<?php echo $id; ?>' method='post'>
            <input type='hidden' name='gosc' value='<?echo $id;?>'>
            <?php tabelkaZapisu ($game->id, 2, $game->gosc, $max_ilosc_ruchow); ?>
        </form>
    </div>
</div>
</body>
</html>
