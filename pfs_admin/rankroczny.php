<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

if ($_GET['delete']) {
    $data = array (
        'osoba'     => '',
        'skalpy'    => 0,
        'partie'    => 0,
        'ranking'   => 0
    );

    pfs_update ($DB_TABLES[year_rank], $data, array ('id' => $_GET['delete']));
    pfs_refresh ();
}

else if (isset ($_POST['change'])){
    $data = array (
        'osoba'     => $_POST['osoba'],
        'skalpy'    => $_POST['skalpy'],
        'partie'    => $_POST['partie'],
        'ranking'   => round (floatval ($_POST['skalpy']) / floatval ($_POST['partie']), 2)
    );

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES[year_rank], $data)
        : pfs_update ($DB_TABLES[year_rank], $data, array ('id' => $_POST['change']));
}
?>
<html>
<head>
    <title>Ranking roczny</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Ranking roczny</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
    </ul>
</div>

<div id="content">
    <table style="width:600px;">
       <tr>
           <th></td>
           <th class='person'>Imię i nazwisko</td>
           <th>Ranking</td>
           <th>Skalpy</td>
           <th>Partie</td>
           <th class='icons'></td>
       </tr>
<?
$lp   = 1;
$rows = pfs_select (array (
    table   => $DB_TABLES[year_rank],
    order   => array ( '!ranking' )
));

foreach ($rows as $row) {
    if ($_GET['edit'] && $_GET['edit'] == $row->id) {
        print "
        <form action='rankroczny.php' method='post'>
            <input type='hidden' name='change' value='".$row->id."'>
            <tr>
                <td class='lp'>" . $lp++ . "</td>
                <td class='person'><input type='text' name='osoba' size='20' value='".$row->osoba."'></td>
                <td>".$row->ranking."</td>
                <td><input type='text' name='skalpy' size='4' value='".$row->skalpy."'></td>
                <td><input type='text' name='partie' size='2' value='".$row->partie."'></td>
                <td class='icons'><input type='submit' value='Zapisz' class='button' /></td>
            </tr>
        </form>";
    }
    else {
        print "
            <tr>
                <td class='lp'>" . $lp++ . "</td>
                <td class='person'>".$row->osoba."</td>
                <td>".$row->ranking."</td>
                <td>".$row->skalpy."</td>
                <td>".$row->partie."</td>
                <td class='icons'>
                    <a href='rankroczny.php?edit=".$row->id."' title='edytuj' class='edit'></a>
                    <a href='rankroczny.php?delete=" . $row->id . "' title='usuń' class='delete' onclick='return confirmDelete (\"$row->osoba\");'></a>
                </td>
            </tr>";
    }
}
?>
        </table>
    </div>
</div>
</body>
</html>
