<?php

function my_strtoupper ($str){
    $ogonki_sma = array ("ą","ć","ę","ł","ń","ó","ś","ź","ż");
    $ogonki_big = array ("Ą","Ć","Ę","Ł","Ń","Ó","Ś","Ź","Ż");
    $str        = strtoupper($str);
    return str_replace($ogonki_sma, $ogonki_big, $str);
}

function tabelkaZapisu ($id, $gracz, $nazwisko, $max_ilosc_ruchow) {
    global $DB_TABLES;

    print '
    <table class="zapis">
        <tr>
            <th colspan="5">
                <input type="submit" value="' . $nazwisko . ' - aktualizuj" class="button"/>
            </th>
        </tr>
        <tr>
            <th class="lp"></th>
            <th class="letters">Litery</th>
            <th class="words">Wyrazy</th>
            <th class="points">Pkt.</th>
            <th class="points"></th>
        </tr>';

    $result = mysql_query ("SELECT * FROM $DB_TABLES[moves] WHERE id_zapisu='".$id."' AND gracz='".$gracz."' ORDER BY ruch");
    $i = 1;

    while ($row = mysql_fetch_array($result)){
        ($row['punkty'] == '-1000') ? $punkty = '' : $punkty = $row['punkty'];
        ($row['suma']   == '0')     ? $suma   = '' : $suma   = $row['suma'];

        print '
        <tr>
            <input type="hidden" name="ruch' . $gracz . '[]" value="' . $row['ruch'] . '">
            <td class="lp">' . $row['ruch'] . '</td>
            <td class="letters" ><input type="text" name="litery' . $gracz . '[]" value="' . $row['litery'] . '" maxlength="7"></td>
            <td class="words" ><input type="text" name="wyrazy' . $gracz . '[]" value="' . $row['wyrazy'] . '"></td>
            <td class="points"><input type="text" name="punkty' . $gracz . '[]" value="' . $punkty . '" maxlength="4" ></td>
            <td class="points"><input type="text" name="suma'   . $gracz . '[]" value="' . $suma . '" disabled></td>
        </tr>';
        ++$i;
    }

    for ($row = $i; $row <= $max_ilosc_ruchow; $row++){
        print '
        <tr>
            <input type="hidden" name="ruch' . $gracz . '[]" value="' . $row . '">
            <td class="lp">' . $row . '</td>
            <td class="letters" ><input type="text" name="litery' . $gracz . '[]" maxlength="7"></td>
            <td class="words" ><input type="text" name="wyrazy' . $gracz . '[]"></td>
            <td class="points"><input type="text" name="punkty' . $gracz . '[]" maxlength="4"></td>
            <td class="points"><input type="text" name="suma'   . $gracz . '[]" disabled></td>
        </tr>';
    }
    print '</table>';
}


function aktualizujPlansze ($id, $blank1, $blank2, $submit_board, $uwagi) {
    global $DB_TABLES;

    $data['blank1']     = my_strtoupper ($blank1);
    $data['blank2']     = my_strtoupper ($blank2);
    $data['plansza']    = "";
    $data['uwagi']      = $uwagi;

    $result         = mysql_query ("SELECT * FROM $DB_TABLES[games] WHERE `id`='".$id."' LIMIT 1");
    $row            = mysql_fetch_object ($result);
    $zbazy          = preg_split ('/(?<!^)(?!$)/u', $row->plansza);
    $data['blank1'] = (!$data['blank1'] ? $row->blank1 : ($data['blank1'] == '-' ? null : $data['blank1']));
    $data['blank2'] = (!$data['blank2'] ? $row->blank2 : ($data['blank2'] == '-' ? null : $data['blank2']));

    for ($i = 0; $i < 225; $i++) {
        //gdy nasz pole jest takie samo jak w bazie lub gdy nasze jest puste -> bierzemy z bazy
        if (!$submit_board[$i] || $submit_board[$i] == $db_board[$i]) {
            $data['plansza'] .= ($db_board[$i] ? $db_board[$i] : '0');
        }

        // kasujemy pole
        else if ($submit_board[$i] == '-') {
            $data['plansza'] .= '0';
        }

        else if ($submit_board[$i]) {
            $data['plansza'] .= my_strtoupper ($submit_board[$i]);
        }
    }

    pfs_update ($DB_TABLES[games], $data, array (id => $id), 1);
}

function aktualizujGracza ($id, $gracz, $litery, $wyrazy, $punkty, $ruch, $suma, $max_ilosc_ruchow) {
    global $DB_TABLES;

    for ($i = 0; $i < $max_ilosc_ruchow; $i++) {
        if ($wyrazy[$i] && strpos ($wyrazy[$i], '-') !== 0) {
            $wyrazy[$i] = my_strtoupper ($wyrazy[$i]);
        }

        if ($litery[$i] != '') {
            $alitery    = preg_split('/(?<!^)(?!$)/u', my_strtoupper ($litery[$i]));
            sort ($alitery, SORT_LOCALE_STRING);
            $litery[$i] = implode ($alitery);
        }

        if ($punkty[$i] != '' && is_numeric ($punkty[$i])) {
            $result = mysql_query("SELECT * FROM $DB_TABLES[moves] WHERE id_zapisu='" . $id . "' AND gracz='" . $gracz . "' AND ruch<='" . $i . "' ORDER BY ruch");
            $s      = 0;
            while ($row = mysql_fetch_array($result)){
                if ($row['punkty'] != '-1000') {
                    $s += $row['punkty'];

                    if($s != $row['suma']) {
                        $q = "UPDATE $DB_TABLES[moves] SET suma='" . $s . "' WHERE id_zapisu='" . $id . "' AND ruch='" . $row['ruch'] . "' AND gracz='" . $gracz . "'";
                        mysql_query($q);
                    }
                }
            }
            $suma[$i] = $s + $punkty[$i];
        }
        else {
            $punkty[$i] = '-1000';
        }

        $data = array (
            id_zapisu   => $id,
            ruch        => $ruch[$i],
            gracz       => $gracz,
            litery      => my_strtoupper ($litery[$i]),
            wyrazy      => $wyrazy[$i],
            punkty      => $punkty[$i],
            suma        => $suma[$i]
        );

        $result = mysql_query ("SELECT * FROM $DB_TABLES[moves] WHERE id_zapisu='" . $id . "' AND ruch='" . $ruch[$i] . "' AND gracz='" . $gracz . "'");
        $update = mysql_num_rows ($result);

        mysql_num_rows ($result)
            ? pfs_update ($DB_TABLES[moves], $data, array (id_zapisu => $id, ruch => $ruch[$i], gracz => $gracz), 1)
            : pfs_insert ($DB_TABLES[moves], $data, 1);
    }
}

?>
