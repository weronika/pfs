<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

$REGIONS = array (
    'poludnie' => 'Region Południe',
    'pomorze'  => 'Region Pomorze',
    'centralny'   => 'Region Centrum i Mazury',
);

$year    = 2015;
define ("MR_TOURS_CNT", 5);
define ("MR_TOURS_MAX", 4);

foreach ($REGIONS as $region => $region_name) {
    $mr_tours[$region] = pfs_select (array (
        table => $DB_TABLES[tours],
        where => array ( 'YEAR(data_od)' => $year, 'region' => $region),
        order => array ( 'data_od' )
    ));

    $mr_results[$region] = pfs_select (array (
        table => $DB_TABLES["mr$year$region"],
        order => array ( '!suma' )
    ));
}

if ($_GET['delete']) {
    $region = $_GET['region'];
    pfs_delete ($DB_TABLES["mr$year$region"], array ('id' => $_GET['delete']));
}

else if (isset ($_POST['change'])) {
    $region = $_POST['region'];

    foreach ($mr_tours[$region] as $tour) {
		$miasto        = strtolower (no_pl_chars ($tour->miasto));
        $data[$miasto] = ($_POST[$miasto] ? $_POST[$miasto] : 0);
    }
    asort ($data);
    $suma = $i = 0;

    foreach (array_values ($data) as $pts) {
        if ($i++ < (MR_TOURS_CNT - MR_TOURS_MAX)) continue;
        $sum += $pts;
    }

    $data['osoba'] = $_POST['osoba'];
    $data['suma']  = $sum;

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES["mr$year$region"], $data)
        : pfs_update ($DB_TABLES["mr$year$region"], $data, array ('id' => $_POST['change']));
}

function table_header ($region) {
    global $mr_tours;

    $header = "<tr>
        <th class='lp'></th>
        <th class='person'>Imię i nazwisko</th>
        <th>Suma</th>";

    foreach ($mr_tours[$region] as $tour) {
		$header .= "<th class='punkty'>$tour->miasto</th>";
    };

    $header .= "<th></th></tr>";
    return $header;
}

?>

<html>
<head>
    <title>Mistrzostwa Regionów <? echo $year; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <style type="text/css">
        td.punkty, th.punkty {
            text-align: center;
            width: 90px;
        }
        td.punkty input {
            width: 30px;
            text-align: center;
            height: 22px;
        }
        input.ui-button {
            padding-top: 2px;
            padding-bottom: 2px;
        }
        h2 {
            margin: 20px 0;
        }

    </style>
</head>

<body>
<div id='header'>
    <h1>Mistrzostwa Regionów <? echo $year; ?></h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <? print "
            <li><a href='mr$year.php?nowa=pomorze'>Dodaj pomorski</a></li>
            <li><a href='mr$year.php?nowa=centralny'>Dodaj centralny</a></li>
            <li><a href='mr$year.php?nowa=poludnie'>Dodaj południowy</a></li>";
        ?>
    </ul>
</div>

<div id="content">

<?
if (isset ($_GET['nowa'])) {
    $region = $_GET['nowa'];
    print "<table>" . table_header ($region) .
        "<form action='mr$year.php' method='post'>
            <input type='hidden' name='change' value='-1'>
            <input type='hidden' name='region' value='$region'>
            <tr>
                <td class='lp'></td>
                <td class='person'><input type='text' name='osoba' size='16'></td>
                <td></td>";
    foreach ($mr_tours[$region] as $tour) {
		$miasto        = strtolower (no_pl_chars ($tour->miasto));
        print "<td class='punkty'><input type='text' name='$miasto'></td>";
    }
    print "
                <td><input type='submit' value='Zapisz' class='button'></td>
            </tr>
        </form>
        </table>";
}


foreach ($REGIONS as $region => $region_name) {
    $i        = 1;
    $prev_sum = 0;

    print "<h2>$region_name:</h2>";
    print "<table>" . table_header ($region);

    foreach ($mr_results[$region] as $person) {
        $i++;
        $prev_sum = $person->suma;

        if (( isset ($_GET['region'])) && ($_GET['region'] == $region) && ( isset ($_GET['edit'])) && ($_GET['edit'] == $person->id)) {
            print "
                <form action='mr$year.php' method='post'>
                    <input type='hidden' name='change' value='$person->id'>
                    <input type='hidden' name='region' value='$region'>
                    <tr>
                        <td class='lp'>" . ($prev_sum != $person->suma ? $i : '&nbsp;') . "</td>
                        <td class='person'><input type='text' name='osoba' size='16' value='$person->osoba'></td>
                        <td class='punkty'>$person->suma</td>";

            foreach ($mr_tours[$region] as $tour) {
				$miasto        = strtolower (no_pl_chars ($tour->miasto));
                print "<td class='punkty'><input type='text' name='$miasto' value='" . $person->$miasto . "'></td>";
            }
            print "<td><input type='submit' value='Zapisz' class='button'></td>
                    </tr>
                </form>";
        }

        else {
            print "
                <tr>
                    <td class='lp'>".($prev_sum != $person->suma ? $i : '&nbsp;')."</td>
                    <td class='person'><a href='mr$year.php?edit=$person->id&region=$region' title='edytuj'>$person->osoba</a></td>
                    <td class='punkty'>$person->suma</td>";

            foreach ($mr_tours[$region] as $tour) {
				$miasto        = strtolower (no_pl_chars ($tour->miasto));
                print "<td class='punkty'>" . $person->$miasto . "</td>";
            }
            print "<td class='icons'>
                        <a href='mr$year.php?edit=$person->id&region=$region' title='edytuj' class='edit'></a>
                        <a href='mr$year.php?delete=$person->id&region=$region' title='usuń' class='delete' onclick='return confirmDelete (\"" . $person->osoba . "\");'></a>
                    </td>
                </tr>";
        }
    }

    print "</table>";
}
?>

</div>
</body>
</html>
