<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

$turnieje = array (
    'sulecin'   => 'Sulęcin',
    'szczecin'  => 'Szczecin',
    'pulawy'    => 'Puławy',
    'elk'       => 'Ełk',
    'krakow'    => 'Kraków',
    'slupsk'    => 'Słupsk',
    'ostroda'   => 'Ostróda',
    'jaworzno'  => 'Jaworzno',
    'wroclaw'   => 'Wrocław'
);

if ($_GET['delete']) {
    pfs_delete ('gp2011', array ('id' => $_GET['delete']));
}

else if (isset ($_POST['change'])) {
    foreach (array_keys ($turnieje) as $tur) {
        $data[$tur] = ($_POST[$tur] ? $_POST[$tur] : 0);
    }
    asort ($data);
    $suma = $i = 0;
    foreach (array_values ($data) as $pts) {
        if ($i++ < 2) continue;
        $sum += $pts;
    }

    $data['osoba'] = $_POST['osoba'];
    $data['suma']  = $sum;

    $_POST['change'] == -1
        ? pfs_insert ('gp2011', $data)
        : pfs_update ('gp2011', $data, array ('id' => $_POST['change']));
}
?>
<html>
<head>
    <title>Grand Prix 2011</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Grand Prix 2011</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='gp2011.php?nowa=1'>Dodaj nową osobę</a></li>
    </ul>
</div>

<div id="content">
    <table>
        <tr>
            <th class='lp'></th>
            <th class='person'>Imię i nazwisko</th>
            <th>Suma</th>
<?
foreach ($turnieje as $key => $value) {
    print '<th>'.$value.'</th>';
}
?>

            <th></th>
        </tr>
<?
if (isset ($_GET['nowa'])) {
    print "
        <form action='gp2011.php' method='post'>
            <input type='hidden' name='change' value='-1'>
            <tr>
                <td class='lp'></td>
                <td class='person'><input type='text' name='osoba' size='16'></td>
                <td></td>";
    foreach (array_keys ($turnieje) as $key) {
        print '<td><input type="text" name="'.$key.'" size="1"></td>';
    }
    print "
                <td><input type='submit' value='Zapisz' class='button'></td>
            </tr>
        </form>";
}

$i          = 1;
$prev_sum   = 0;
$rows       = pfs_select (array (
    table   => $DB_TABLES[gp2011],
    order   => array ( '!suma' )
));
foreach ($rows as $row) {
    $i++;
    $prev_sum = $row->suma;

    if ( ( isset ($_GET['edit'])) && ($_GET['edit'] == $row->id)) {
        print "
            <form action='gp2011.php' method='post'>
                <input type='hidden' name='change' value='".$row->id."'>
                <tr>
                    <td class='lp'>".($prev_sum != $row->suma ? $i : '&nbsp;')."</td>
                    <td class='person'><input type='text' name='osoba' size='16' value='".$row->osoba."'></td>
                    <td>".$row->suma."</td>";

        foreach (array_keys ($turnieje) as $key) {
            print '<td><input type="text" name="'.$key.'" size="1" value="'.$row->$key.'"></td>';
        }
        print "
                    <td><input type='submit' value='Zapisz' class='button'></td>
                </tr>
            </form>";
    }

    else {
        print "
            <tr>
                <td class='lp'>".($prev_sum != $row->suma ? $i : '&nbsp;')."</td>
                <td class='person'><a href='gp2011.php?edit=".$row->id."' title='edytuj'>".$row->osoba."</a></td>
                <td class='center'>".$row->suma."</td>";

        foreach (array_keys ($turnieje) as $key) {
            print '<td class="center">'.$row->$key.'</td>';
        }
        print '
                <td class="icons">
                    <a href="gp2011.php?edit='.$row->id.'" title="edytuj" class="edit"></a>
                    <a href="gp2011.php?delete='.$row->id.'" title="usuń" class="delete" onclick="return confirmDelete (\'' . $row->osoba . '\');"></a>
                </td>
            </tr>';
    }
}
?>
    </table>
</body>
</html>
