<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'siodemki') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[sevens], array ('id' => $_GET['delete']));
}

else if ($_POST['edit'] || $_POST['nowa']) {
    $siodemka       = strtoupper ($_POST['siodemka']);
    $nazwa_pliku    = str_replace (array ("Ą", "Ć", "Ę", "Ł", "Ń", "Ó", "Ś", "Ź", "Ż"), array ("A", "C", "E", "L", "N", "O", "S", "Z", "Z"), $siodemka);

    if (($aktywna = ($_POST['aktywna'] == 'aktywna'))) {
        pfs_update ($DB_TABLES[sevens], array ( 'aktywna' => 0 ), array ( 'aktywna' => 1 ));
    }

    $data = array (
        'siodemka'      => $siodemka,
        'aktywna'       => $aktywna,
        'nazwa_pliku'   => $nazwa_pliku,
        'znaczenie'     => sierotki ($_POST['znaczenie']),
        'zrodlo'        => $_POST['zrodlo'],
        'odmiana'       => $_POST['odmiana'],
        'przedluzki'    => ($_POST['przedluzki'] ? $_POST['przedluzki'] : 'brak'),
        'anagramy'      => ($_POST['anagramy'] ? $_POST['anagramy'] : 'brak'),
        'zblankiem'     => ($_POST['zblankiem'] ? $_POST['zblankiem'] : 'brak'),
        'subanagramy'   => $_POST['subanagramy'],
        'pochodne'      => $_POST['pochodne']
    );

    if ($_POST['nowa']) {
        $auto_id = pfs_insert ($DB_TABLES[sevens], $data);
        pfs_photo_update ($_FILES[foto7], 'sevens', $auto_id);
    }
    else {
        pfs_update ($DB_TABLES[sevens], $data, array ('id' => $_POST['edit']));
        pfs_photo_update ($_FILES[foto7], 'sevens', $_POST['edit']);
    }
}
?>
<html>
<head>
    <title>Siódemki tygodnia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Siódemki tygodnia</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='siodemki.php'>Nowa siódemka</a></li>
    </ul>
</div>

<div id='content'>
    <table>
<?
$i      = 0;
$sevens = pfs_select (array (
    table   => $DB_TABLES[sevens],
    order   => array ( 'siodemka' )
));
foreach ($sevens as $seven) {
    print ($i % 5 ? '' : '<tr>') . '
        <td class="icons">
            <a href="siodemki.php?edit=' . $seven->id . '" title="edytuj" class="edit"></a>
            <a href="http://www.pfs.org.pl/siodemka.php?s=' . $seven->siodemka . '" title="'. $seven->siodemka .'" target="_blank" class="link"></a>
            <a href="siodemki_lista.php?delete=' . $seven->id . '" title="usuń" class="delete" onclick="return confirmDelete (\'' . $seven->siodemka . '\');"></a>
        </td>
        <td>
            <a href="siodemki.php?edit=' . $seven->id . '" title="edytuj">
                <span ' . ($seven->aktywna ? 'class="active7">' : '>') . $seven->siodemka . '</span>
            </a>
        </td>';

    if ($i % 5 == 5) {
        print '</tr>';
    }

    $i++;
}
?>
    </table>
</div>
</body>
</html>
