<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'siodemki') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

if (isset ($_GET['edit'])) {
    $in = pfs_select_one (array (
        table   => $DB_TABLES[sevens],
        where   => array ( id => $_GET['edit'] )
    ));
}
?>

<html>
<head>
    <title>Siódemka tygodnia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>"Siódemka tygodnia"</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='siodemki_lista.php'>Siódemki</a></li>
        <? if ($in->id) print "<li><a target='_blank' href='http://www.pfs.org.pl/siodemka.php?s=$in->nazwa_pliku'>Strona siódemki</a></li>";?>
    </ul>
</div>

<div id="content">

<?php
print "
<form enctype='multipart/form-data' action='siodemki_lista.php' method='post'>".
($in->id ? "<input type='hidden' name='edit' value='$in->id'>" : "<input type='hidden' name='nowa' value=1>")."
<input type='hidden' name='MAX_FILE_SIZE' value='120000'>
<table class='formTable'>
    <tr>
        <th>Siódemka:</th>
        <td>
            <input type='text' name='siodemka' maxlength='7' class='medium' value='$in->siodemka'>
            <input type='checkbox' name='aktywna' id='aktywna' class='button' title='Siódemka tygodnia' value='aktywna'".($in->aktywna ? " checked" : "").">
            <label for='aktywna'>Aktualna 7 tygodnia</label>
        </td>
    </tr>
    <tr>
        <th>Zdjęcie:</th>
        <td>
            <input name='foto7' type='file' class='medium'>
            <span>tylko JPG (max. 120kB)</span>
        </td>
    </tr>
    <tr>
        <th>Źródło zdjęcia:</th>
        <td><input type='text' name='zrodlo' class='long' value='$in->zrodlo'></td>
    </tr>
    <tr>
        <th>Znaczenie:</th>
        <td><textarea name='znaczenie' maxlength='65530' class='long' rows='4'>$in->znaczenie</textarea></td>
    </tr>
    <tr>
        <th>Odmiana:</th>
        <td><textarea name='odmiana' maxlength='65530' class='long' rows='2'>$in->odmiana</textarea></td>
    </tr>
    <tr>
        <th>Przedłużki:</th>
        <td>
            <span>wyróżnianie litery - &lt;span&gt;A&lt;/span&gt;</span>
            <textarea name='przedluzki' maxlength='65530' class='long' rows='2'>$in->przedluzki</textarea>
        </td>
    </tr>
    <tr>
        <th>Anagramy:</th>
        <td>
            <textarea name='anagramy' maxlength='65530' class='long' rows='2'>$in->anagramy</textarea>
        </td>
    </tr>
    <tr>
        <th>Anagramy z&nbsp;blankiem:</th>
        <td>
            <span>wyróżnianie litery - &lt;span&gt;A&lt;/span&gt;</span>
            <textarea name='zblankiem' maxlength='65530' class='long' rows='2'>$in->zblankiem</textarea>
        </td>
    </tr>
    <tr>
        <th>Subanagramy:</th>
        <td><textarea name='subanagramy' maxlength='65530' class='long' rows='2'>$in->subanagramy</textarea></td>
    </tr>
    <tr>
        <th>Wyrazy pochodne:</th>
        <td><textarea name='pochodne' maxlength='65530' class='long' rows='2'>$in->pochodne</textarea></td>
    </tr>
</table>

<div style='width:780px;margin:0 auto;text-align:center;margin-top:7px;'>
    <input type='submit' value='Aktualizuj siódemkę' class='button'>
</div>

</form>";

?>

</div>
</body>
</html>
