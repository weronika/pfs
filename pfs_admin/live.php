<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'live') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();
$id       = $_GET['id'];
$tab      = ($_GET['tab'] ? ($_GET['tab']-1) : 0);
$turniej  = pfs_select_one (array (
    table   => $DB_TABLES[tours],
    fields  => array ( 'nazwa', 'ilosc_rund'),
    where   => array ( id => $id, '!show_live' => 0 ),
));

if ($runda = $_POST['save']) {
    $data = array (
        id_turnieju     => $id,
        nr_rundy        => $runda,
        rozstawienie    => $_POST['rozstawienie' . $runda],
        klasyfikacja    => $_POST['klasyfikacja' . $runda],
        wyniki          => $_POST['wyniki' . $runda],
        komentarz       => $_POST['komentarz' . $runda]
    );

    pfs_replace ($DB_TABLES[live], $data, 1);
    pfs_refresh (array (id => $id, tab => $runda));
}
?>

<html>
<head>
    <title>LIVE - <? print $turniej->nazwa; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <style>
        th { vertical-align: top; width: 70px; text-align:right; padding-right: 10px; }
        textarea { width: 100%; background: white; }
    </style>
    <script>
        $(function(){ $( "#tabs" ).tabs ({ selected: <? print $tab; ?>}).show (); });
    </script>
</head>

<body>
<div id='header'>
    <h1><? print $turniej->nazwa; ?> - LIVE</h1>
    <ul class='menu'>
<? if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='main.php'>Menu</a></li>"; } ?>
        <li><a href='live_lista.php'>Turnieje</a></li>
    </ul>
</div>

<div id="content">
    <div id="tabs">

<?
print '<ul>';
for ($i = 1; $i <= $turniej->ilosc_rund; $i++) {
    print '<li><a href="#tabs-'.$i.'">' . $i . '</a></li>';
}
print '</ul>';

for ($i = 1; $i <= $turniej->ilosc_rund; $i++) {
    $runda  = pfs_select_one (array (
        table   => $DB_TABLES[live],
        where   => array ( id_turnieju => $id, 'nr_rundy' => $i ),
    ));
    if (!$runda->nr_rundy) $runda->nr_rundy = $i;

    print "
        <div id='tabs-$runda->nr_rundy'>
        <h1 style='text-align:center;'>Runda $runda->nr_rundy</h1>
        <form action='live.php?id=" . $id . "' method='post'>
            <input type='hidden' name='save' value='$runda->nr_rundy'>
            <table id='runda$runda->nr_rundy' class='formTable'>
                <tr>
                    <th>Rozstawienie:</th>
                    <td><textarea name = 'rozstawienie$runda->nr_rundy' rows = '8'>$runda->rozstawienie</textarea></td>
                </tr>
                <tr>
                    <th>Wyniki:</th>
                    <td><textarea name = 'wyniki$runda->nr_rundy' rows = '8'>$runda->wyniki</textarea></td>
                </tr>
                <tr>
                    <th>Klasyfikacja:</th>
                    <td><textarea name = 'klasyfikacja$runda->nr_rundy' rows = '8'>$runda->klasyfikacja</textarea></td>
                </tr>
                <tr>
                    <th>Komentarze<br>sędziego:</th>
                    <td><textarea name = 'komentarz$runda->nr_rundy' rows = '3'>$runda->komentarz</textarea></td>
                </tr>
            </table>

            <div style='width:780px;margin:0 auto;text-align:center;margin-top:7px;'>
                <input type='submit' value='Zapisz rundę $runda->nr_rundy' class='button'>
            </div>
        </form>
        </div>";
}
?>
    </div>
</div>
</body>
</html>
