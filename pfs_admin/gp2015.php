<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

$CLASSIFICATIONS = array (
    gold   => 1,
    silver => 2,
    brown  => 3,
);

$CLASSIFICATIONS_NAMES = array (null, "złota", "srebrna", "brązowa");

$year     = 2015;
$gp_tours = pfs_select (array (
    table => $DB_TABLES[tours],
    where => array ( 'YEAR(data_od)' => $year, 'rank' => $TOUR_STATUS[gp]),
    order => array ( 'data_od' )
));

define ("GP_TOURS_CNT", count ($gp_tours));
define ("GP_TOURS_MAX", 4);

$gp_results = pfs_select (array (
    table   => $DB_TABLES['gp'.$year],
    order   => array ( '!suma' )
));

if ($_GET['delete']) {
    pfs_delete ("gp$year", array ('id' => $_GET['delete']));
}

else if (isset ($_POST['change'])) {
    foreach ($gp_tours as $tour) {
        $miasto        = strtolower (no_pl_chars ($tour->miasto));
        $data[$miasto] = ($_POST[$miasto] ? $_POST[$miasto] : 0);
    }
    asort ($data);
    $suma = $i = 0;

    foreach (array_values ($data) as $pts) {
        if ($i++ < (GP_TOURS_CNT - GP_TOURS_MAX)) continue;
        $sum += $pts;
    }

    $data['osoba']        = $_POST['osoba'];
    $data['klasyfikacja'] = $_POST['classification'];
    $data['suma']         = $sum;

    $_POST['change'] == -1
        ? pfs_insert ("gp$year", $data)
        : pfs_update ("gp$year", $data, array ('id' => $_POST['change']));
}

function table_header () {
    global $gp_tours;

    $header = "<tr>
        <th class='lp'></th>
        <th class='person'>Imię i nazwisko</th>
        <th>Suma</th>";

    foreach ($gp_tours as $tour) {
        $header .= "<th>$tour->miasto</th>";
    };

    $header .= "<th></th></tr>";
    return $header;
}

?>

<html>
<head>
    <title>Grand Prix <? echo $year; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <style type="text/css">
        td.punkty {
            text-align: center;
        }
        td.punkty input {
            width: 30px;
            text-align: center;
        }
        h2 {
            margin: 20px 0;
        }

    </style>
</head>

<body>
<div id='header'>
    <h1>Grand Prix <? echo $year; ?></h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <? print "
            <li><a href='gp$year.php?nowa=1'>Dodaj złoty</a></li>
            <li><a href='gp$year.php?nowa=2'>Dodaj srebrny</a></li>
            <li><a href='gp$year.php?nowa=3'>Dodaj brązowy</a></li>";
        ?>
    </ul>
</div>

<div id="content">

<?
if (isset ($_GET['nowa'])) {
    $classify = $_GET['nowa'];
    print "<table>" . table_header () .
        "<form action='gp$year.php' method='post'>
            <input type='hidden' name='change' value='-1'>
            <input type='hidden' name='classification' value='$classify'>
            <tr>
                <td class='lp'></td>
                <td class='person'><input type='text' name='osoba' size='16'></td>
                <td></td>";
    foreach ($gp_tours as $tour) {
        $miasto = strtolower (no_pl_chars ($tour->miasto));
        print "<td class='punkty'><input type='text' name='$miasto'></td>";
    }
    print "
                <td><input type='submit' value='Zapisz' class='button'></td>
            </tr>
        </form>
        </table>";
}


foreach ($CLASSIFICATIONS as $classify) {
    $i          = 1;
    $prev_sum   = 0;

    print "<h2>Klasyfikacja $CLASSIFICATIONS_NAMES[$classify]:</h2>";
    print "<table>" . table_header ();

    foreach ($gp_results as $person) {
        if ($person->klasyfikacja != $classify) continue;

        $i++;
        $prev_sum = $person->suma;

        if ( ( isset ($_GET['edit'])) && ($_GET['edit'] == $person->id)) {
            print "
                <form action='gp$year.php' method='post'>
                    <input type='hidden' name='change' value='$person->id'>
                    <input type='hidden' name='classification' value='$classify'>
                    <tr>
                        <td class='lp'>" . ($prev_sum != $person->suma ? $i : '&nbsp;') . "</td>
                        <td class='person'><input type='text' name='osoba' size='16' value='$person->osoba'></td>
                        <td>$person->suma</td>";

            foreach ($gp_tours as $tour) {
                $miasto = strtolower (no_pl_chars ($tour->miasto));
                print "<td class='punkty'><input type='text' name='$miasto' value='" . $person->$miasto . "'></td>";
            }
            print "<td><input type='submit' value='Zapisz' class='button'></td>
                    </tr>
                </form>";
        }

        else {
            print "
                <tr>
                    <td class='lp'>".($prev_sum != $person->suma ? $i : '&nbsp;')."</td>
                    <td class='person'><a href='gp$year.php?edit=$person->id' title='edytuj'>$person->osoba</a></td>
                    <td class='center'>$person->suma</td>";

            foreach ($gp_tours as $tour) {
                $miasto = strtolower (no_pl_chars ($tour->miasto));
                print "<td class='center'>" . $person->$miasto . "</td>";
            }
            print "<td class='icons'>
                        <a href='gp$year.php?edit=$person->id' title='edytuj' class='edit'></a>
                        <a href='gp$year.php?delete=$person->id' title='usuń' class='delete' onclick='return confirmDelete (\"" . $person->osoba . "\");'></a>
                    </td>
                </tr>";
        }
    }

    print "</table>";
}
?>

</div>
</body>
</html>
