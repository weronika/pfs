<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();
?>

<html>
<head>
    <title>Nowy klub</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Nowy klub</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='kluby_lista.php'>Kluby</a></li>
    </ul>
</div>

<div id="content">
<?php
if (isset ($_GET['edit'])) {
    $id = $_GET['edit'];
    $in = pfs_select_one (array (
        table   => $DB_TABLES[clubs],
        where   => array ( id => $id ),
    ));
}

print "
<form action='kluby_lista.php' method='post' enctype='multipart/form-data'>" .
($id ? "<input type='hidden' name='edit' value='".$id."'>" : "<input type='hidden' name='nowy' value=1>") . "
<input type='hidden' name='MAX_FILE_SIZE' value='80000'>
<table class='formTable'>
    <tr>
        <th>Nazwa klubu:</th>
        <td><input type='text' name='nazwa' maxlength='240' size='60' value='".$in->nazwa."'></td>
    </tr>
    <tr>
        <th>Logo:</th>
        <td><input name='logo' type='file'></td>
    </tr>
    <tr>
        <th>Data rejestracji:</th>
        <td><input type='text' name='data' maxlength='10' value='".$in->data."' class='datepicker'></td>
    </tr>
    <tr>
        <th>Liczba członków:</th>
        <td><input type='text' name='liczbaczlonkow' maxlength='4' size='12' value='".$in->liczbaczlonkow."'/></td>
    </tr>
    <tr>
        <th>Strona www:</th>
        <td><input type='text' name='stronawww' maxlength='120' class='long' value='".$in->stronawww."'/></td>
    </tr>
    <tr>
        <th>Miasto:</th>
        <td><input type='text' name='miasto' class='medium' maxlength='64' value='".$in->miasto."'></td>
    </tr>
    <tr>
        <th>Miejsce spotkań:</th>
        <td><textarea name='adres' maxlength='65530' class='long' rows='3'/>".$in->adres."</textarea></td>
    </tr>
    <tr>
        <th>Link do mapki:</th>
        <td><input type='text' name='mapka' maxlength='120' class='long' value='".$in->mapka."'/></td>
    </tr>
    <tr>
        <th>Terminy spotkań:</th>
        <td><textarea name='terminy' maxlength='65530' class='long' rows='2'/>".$in->terminy."</textarea></td>
    </tr>
    <tr>
        <th>Kontakt z klubem:</th>
        <td><textarea name='kontakt' maxlength='65530' class='long' rows='2'/>".$in->kontakt."</textarea></td>
    </tr>
    <tr>
        <th>Władze klubu:</th>
        <td><textarea name='wladze' maxlength='65530' class='long' rows='2'/>".$in->wladze."</textarea></td>
    </tr>

</table>

<div style='width:780px;margin:0 auto;text-align:center;margin-top:7px;'>
    <input type='submit' value='Aktualizuj klub' class='button'>
</div>

</form>";
?>

</div>
</body>
</html>
