<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'siodemki') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ('osps');
session_start ();

if ($_POST['add_words']) {
    $ilosc = sizeof ($_POST['slowa']);
    for ($i = 0; $i < $ilosc; $i++){
        $slowo = $_POST['slowa'][$i];
        if ($slowo) {

            $orginal    = $slowo;
            $slowo      = strtoupper ($slowo);
            $trans      = array ('Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ś' => 's', 'Ź' => 'x',
                'Ż' => 'z', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z', 'ż' => 'z');
            $slowo      = strtr ($slowo, $trans);
            $dlugosc    = strlen ($slowo);

            $in_db      = pfs_select_one (array (
                table   => "ana$dlugosc",
                where   => array ( word => $slowo )
            ));

            if ($in_db) {
                $_SESSION['infos'] .= "<span class='important'>Słowo <b>$orginal</b> już występuje w słowniku.</span><br>";
            }
            else {
                $alfabet = array ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','Y','Z','a','c','e','l','n','o','s','x','z');
                $licznik = array (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

                for ($k = 0; $k < $dlugosc; $k++) {
                    if (in_array ($slowo[$k], $alfabet)) {
                        for ($j = 0; $j < 32; $j++) {
                            if ($slowo[$k] == $alfabet[$j]) {
                                $licznik[$j]++;
                                break;
                            }
                        }
                    }
                }

                $letters = '';
                for ($j = 0; $j < 32; $j++)
                    $letters .= $licznik[$j];

                //pfs_insert ("ana$dlugosc", array (word => $slowo, letters => $letters));
                //$_SESSION['infos'] .= "Słowo <b>$orginal</b> zostało pomyślnie dodane do słownika jako <b>$slowo</b>.<br>";
            }
        }
    }
}

// $dousuniecia = array ('CENTRIOLEM', 'CENTRIOLU', 'MINJANA', 'PUNKOWNE', 'PUNKÓWNEJ', 'PUNKÓWNYCH', 'PUNKÓWNYM', 'PUNKÓWNYMI', 'ROGOZĘBU');

// foreach ($dousuniecia as $nr => $slowopl) {
//    $trans = array ('Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ś' => 's', 'Ź' => 'x',
//                'Ż' => 'z', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z', 'ż' => 'z');
//    $slowo = strtr ($slowopl, $trans);
//    $query = "DELETE FROM ana".strlen($slowo)." WHERE word='".$slowo."'";
//    $res   = mysql_query ($query);

//    if($res)
//        print $slowopl." pomyślnie usunięty ze słownika.<br>";
//    else
//        print "<b>".$slowopl." nie udało się usunąć ze słownika.</b><br>";

// }

?>

<html>
<head>
    <title>Aktualizacja OSPS-a</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Aktualizacja OSPS-a</h1>
    <ul class='menu'>
<? if ($_COOKIE['pfsuser'] == 'admin' || $_COOKIE['pfsuser'] == 'siodemki') { print "<li><a href='main.php'>Menu</a></li>"; } ?>
    </ul>
</div>

<div id="content">

<? print "<div style='text-align:center;margin-bottom:15px;'>".$_SESSION['infos']."</div>"; unset ($_SESSION['infos']); ?>

<form action='osps_aktualizacja.php' method='post'>
<table class="formTable" style="width:300px;">
<? for ($i = 1; $i < 11; $i++) {
    print "<tr><th class='lp'>$i</th><td><input name='slowa[]'></td></tr>";
}
?>
</table>
<div style='width:300px;margin:0 auto;text-align:center;margin-top:7px;'>
    <!--<input type='submit' name='delete_words' value='Usuń słowa' class='button'>-->
    <input type='submit' name='add_words' value='Dodaj słowa' class='button'>
</div>

</form>

</div>
</body>
</html>
