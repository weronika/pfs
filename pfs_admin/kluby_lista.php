<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[clubs], array ('id' => $_GET['delete']));
}

else if ($_POST['edit'] || $_POST['nowy']) {

    $data = array (
        'nazwa'          => $_POST['nazwa'],
        'adres'          => $_POST['adres'],
        'miasto'         => $_POST['miasto'],
        'mapka'          => $_POST['mapka'],
        'terminy'        => $_POST['terminy'],
        'stronawww'      => $_POST['stronawww'],
        'liczbaczlonkow' => $_POST['liczbaczlonkow'],
        'kontakt'        => $_POST['kontakt'],
        'wladze'         => $_POST['wladze'],
        'data'           => $_POST['data']
    );

    if ($_POST['nowy']) {
        $auto_id = pfs_insert ($DB_TABLES[clubs], $data);
        pfs_photo_update ($_FILES[logo], $DB_TABLES[clubs], $auto_id);
    }
    else {
        pfs_update ($DB_TABLES[clubs], $data, array ('id' => $_POST['edit']));
        pfs_photo_update ($_FILES[logo], $DB_TABLES[clubs], $_POST['edit']);
    }
}
?>

<html>
<head>
    <title>Kluby PFS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css">
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Kluby PFS</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='kluby.php'>Nowy klub</a></li>
    </ul>
</div>

<div id="content">
    <table style="width:500px;">
<?
$rows = pfs_select (array (
    table   => $DB_TABLES[clubs],
    order   => array ( 'miasto' )
));
foreach ($rows as $row) {
    print '
        <tr>
            <td><a href="kluby.php?edit='.$row->id.'" title="edytuj">'.$row->nazwa.'</a></td>
            <td>'.$row->miasto.'</td>
            <td class="icons">
                <a href="kluby.php?edit='.$row->id.'" title="edytuj" class="edit"></a>
                <a href="kluby_lista.php?delete='.$row->id.'" title="usuń" class="delete" onclick="return confirmDelete (\'' . $row->nazwa . '\');"></a>
            </td>
        </tr>';
}
?>
    </table>
</div>

</body>
</html>
