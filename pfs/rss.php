<?php
    header('Content-Type: application/rss+xml; charset=UTF-8');

    include_once "files/php/funkcje.php";

    pfs_connect ();
    $result = pfs_select (array (
        table   => $DB_TABLES[news],
        order   => array ('!data'),
        limit   => 10
    ));

?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
        <atom:link href="http://www.pfs.org.pl/rss.php" rel="self" type="application/rss+xml" />
        <title>Polska Federacja Scrabble</title>
        <link>http://pfs.org.pl</link>
        <description>PFS - aktualności</description>
        <language>pl</language>
        <copyright>Copyright © Polska Federacja Scrabble</copyright>
        <lastBuildDate><? print date ('r', strtotime ($result[0]->data)); ?></lastBuildDate>

<?php
    foreach($result as $rss) {
        print '
        <item>
            <guid isPermaLink="false">PFS - news no. ' . $rss->id . '</guid>
            <link>' . $rss->link . '</link>
            <title>' . $rss->tytul . '</title>
            <description>' . $rss->descr . '</description>
            <pubDate>' . date ('r', strtotime ($rss->data)) . '</pubDate>
        </item>';
    }
?>
    </channel>
</rss>