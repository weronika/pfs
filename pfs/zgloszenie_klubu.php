<? include_once "files/php/funkcje.php"; ?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Kluby : Zgłoszenie klubu do Rejestru Klubów</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("kluby","rejestrklubow");</script>
    <style type="text/css">
        input, textarea, select{
            margin: 3px 0 15px 0;
            padding: 2px;
        }
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Zgłoszenie klubu do Rejestru Klubów")</script></h1>

<? if(empty($_POST['submit'])){
    print "<form method='post'>
    <table><tr><td>
    <div>Nazwa klubu:<br />
    <input name='nazwa' type='text' maxlength='70' size='40'></div>

    <div>Miejsce spotkań klubowych:<br />
    <textarea name='miejsce' cols='60' rows='3'></textarea></div>

    <div>Terminy spotkań klubowych:<br />
    <textarea name='terminy' cols='60' rows='3'></textarea></div>

    <div>Liczba członków:<br />
    <input name='liczbaczlonkow' type='text' maxlength='10' size='10'></div>

    <div>Strona WWW klubu:<br />
    <input name='stronawww' type='text' maxlength='200' size='70'></div>

    <div>Kontakt z klubem (opiekunem klubu):<br />
    <textarea name='kontakt' cols='60' rows='3'></textarea></div>

    <div>Dane ewentualnych władz klubowych:<br />
    <textarea name='wladze' cols='60' rows='3'></textarea></div>

    </td></tr>
    <tr><td colspan='2'>
    <input name='reset' type='reset' value='Wyczyść' class='przycisk' >
    <input type='submit' name='submit' value='Wyślij' class='przycisk' >
    </td></tr></table>
    </form>";
 }
else{

    $message = "Nazwa klubu: $_POST[nazwa]\n\nMiejsce spotkań klubowych: $_POST[miejsce]\n\nTerminy spotkań klubowych: $_POST[terminy]\n\nLiczba członków: $_POST[liczbaczlonkow]\n\nStrona WWW klubu: $_POST[stronawww]\n\nKontakt z klubem: $_POST[kontakt]\n\nWładze: $_POST[wladze]";
    mail_utf8 ("pfs@pfs.org.pl", "pfs@pfs.org.pl", "Zgłoszenie $_POST[nazwa] do Rejestru Klubów", $message);
    echo "Zgłoszenie zostało wysłane.";
}
?>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
