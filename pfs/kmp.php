<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Kluby : Klubowe Mistrzostwa Polski</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("kluby","kmp");</script>
	<style>
		h2{
	}
	</style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Klubowe Mistrzostwa Polski")</script></h1>


<?
$tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( '<=data_do' => date ("Y-m-d"), '!id' => '', '!status' => '', 'typ' => 'Klubowe Mistrzostwa Polski'),
    order   => array ( '!data_od' ),
));
foreach ($tours as $relacja){
    pokazRelacje ($relacja);
}
?>

<?require_once "files/php/bottom.php"?>
</body>
</html>
