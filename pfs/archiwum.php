﻿<?
include_once "files/php/funkcje.php";
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Archiwum turniejów</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" />
    <![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script>
    <![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script>
    <![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","archiwum");</script>
    <style type="text/css">
    #formtable{
        font-size: 11px;
        font-weight: normal;
        border-collapse: collapse;
    }
    #formtable td{
        padding: 4px;
    }
    #formtable td:first-child{
        width: 100px;
    }
    table.wyniki{
        margin-top: 20px;
        font-size: inherit;
    }
    table.wyniki td{
        padding: 1px 20px 0 0;
    }
    .linki{
        list-style: none;
        text-align: right;
        float: right;
    }
    .linki li{
        margin-bottom: 2px;
    }

    input[type="text"], select {
        width: 200px;
        border: solid 1px #B2C8E0;
        border-radius: 3px;
        box-shadow: #EFF6FE 2px 2px 0 0 inset;
        color: #5B738E;
        display: inline-block;
        vertical-align: middle;
        padding: 4px 7px;
        font-size: 13px;
        line-height: 16.75px;
        font-family: "Helvetica Neue",Arial,sans-serif;
        font-weight: 400;
    }
    input:focus {
        color: #394d63;
        border-color: #809CBA;
    }

    .test-przycisk {
        margin-left: 1em;
        background: -moz-linear-gradient(#669BE1, #5784BF) repeat scroll 0 0 #5784BF;
        border-radius: 6px;
        box-shadow: 0 1px rgba(0, 0, 0, 0.1), 0 -2px rgba(0, 0, 0, 0.1) inset;
        color: #FFFFFF;
        cursor: pointer;
        font: bold 13px/1em "Trebuchet MS",sans-serif;
        padding: 6px 10px 7px;
        position: relative;
        text-align: center;
        text-shadow: 0 1px 0 rgba(0, 0, 0, 0.5);
        line-height: 1.3em;
        font-family: "Helvetica Neue",Arial,sans-serif;
        font-size: 11px;
    }
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Archiwum turniejów PFS")</script></h1>
<ul class="linki">
    <li><a href="mapaturniejow.php">Mapa Turniejów Scrabblowych</a></li>
    <li><a href="zapisy.php">Archiwalne zapisy partii</a></li>
    <li><a href="archiwum_gp.php">Historia Grand Prix</a></li>
    <li><a href="podsumowania.php">Podsumowania lat</a></li>

</ul>

<form action="archiwum.php" method="post">
<table id="formtable">
    <tr>
        <td>Nazwa turnieju:</td>
        <td colspan="3"><input name="nazwa" type="text" maxlength="255" size="60" <?if(isset($_POST['nazwa'])) print "value='".$_POST['nazwa']."'"?> /></td>
    </tr>
    <tr>
        <td>Miejscowość:</td>
        <td><input name="miasto" type="text" maxlength="255" size="30" <?if(isset($_POST['miasto'])) print "value='".$_POST['miasto']."'"?> /></td>
        <td>Rok:</td>
        <td><select name="rok">
            <option value='all'></option>
        <?
        $year_act   = date('Y');
        $tmp        = pfs_select_one (array (
            table   => $DB_TABLES[tours],
            where   => array ( '!id_turnieju' => '', '!data_od' => '0000-00-00', ),
            order   => array ( 'data_od' ),
        ));
        $year_min  = (int) substr ($tmp->data_od, 0, 4);

        for ($y = $year_act; $y >= $year_min; $y--){
            print "<option value='$y' ".
                (($_POST['rok'] && $_POST['rok'] == $y) || (!$_POST['rok'] && $y == $year_act) ? "selected='selected'" : "") .
                ">$y</option>";
        }
        ?>
        </select>
        </td>
    </tr>
    <tr>
        <td>Typ turnieju:</td>
        <td colspan="3">
            <select name="typ">
                <option value="all">wszystkie</option>
                <option value="rank"                            <?print ($_POST['typ'] == 'rank'                              ? "selected='selected'" : "")?>>tylko rankingowe</option>
                <option value="nierank"                         <?print ($_POST['typ'] == 'nierank'                           ? "selected='selected'" : "")?>>tylko nierankingowe</option>
                <option value="gp"                              <?print ($_POST['typ'] == 'gp'                                ? "selected='selected'" : "")?>>Grand Prix</option>
                <option value="wczasy"                          <?print ($_POST['typ'] == 'wczasy'                            ? "selected='selected'" : "")?>>wczasy scrabblowe</option>
                <option value="Puchar Polski"                   <?print ($_POST['typ'] == 'Puchar Polski'                     ? "selected='selected'" : "")?>>Puchar Polski</option>
                <option value="Mistrzostwa Polski"              <?print ($_POST['typ'] == 'Mistrzostwa Polski'                ? "selected='selected'" : "")?>>Indywidualne Mistrzostwa Polski</option>
                <option value="Klubowe Mistrzostwa Polski"      <?print ($_POST['typ'] == 'Klubowe Mistrzostwa Polski'        ? "selected='selected'" : "")?>>Klubowe Mistrzostwa Polski</option>
                <option value="Mistrzostwa Polski Nauczycieli"  <?print ($_POST['typ'] == 'Mistrzostwa Polski Nauczycieli'    ? "selected='selected'" : "")?>>Mistrzostwa Polski Nauczycieli</option>
                <option value="Mistrzostwa Polski Szkół"        <?print ($_POST['typ'] == 'Mistrzostwa Polski Szkół'          ? "selected='selected'" : "")?>>Mistrzostwa Polski Szkół</option>
                <option value="Mistrzostwa Polski po angielsku" <?print ($_POST['typ'] == 'Mistrzostwa Polski po angielsku'   ? "selected='selected'" : "")?>>Mistrzostwa Polski po angielsku</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Zwycięzca:</td>
        <td colspan="3">
        <input name="zwyciezca" type="text" maxlength="255" size="30" value="<?print $_POST['zwyciezca']?>" />
        </td>
    </tr>
    <tr>
        <td>Sortuj wg:</td>
        <td colspan="3">
            <select name="sort">
                <option value="data_od"     <?print ($_POST['sort' ] == 'data_od'   ? "selected='selected'" : "")?>>daty
                <option value="miasto"      <?print ($_POST['sort'] == 'miasto'     ? "selected='selected'" : "")?>>miejscowości
                <option value="zwyciezca"   <?print ($_POST['sort'] == 'zwyciezca'  ? "selected='selected'" : "")?>>zwycięzcy
            </select>
        </td>
    </tr>
    <tr>
        <td>Wyświetl relację:</td>
        <td><input type="checkbox" name="zrelacja" value="zrelacja" <?print ($_POST['zrelacja'] ? 'checked' : ''); ?> /></td>
        <td class='alignright' colspan='2'><input type='submit' value='Wyszukaj' name="szukaj" class='test-przycisk'/></td>
    </tr>
</table>
</form>
<?
if ($_POST['szukaj']){
    $where = array ( '!id' => '', '!status' => '', '<=data_od' => 'NOW()' );
    if ($_POST['nazwa'])        $where['%nazwa']  = "%$_POST[nazwa]%";
    if ($_POST['miasto'])       $where['%miasto'] = "%$_POST[miasto]%";
    if ($_POST['rok'] && $_POST['rok'] != 'all') { $where['>=data_do'] = "$_POST[rok]-01-01"; $where['<=data_do'] = "$_POST[rok]-12-31"; }
    if ($_POST['typ'] == 'gp')      $where['rank'] = $TOUR_STATUS['gp'];
    else if ($_POST['typ'] == 'rank')    $where['rank'] = $TOUR_STATUS['rank'];
    else if ($_POST['typ'] == 'nierank') $where['rank'] = $TOUR_STATUS['norank'];
    else if ($_POST['typ'] == 'wczasy')  $where['rank'] = $TOUR_STATUS['vacation'];
    else if ($_POST['typ'] != 'all')     $where['%typ'] = $_POST['typ'];
    if ($_POST['zwyciezca'] && $_POST['zwyciezca'] != 'all') $where['%zwyciezca'] = "%$zwyciezca%";

    $found = pfs_select (array (
        table   => $DB_TABLES[tours],
        where   => $where,
        order   => array ( ($_POST['sort'] == 'data_od' ? '!data_od' : $_POST['sort']) ),
    ));
    $rows_cnt = sizeof ($found);
    print ($rows_cnt ? "Wyniki: <b>$rows_cnt</b>" : "Brak wyników odpowiadających zapytaniu.");

    if ($_POST['zrelacja']) {
        foreach ($found as $tour) {
            menuTurnieju ($tour, true);
            pokazRelacje ($tour);
        }
    }
    else{
        print "<table class='wyniki'>";
        foreach ($found as $tour) {
            print "<tr><td><a href='turniej.php?id=$tour->id'>". wyswietlDate ($tour->data_od, $tour->data_do, true) ."</a></td>".
          "<td><a href='turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
        }
        print "</table>";
    }
}

require_once "files/php/bottom.php"?>
</body>
</html>
