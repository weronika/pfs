<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wiesław Olewiski</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Odszedł Wiesław Olewiński</h1>

<center><img src="../rozne/wiesol.jpg"></center><br> <br>
Z wielkim smutkiem zawiadamiamy, że odszedł od nas wspaniały, pełen poczucia humoru kolega, Wiesław Olewiński - członek Polskiej Federacji Scrabble i zarazem jeden z najstarszych scrabblistów, grający aktywnie na turniejach w latach 1996-2008.  <br> <br>Uroczystość żałobna odbędzie się w kościele Św. Katarzyny w Warszawie przy ul. Fosa 17 (Służew), we czwartek 12 lutego 2015 r. o godzinie 13.30.


<?require_once "../files/php/bottom.php"?>
</body>
</html>
