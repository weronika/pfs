<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Galeria turniejów scrabblowych</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","galeria");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Zdjęcia z turniejów")</script></h1>

<div id="tabs" style="display:none;">
<?
    print '<ul>';
    for ($year = date ('Y'); $year >= 2001; $year--) {
        print "<li><a href='#tabs-$year'>$year</a></li>";
    }
    print '</ul>';

    for ($year = date ('Y'); $year >= 2001; $year--) {
        $galleries = pfs_select (array (
            table   => $DB_TABLES[tours],
            fields  => array ( 'galeria1', 'galeria2', 'galeria3', 'galeria4', 'galeria5', 'nazwa', 'miasto'),
            where   => array (
                '>=data_do'     => "$year-01-01",
                '<=data_do'     => "$year-12-31",
                '!galeria1'     => ''
            ),
            order   => array ('!data_od')
        ));
        print "<div id='tabs-$year'><ol>";

        foreach ($galleries as $gallery) {
            if ($gallery->galeria1) print "<li><a href='$gallery->galeria1' target='_blank'>$gallery->nazwa ($gallery->miasto)</a></li>";
            if ($gallery->galeria2) print "<li><a href='$gallery->galeria2' target='_blank'>$gallery->nazwa ($gallery->miasto)</a></li>";
            if ($gallery->galeria3) print "<li><a href='$gallery->galeria3' target='_blank'>$gallery->nazwa ($gallery->miasto)</a></li>";
            if ($gallery->galeria4) print "<li><a href='$gallery->galeria4' target='_blank'>$gallery->nazwa ($gallery->miasto)</a></li>";
            if ($gallery->galeria5) print "<li><a href='$gallery->galeria5' target='_blank'>$gallery->nazwa ($gallery->miasto)</a></li>";
        }
        print "</ol></div>";
    }
?>
</div>
<?require_once "files/php/bottom.php"?>
</body>
</html>
