<?php
require_once 'files/php/funkcje.php';

function stojak ($id, $gracz) {
    global $DB_TABLES;

    $last_move = pfs_select_one (array (
        table   => $DB_TABLES[moves],
        fields  => array ( 'MAX(`ruch`)' ),
        where   => array ( '>punkty' => '-100', 'id_zapisu' => $id, 'gracz' => $gracz )
    ));

    $last_move2 = pfs_select_one (array (
        table   => $DB_TABLES[moves],
        fields  => array ( 'MAX(`ruch`)' ),
        where   => array ( '!litery' => '', 'id_zapisu' => $id, 'gracz' => $gracz )
    ));

    $last_set = pfs_select_one (array (
        table   => $DB_TABLES[moves],
        fields  => array ( 'litery' ),
        where   => array ( 'id_zapisu' => $id, 'gracz' => $gracz, 'ruch' => max ($last_move->max, $last_move2->max) )
    ));

    $stojak = preg_split ('/(?<!^)(?!$)/u', $last_set->litery);

    for ($i = 0; $i < 7; ++$i) {
        $output .= '<td>';
        if ($stojak[$i] != null) {
            ($stojak[$i] == '*')
                ? $literka = 'x'
                : $literka = $stojak[$i];
            $output .= '<script>literki("' . $literka . '", 0)</script>';
        }
        $output .= '</td>';
    }

    return $output;
}

if (isset ($_GET['id'])) {
    $game = pfs_select_one (array (
        table   => $DB_TABLES[games],
        where   => array ( 'id' => $_GET['id'] )
    ));

    $board_tmp = preg_split('/(?<!^)(?!$)/u', $game->plansza);

    foreach ($board_tmp as $index => $letter) {
        $board[$index][letter] = $letter;
    }

    foreach ($BOARD_BONUSES as $bonus => $numbers) {
        foreach ($numbers as $number) {
            $board[$number][bonus] = $bonus;
        }
    }
?>
<html>
<head>
    <title><?php echo $game->nazwa; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="refresh" content="120">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("glowna","aktualnosci");</script>
    <style type="text/css">
        table.zapis { width: 243px; font-size: 9px;}
        table.zapis td.gracz{ font-size: 14px; font-weight: bold; text-align: center; padding: 10px 5px; }
        table.zapis th, table.zapis td{ padding: 6px 2px; }
        table#plansza   {margin: 5px; text-align: center;}
        table#plansza td    {width: 25px; height: 25px;  line-height: 25px; background:#3C8571;  border: 1px solid #23574B;}
        table#plansza th, table#plansza td:first-child  {width: 20px; height: 25px; line-height: 25px; background:#23574B; border: 1px solid #23574B; color:white; font-weight: normal; font-size:9px;}
        table#plansza td.word3    {background: #DB3920;}
        table#plansza td.word2   {background: #EFA284;}
        table#plansza td.letter3       {background: #4194E0;}
        table#plansza td.letter2  {background: #79B6E5;}
        span.blank{color: red;}
        #worek{ text-align: left;   margin: 0 10px 20px 10px;}
    </style>
    <script>
        var worektab = new Array ('a','a','a','a','a','a','a','a','a','a5','b','b','c','c','c','c6','d','d','d','e','e','e','e','e','e','e','e5','f','g','g','h','h','i','i','i','i','i','i','i','i','j','j','k','k','k','l','l','l','l3','l3','m','m','m','n','n','n','n','n','n7','o','o','o','o','o','o','o5','p','p','p','r','r','r','r','s','s','s','s','s5','t','t','t','u','u','w','w','w','w','y','y','y','y','z','z','z','z','z','z5','z9','blank','blank');
        var worektab_stary = new Array ('a','a','a','a','a','a','a','a','a5','b','b','c','c','c','c6','d','d','d','e','e','e','e','e','e','e','e5','f','f','g','g','h','h','i','i','i','i','i','i','i','i','j','j','k','k','k','l','l','l','l3','l3','m','m','m','n','n','n','n','n','n7','o','o','o','o','o','o','o5','p','p','p','r','r','r','r','s','s','s','s','s5','t','t','t','u','u','w','w','w','w','y','y','y','y','z','z','z','z','z','z5','z9','blank','blank');

        <? if ($game->data < '2001-01-01') print 'worektab = worektab_stary;'?>
        var stantab  = new Array (100);
        for (i = 0; i < 100; i++) {
            stantab[i] = 0;     //1-literka jest na planszy, 0 - w worku
        }

        function wykresl (plytka) {
            for (i = 0; i < 100; i++){
                if (worektab[i] == plytka && stantab[i] == 0){
                    stantab[i] = 1;
                    break;
                }
            }
        }

        function literki (litera, blanek) {
            if (litera != null && litera != '') {
                var a = litera.toLowerCase ();
                switch(a) {
                    case 'ą': a='a5'; break;
                    case 'ć': a='c6'; break;
                    case 'ę': a='e5'; break;
                    case 'ł': a='l3'; break;
                    case 'ń': a='n7'; break;
                    case 'ó': a='o5'; break;
                    case 'ś': a='s5'; break;
                    case 'ż': a='z5'; break;
                    case 'ź': a='z9'; break;
                    case 'x': a='blank'; break;
                    default:  break;
                }
                if (blanek) {
                    wykresl('blank');
                    document.write ("<img src='files/tiles_org/blanki/" + a + ".gif' alt='" + litera + "' class='literka'>");
                }
                else {
                    wykresl(a);
                    document.write ("<img src='files/tiles_org/" + a + ".gif' alt='" + litera + "' class='literka'>");
                }
            }
        }

        function worek(){
            var ile, koncowka, katalog;

            ile     = 100 - $('.literka').length;

            if (ile == 1) {
                koncowka = 'litera';
            }
            else if ((ile%10 == 2) || (ile%10 == 3) || (ile%10 == 4)) {
                koncowka = 'litery';
            }
            else {
                koncowka = 'liter';
            }

            if (ile > 0) {
                $('#worek').html ('W worku pozostało: <b>' + ile + ' </b>' + koncowka + '<br>');
            }
            else {
                $('#worek').html ('W worku nie ma już liter<br>');
            }

            for (i = 0; i < 100; i++){
                katalog = (stantab[i] == 1
                    ? 'files/tiles_org/wykreslanka/'
                    : 'files/tiles_org/'
                );

                $('#worek').append ('<img src="' + katalog + worektab[i] + '.gif" alt="' + worektab[i] + '">');
            }
        }

        $(document).ready (worek);

    </script>
    
</head>

<body>
    
<?php require_once "files/php/menu.php";
print "<h1>$game->nazwa<span>$game->gospodarz : $game->gosc</span></h1>";
?>

<div id='worek'></div>
<div style="margin:20px;"><? print $game->uwagi; ?></div>

<table class='klasyfikacja zapis onleft' style='margin-left:-20px;'>
    <tr>
        <td class='gracz' colspan='5'><?php echo $game->gospodarz; ?></td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th>Litery</th>
        <th>Wyrazy</th>
        <th>Pkt</th>
        <th>Suma</th>
    </tr>

<?
$host_moves = pfs_select (array (
    table   => $DB_TABLES[moves],
    where   => array ( id_zapisu => $game->id, gracz => 1 ),
    order   => array ( 'ruch' )
));

foreach ($host_moves as $move) {
    if ($move->litery || $move->punkty > -1000 || $move->wyrazy || $move->suma) {
        if ($move->punkty > -1000){
            $punkty = $move->punkty;
            $suma   = $move->suma;
        }
        else {
            $punkty = $suma = '';
        }

        $wyrazy = $move->wyrazy;
        if (strpos ($wyrazy, '-') === 0) {
            $wyrazy = str_replace ('-', '', $wyrazy);
        }

        print "
            <tr>
                <td>$move->ruch</td>
                <td class='alignleft'>$move->litery</td>
                <td class='alignleft'>$wyrazy</td>
                <td>$punkty</td>
                <td>$suma</td>
            </tr>";
    }
}
print '
    </table>
    <table id="plansza" class="onleft">
        <tr>
            <th>&nbsp;</th>
';

for ($i = 1; $i < 16; ++$i) {
    print "<th>$i</th>";
}

print '<th>&nbsp;</th></tr>';

$bl1 = strlen($game->blank1) > 2
    ? (ord($game->blank1[0]) - 65) * 15 + intval($game->blank1[1].$game->blank1[2])
    : (ord($game->blank1[0]) - 65) * 15 + intval($game->blank1[1]);

$bl2 = strlen($game->blank2) > 2
    ? (ord($game->blank2[0]) - 65) * 15 + intval($game->blank2[1].$game->blank2[2])
    : (ord($game->blank2[0]) - 65) * 15 + intval($game->blank2[1]);

for ($col = 0; $col < 15; $col++) {
    $output .= '<tr><td class="aligncenter">' . chr(65 + $col) . '</td>';

    for ($row = 0; $row < 15; $row++) {
        $index   = $col * 15 + $row;
        $output .= '<td class="' . $board[$index][bonus] . '">';

        if ($board[$index][letter] != '0') {
            $blanek  = (($index + 1) == $bl1 || ($index + 1) == $bl2 ? 1 : 0);
            $output .= '<script>literki ("' . $board[$index][letter] . '", ' . $blanek. ')</script>';
        }
    }
    $output .= '<th>&nbsp;</th></tr>';
}

print $output;
print '
    <tr>
        <th colspan=17>&nbsp;</th>
    </tr>
    <tr><th>&nbsp;</th>';

print stojak ($game->id, 1);
print '<th></th>';
print stojak ($game->id, 2);

$result = mysql_query ("SELECT SUM(`punkty`) AS `suma` FROM $DB_TABLES[moves] WHERE `id_zapisu`='" . $game->id . "' AND `punkty`<>'-1000' AND `gracz`='1' GROUP BY `gracz`");
$row    = mysql_fetch_array($result);
$suma1  = $row['suma'];
$result = mysql_query ("SELECT SUM(`punkty`) AS `suma` FROM $DB_TABLES[moves] WHERE `id_zapisu`='" . $game->id . "' AND `punkty`<>'-1000' AND `gracz`='2' GROUP BY `gracz`");
$row    = mysql_fetch_array($result);
$suma2  = $row['suma'];

print "
        <th>&nbsp;</th>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th colspan='7'>
            <b>$game->gospodarz: $suma1</b>
        </th>
        <th>&nbsp;</th>
        <th colspan='7'>
            <b>$game->gosc: $suma2</b>
        </th>
        <th>&nbsp;</th>
    </tr>
</table>

<table class='klasyfikacja onright zapis' style='margin-right:-20px;'>
    <tr>
        <td class='gracz' colspan='5'>$game->gosc</td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th>Litery</th>
        <th>Wyrazy</th>
        <th>Pkt</th>
        <th>Suma</th>
    </tr>
";

$result = mysql_query ("SELECT * FROM $DB_TABLES[moves] WHERE `id_zapisu`='" . $game->id . "' AND `gracz`='2' ORDER BY `ruch` ASC");

while ($row = mysql_fetch_array($result)){
    if ($row['litery'] != null || $row['punkty'] != '-1000' || $row['wyrazy'] != null ||$row['suma'] != '0') {
        $wyrazy = $row['wyrazy'];
        if ($row['punkty'] == '-1000'){
            $punkty = $suma = '';
        }
        else {
            $punkty = $row['punkty'];
            $suma   = $row['suma'];
        }

        if (strpos ($wyrazy, '-') === 0) {
            $wyrazy = str_replace ('-', '', $wyrazy);
        }
        print "
            <tr>
                <td>" . $row['ruch'] . "</td>
                <td class='alignleft'>" . $row['litery'] . "</td>
                <td class='alignleft'>$wyrazy</td>
                <td>$punkty</td>
                <td>$suma</td>
            </tr>";
    }
}

print '</table>';
require_once "files/php/bottom.php";

print '
    </body>
</html>';

    }

?>
