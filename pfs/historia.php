<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Scrabble : Historia SCRABBLE</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","historia");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Historia SCRABBLE")</script></h1>

Uznaje się, iż wynalazcą gry w 1933 roku jest bezrobotny architekt amerykański <b>Alfred Butts.</b><br />
<img src="files/img/alfredbutts.jpg" class="onright" alt="Alfred Butts" />
Kiedyś wpadło mu w rękę opowiadanie Edgara Allana Poe „Złoty żuk”, którego fabuła opiera się na poszukiwaniach pewnego ukrytego skarbu. Bohater musiał
złamać szyfr, w którym literom alfabetu odpowiadały znaki. Poe pisał: „W języku angielskim spotyka się najczęściej głoskę E. Po niej następują z kolei: AOIDHNRSTUYCFGLMWBKPQXZ”. To nasunęło Alfredowi pomysł: „a gdyby tak opracować grę językową opartą na najczęściej używanych literach?”. Swój wynalazek Butts nazwał <b>Lexiko</b>. Była to wczesna, prymitywniejsza wersja scrabble. Jednak żaden poważny producent gier nie był zainteresowany jej promocją. Alfred zaczął zatem działać na własną rękę. Przez miesiąc udało mu się sprzedać aż dwa komplety, każdy za półtora dolara.<br /><br />

Przez kilka następnych lat Butts doskonalił swój wynalazek. Próbował umieszczać pole startowe, oznaczane gwiazdką w prawym górnym rogu planszy, potem w lewym, aż wreszcie zdecydował się na środek. Początkowo rozdawał graczom po dziewięć liter, następnie zmniejszył ich liczbę do siedmiu. We wczesnych wersjach gry nie było tzw. blanka, później pojawił się jeden, wreszcie dwa blanki. Gra znajdowała nabywców, ale wieść o niej rozchodziła się wyłącznie drogą ustną i nie było mowy o masowej produkcji — sprzedaż szła w setki zestawów, nie w tysiące.<br /><br />

<img src="files/img/crisscross.jpg" class="onleft" alt="Criss Cross" />

Aż tu nagle, w roku 1947, pewien wyższej rangi urzędnik państwowy nazwiskiem <b>James Brunot</b> zagrał w Lexiko z żoną. Zaskoczony, że gra ta nie jest produkowana na skalę masową, zaproponował Alfredowi spółkę. Gra zmieniła nazwę na <b>Criss cross words.</b> Sześć lat później, kiedy Brunot zmienił nazwę gry na <b>Scrabble</b>, do amerykańskich sklepów z zabawkami trafiły jej pierwsze komplety.<br /><br />

Gra nie przyjęła się od razu. Decydujący okazał się dopiero moment, w którym <b>Jack Straus</b>, dyrektor dużej sieci domów towarowych Macy's, zetknął się ze scrabblami podczas wakacji w 1952 roku. Po powrocie do pracy stwierdził ze zgrozą, że w jego sklepach nie sprzedaje się tej gry i od razu zamówił kilka tysięcy sztuk. Za jego przykładem poszły mniejsze sklepy i w ciągu kilku miesięcy produkt Brunota i Buttsa zyskał rangę fenomenu. Pod koniec roku sprzedawano ponad dwa tysiące gier tygodniowo. W „Herald Tribune” pisano: „W mijającym roku 1953 wiele się działo: od objęcia urzędu prezydenta przez republikanina, aż po karierę gry w scrabble”. Pierwsze mistrzostwa Świata odbyły się w 1991 roku w Londynie, piąte w 1999 w Melbourne. James Brunot umarł w październiku 1984 roku i nie dożył pierwszych mistrzostw świata w scrabble. Alfred Butts umarł w kwietniu 1993 roku w wieku 93 lat.<br /><br />

<img src="files/img/gamefolio.jpg" class="onright" alt="Scrabble" />

Równe 50 lat po pierwszym sukcesie rynkowym magia scrabbli wciąż jest żywa. Na całym globie sprzedano dotąd ponad 100 mln. kompletów do gry. Działają 
dziesiątki tysięcy klubów scrabble. Co roku gracze z ponad 30 krajów uczestniczą w mistrzostwach świata.<br /><br />

W Polsce gry oparte na zasadach Scrabble pojawiły się na przełomie lat 70-tych i 80-tych. Najpierw (1979) była to „Krzyżanka” Lecha Pijanowskiego, następnie (1983) pojawiła się „Gra w krzyżówkę” Marka Penszki. W roku 1986 Jacek Ciesielski rozpoczął kilkuletnią popularyzację Scrabble w Polsce.
<img src="files/img/scrabbledeluxe.gif" class="onleft" alt="Scrabble Deluxe" />
W tej wersji po raz pierwszy można było układać inne części mowy niż rzeczowniki, aczkolwiek nadal zasady dopuszczalności słów były o wiele bardziej rygorystyczne niż obecne. I Ogólnopolski Turniej Scrabble pod patronatem tygodnika „Razem” odbył się 11 kwietnia 1987 roku w Warszawie. Turniej wygrał Arkadiusz Karasiński, zostając pierwszym nieoficjalnym Mistrzem Polski, a zadebiutowali na nim między innymi gracze znajdujący się później wysoko na liście rankingowej — Edward Stefaniak i Michał Derlacki. Pod patronatem „Razem” rozegrano jeszcze trzy Ogólnopolskie Turnieje Scrabble, których zwycięzcami zostali Andrzej Krystera (Poznań; listopad 1987), Ryszard Dębski (Zaborów; październik 1988) i Henryk Fido (Wałbrzych; październik 1989). W Poznaniu po raz pierwszy grano na obecnym zestawie liter, składającym się ze 100 płytek i nie zawierającym Q, V, ani X. W Wałbrzychu z kolei grano systemem „high score”, czyli na największą liczbę małych punktów, co wymuszało grę otwartą, bez blokowania. Mimo to układano wtedy mało skrabli — rekordowo 10 w 17 partiach, a niewyłożenie żadnego nie należało do rzadkości. W listopadzie 1989 na warszawskim Żoliborzu powstał pierwszy klub Scrabble, założony przez Michała Derlackiego, któremu na pierwszym spotkaniu towarzyszyli późniejszy czterokrotny mistrz Polski Tomasz Zwoliński i Marcin Skrzyniarz. Wkrótce dołączyli do nich Paweł Stanosz i Zdzisław Szota. <br /><br />

Od 1993 roku odbywają się Mistrzostwa Polski, a w 1997 roku powstała Polska Federacja Scrabble.<br /><br />

<div class="alignright">
	<i>Źródło:<br />Forum za Jonathan Maitland © The Spectator, 15.02.2003<br /><a href="http://www.scrabble.com" target="_blank">www.scrabble.com</a></i>
</div>
<?require_once "files/php/bottom.php"?>
</body>
</html>

