<?include_once "files/php/funkcje.php";
$sql_conn = pfs_connect ();?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Kluby : Rejestr klubów PFS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("kluby","rejestrklubow");</script>
  <style type="text/css">
    table.klub{
        border-spacing: 0;
        margin-top: 10px;

    }
    table.klub td{
        padding: 10px 10px 10px 0;
        vertical-align: top;
    }
    table.klub td:first-child{
        font-weight: bold;
        width: 200px;
    }
    #leftpanel{
        margin: 0 0 40px 0;
        height: 370px;
        width: 290px;
        float: left;
    }
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Rejestr klubów PFS")</script></h1>

<img src="files/mapaklubow.png"  style="float:left;" usemap="#mapa" alt="mapa" width="415px"/>
<p class="alignright">
    <a href="uchwalyklubowe.php">Uchwały dotyczące klubów</a><br />
    <a href="zgloszenie_klubu.php">Formularz zgłoszenia klubu do rejestru</a>
</p>

<map id="mapa" name="mapa">
    <area shape="circle" coords="171,  17, 5" href="#9" alt="" />
    <area shape="circle" coords="183,  31, 5" href="#13" alt="" />
    <area shape="circle" coords="232,  74, 5" href="#5" alt="" />
    <area shape="circle" coords="219,  91, 5" href="#21" alt="" />
    <area shape="circle" coords=" 21,  91, 5" href="#7" alt="" />
    <area shape="circle" coords=" 50, 139, 5" href="#3" alt="" />
    <area shape="circle" coords="116, 112, 5" href="#4" alt="" />
    <area shape="circle" coords="183, 120, 5" href="#22" alt="" />
    <area shape="circle" coords="167, 135, 5" href="#23" alt="" />
    <area shape="circle" coords="117, 162, 5" href="#19" alt="" />
    <area shape="circle" coords="142, 209, 5" href="#8" alt="" />
    <area shape="circle" coords="221, 203, 5" href="#2" alt="" />
    <area shape="circle" coords="150, 213, 5" href="#20" alt="" />
    <area shape="circle" coords="117, 243, 5" href="#14" alt="" />
    <area shape="circle" coords="347, 232, 5" href="#15" alt="" />
    <area shape="circle" coords="299, 297, 5" href="#17" alt="" />
    <area shape="circle" coords="287, 322, 5" href="#25" alt="" />
    <area shape="circle" coords="245, 317, 5" href="#12" alt="" />
    <area shape="circle" coords="230, 317, 5" href="#6" alt="" />
    <area shape="circle" coords="203, 304, 5" href="#10" alt="" />
    <area shape="circle" coords="179, 313, 5" href="#11" alt="" />
    <area shape="circle" coords="287, 170, 5" href="#16" alt="" />
    <area shape="circle" coords="222, 309, 5" href="#18" alt="" />
    <area shape="circle" coords="198, 283, 5" href="#26" alt="" />
    <area shape="circle" coords="262, 182, 5" href="#27" alt="" />
    <area shape="circle" coords="297, 170, 5" href="#42" alt="" />
    <area shape="circle" coords="161, 110, 5" href="#29" alt="" />
    <area shape="circle" coords="283,  81, 5" href="#30" alt="" />
    <area shape="circle" coords="307, 170, 5" href="#31" alt="" />
    <area shape="circle" coords="365, 107, 5" href="#36" alt="" />
    <area shape="circle" coords="375, 107, 5" href="#38" alt="" />
    <area shape="circle" coords="10, 64, 5" href="#39" alt="" />
    <area shape="circle" coords="273, 174, 5" href="#37" alt="" />
</map>


<?$result = mysql_query("SELECT * FROM $DB_TABLES[clubs] ORDER BY data DESC");
while($row = mysql_fetch_array($result)){
    print "<h2 id='".$row['id']."'>".$row['nazwa']."</h2>";
    $photo = pfs_photo_read ($DB_TABLES[clubs], $row[id]);
    if ($photo) {
        print "<img class='onright' src='foto_action.php?id=$photo->id' style='width:$photo->photo_width"."px;' alt='$row[nazwa]' />";
    }

    print "<table class='klub'><tr>";
    if ($row['adres']!='')  {
        print "<td>Miejsce spotkań</td><td>".$row['adres'];
        if ($row['mapka']!='')  print "<br><a href='".$row['mapka']."' target='_blank'><b>mapka</b></a>";
        print "</td></tr>";
    }
    if ($row['terminy']!='') print "<tr><td>Terminy spotkań</td><td>".$row['terminy']."</td></tr>";
    if ($row['liczbaczlonkow']!='') print "<tr><td>Liczba członków klubu</td><td>".$row['liczbaczlonkow']."</td></tr>";
    if ($row['stronawww']!='')  print "<tr><td>Strona www</td><td><a href='".$row['stronawww']."' target='_blank'>".$row['stronawww']."</a></td></tr>";
    if ($row['kontakt']!='')    print "<tr><td>Kontakt z klubem</td><td>".$row['kontakt']."</td></tr>";
    if ($row['wladze']!='') print "<tr><td>Władze klubu</td><td>".$row['wladze']."</td></tr>";
    if ($row['data']!='0000-00-00') print "<tr><td>Data wpisania do rejestru</td><td>".$row['data']."</td></tr>";
    print "</table>";
}

require_once "files/php/bottom.php"?>
</body>
</html>
