<?include "../files/php/funkcje.php"?>

<html>
<head>
	<title>Polska Federacja Scrabble :: OSPS uzupełnienia Luty 2009</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("scrabble","osps");</script>
	
	<style>
		td{
			padding: 1px 16px;
		}
		th{
			font-weight: bold;
			padding: 8px 16px;
			text-align: left;
			vertical-align: top;
		}
		table{
			margin-top: 20px;
		}
	</style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1><script>naglowek("Uzupełnienie OSPS")</script></h1>
Lista słów, które zostaną dodane do słownika wraz z nowym update'em - są to słowa, które powinny znajdować się w słowniku OSPS,<br>a ich odmiana została pominięta pomimo obecności w słownikach źródłowych.</span>


<table>
	<tr><th>Słowo</th><th>Forma podst.</th><th>Słowo</th><th>Forma podst.</th></tr>
	<tr><td>CZARODZIEI</td><td>CZARODZIEJ</td><td>PARKINA</td><td>PARKIN</td></tr>
	<tr><td>DŻAMBO</td><td>DŻAMBO</td><td>PYKANA</td><td>PYKAĆ</td></tr>
	<tr><td>DŻETA</td><td>DŻET</td><td>PYKANĄ</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANA</td><td>PYKAĆ</td><td>PYKANE</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANĄ</td><td>PYKAĆ</td><td>PYKANEGO</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANE</td><td>PYKAĆ</td><td>PYKANEJ</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANEGO</td><td>PYKAĆ</td><td>PYKANEMU</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANEJ</td><td>PYKAĆ</td><td>PYKANI</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANEMU</td><td>PYKAĆ</td><td>PYKANY</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANI</td><td>PYKAĆ</td><td>PYKANYCH</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANY</td><td>PYKAĆ</td><td>PYKANYM</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANYCH</td><td>PYKAĆ</td><td>PYKANYMI</td><td>PYKAĆ</td></tr>
	<tr><td>NIEPYKANYM</td><td>PYKAĆ</td><td>ZROZUMCIE</td><td>ZROZUMIEĆ</td></tr>
	<tr><td>NIEPYKANYMI</td><td>PYKAĆ</td><td>ZROZUMIEJCIE</td><td>ZROZUMIEĆ</td></tr>
	<tr><td>OBSKURĄ</td><td>OBSKURA</td><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>OBSKURA</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURACH</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURAMI</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKUR</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURĘ</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURO</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKUROM</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURY</td><td>OBSKURA</td></td><td></td><td></tr>
	<tr><td>OBSKURZE</td><td>OBSKURA</td></td><td></td><td></tr>
</table>


<?require_once "../files/php/bottom.php"?>
</body>
</html>
