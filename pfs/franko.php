<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Różności :: Scrabble frankofońskie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","franko");</script>
 <style type="text/css">
 	p.wstep{
		font-style:italic;
	}
 </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Scrabble frankofońskie")</script></h1>

L'histoire a commencé dans les années 70 quand Wojciech Pijanowski (connu pour animer
le jeu télévisé < La Roue de la fortune >) a créé le jeu < Krzyżanka > (petits mots croisés). En
1986, Jacek Ciesielski commence a populariser le Scrabble en Pologne. Avec le soutien du
magazine Razem (< Ensemble >) il a organisé en 1987 un premier tournoi de Scrabble, mais
parmi les lettres des jeux on trouvait Q, X, V qui n'existent que dans les mots d'origine anglaise.
En 1989 a été créée la premičre version polonaise du Scrabble avec 100 lettres. Cette année-
lŕ, Tomasz Zwoliński et Marcin Skyniarz ont créé le premier club a Varsovie. Des 1993 est
organisé un championnat de Pologne, et en 1997 nait la Fédération Polonaise de Scrabble.
Tomasz Zwoliński, l'un des premiers scrabbleurs a gagné ce championnat 4 fois. En Pologne,
on joue seulement en Scrabble classique. Il n'y a pas de pénalité lors d'une contestation
injustifiée. Chaque joueur a 20 minutes pour toute la partie. Une trentaine de tournois est
organisée chaque année sous l'égide de la PFS dans tout le pays.<br><br>

Il existe également un Grand Prix (remporté par le joueur marquant le plus de points sur
un circuit de 12 tournois (représentés par une étoile sur la carte), un Championnat des
enseignants, un Championnat scolaire (primaire et secondaire), la Coupe de Pologne (ŕ
élimination directe) et un championnat interclubs (il existe une quarantaine de clubs). Le jeu en
langues étrangčres n'est pas encore trčs populaire mais cette situation va peut-ętre changer ŕ
l'avenir!<br><br>

Le classement est basé sur la méthode de Hollington. La cote est établie sur la base des 200
derničres parties jouées au cours des deux derničres années. On compte 50 points en cas de
défaite, 100 points en cas de match nul et 150 en cas de victoire. De plus, on ajoute des points
si le joueur avec qui on joue a une cote supérieure ŕ 100, par exemple joueur X (cote 100)
rencontre le joueur Y (129). En cas de défaite, il marque 79 points (50+29), en cas de victoire il
marque 179 (150+29).<br><br>

L'OSPS (le dictionnaire officiel du Scrabble polonais) est la référence en tournoi. Il compte prčs
de 2 millions de mots ! La lettre la plus chčre est Ź qui compte 9 points.<br><br>

Liste des mots de 2 lettres:<br>
<pre>
AD AG AJ AL AR AS AT AU AŻ
BA BE BO BU BY
CE CI CO
DA DE DO
EF EH EJ EL EŁ EM EN EŃ ER ES EŚ ET EW
FA FE FI FU
GĘ GO
HA HE HĘ HI HM HO HU
ID II IŁ IM IN IW IŻ
JA JĄ JE
KA KI KO KU
LA LI LU
MA MĄ ME MI MU MY
NA NI NO NY
OD OJ OK OM ON OŃ OP OR OS OŚ OT OZ OŻ
ÓD ÓS ÓW
PA PE PI PO
RE
SĄ SE SI SU
TA TĄ TE TĘ TO TS TU TY
UD UF UL UT
WE WU WY
ZA ZE
ŻE
</pre>
<br>
Miłosz Wrzałek
(d'apres le site de la fédération polonaise de Scrabble ( www.pfs.org.pl)




<?require_once "files/php/bottom.php"?>
</body>
</html>

