<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Paulina_Naplocha</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Odeszła Paulina Naplocha</h1>

<center><img src="../rozne/Babcia.jpg"></center><br> <br>
Z wielkim smutkiem zawiadamiamy, że 5 listopada 2014, w wieku 80 lat odeszła od nas jedna z najwytrwalszych scrabblistek, Paulina Naplocha. Paulina była wspaniałą, ciepłą osobą, która bardzo kochała swoją rodzinę. Kochała też Scrabble - rozegrała 49 turniejów towarzysząc swoim córkom i wnukom w scrabblowych wojażach, czasem w bardzo odległe miejsca w Polsce. Nierzadko odbierała nagrodę dla najstarszego uczestnika! Aktywnie uczestniczyła też w życiu raciborskiego klubu SZKRAB. <br> <br>Uroczystość żałobna odbędzie się na Cmentarzu Jeruzalem w Raciborzu, ul.Ocicka 100, w sobotę 8 listopada 2014 r. o godzinie 14.00.


<?require_once "../files/php/bottom.php"?>
</body>
</html>
