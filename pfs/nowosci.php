<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Nowości na stronie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("glowna","anagramator");</script>
	<script>
		function go(url){
			/*url = url.replace("'", "&#39;");
			url = url.replace("&apos;", "&#39;");*/
			window.opener.location.href = url;
		}
	</script>
	<style>
		h2{
			text-align: center;
			margin-top: 10px;
		}
		#nowosci{
			background: url("files/img/bkg-nowosci.png") repeat-y scroll;
			padding: 10px;
			height: 100%;
			width: 580px;
			margin-top: -30px;
		}

		h1{
			background: url("files/img/top-nowosci.png") no-repeat scroll;
			margin-top: 0px;
			padding: 30px 10px;
			height: 30px;
			width: 580px;
		}
	</style>
	
</head>

<body onLoad="javascript: window.focus()" onUnload="javascript: window.opener.focus()">


<h1><script>naglowek("Nowości na stronie")</script></h1>

<div id="nowosci">



<h2>10 sierpnia</h2><ul><li>Siódemka tygodnia — <a href=javascript:go(&quot;siodemka.php&quot;)>UNISEKS</a></li></ul>

<h2></h2>
<!--
<ul><li><a href=javascript:go(&quot;rozne/konkurs2015.php&quot;)>Konkurs na organizację imprez centralnych w 2015 roku</a></li> - szczegóły organizacji imprez i wsparcia dla organizatorów</ul>
-->
<!--
<h2></h2><ul><li><a href=javascript:go(&quot;turniej.php?id=803#info&quot;)>XXII Mistrzostwa Polski</a> zapisy i informacje</li></ul>-->




</div>
</body>
</html>
