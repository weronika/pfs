<?include_once "files/php/funkcje.php";?>

<?
if (!$_GET['s']) {
    $siodemka = pfs_select_one (array (
        table   => $DB_TABLES[sevens],
        fields  => array ('nazwa_pliku', 'siodemka'),
        where   => array ( aktywna => 1 )
    ));
}
else {
    $siodemka = pfs_select_one (array (
        table   => $DB_TABLES[sevens],
        fields  => array ('nazwa_pliku', 'siodemka'),
        where   => array ( nazwa_pliku => $_GET['s'] )
    ));
}

$lit = $_GET['lit'] ? $_GET['lit'] : $siodemka->nazwa_pliku[0];
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Siódemka tygodnia : <? print $siodemka->siodemka;?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("glowna","siodemka");</script>
    <style type="text/css">
        span.siodemka{
            margin: 40px 200px 0 200px;
            letter-spacing: 2px;
            text-align: center;
            color: #E7484F;
            font-size: 15px;
            font-weight: bold;
            display: block;
        }
        span.siodemka img{
                vertical-align: middle;
        }
        .slowa span{
            color: #E7484F;
            font-weight: bold;
        }
        img{
        }
        #alfabet{
            float: left;
            list-style:none;
            margin: 0;
            text-align: center;
            color: #555;
        }
        #alfabet option{
            padding: 0 8px;
        }
        #galeria7{
            padding: 0;
            text-align: left;
            line-height: 18px;
            margin-left: 50px;
            font-family: "courier new";
            font-size: 13px;
        }
        #galeria7 a{
            padding: 0 10px;
        }
    </style>
    <script>
        function zmianaLitery(sel, siodemka){
            opcje = sel.options;
            for(i=0; i<opcje.length; i++)
                if(opcje[i].selected)
                    litera = opcje[i].value;
            document.location="siodemka.php?s="+siodemka+"&lit="+litera;
        }

        function literki(siodemka){
            for (i=0; i<7; i++){
                a = (siodemka.charAt(i)).toLowerCase();
                switch(a){
                    case 'ą': a='a5'; break;
                    case 'ć': a='c6'; break;
                    case 'ę': a='e5'; break;
                    case 'ł': a='l3'; break;
                    case 'ń': a='n7'; break;
                    case 'ó': a='o5'; break;
                    case 'ś': a='s5'; break;
                    case 'ż': a='z5'; break;
                    case 'ź': a='z9'; break;
                    default:  break;
                }
                document.write("<img src='files/tiles/" + a + ".png' alt='" + a + "'>");
            }
        }
    </script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Siódemka tygodnia")</script></h1>

<?
print "<select id='alfabet' onChange='zmianaLitery(this, \"$siodemka->nazwa_pliku\")'>";
print "<option value='A' "; if($lit == 'A') print "selected='selected'"; print ">A</option>";
print "<option value='B' "; if($lit == 'B') print "selected='selected'"; print ">B</option>";
print "<option value='C' "; if($lit == 'C') print "selected='selected'"; print ">C</option>";
print "<option value='D' "; if($lit == 'D') print "selected='selected'"; print ">D</option>";
print "<option value='E' "; if($lit == 'E') print "selected='selected'"; print ">E</option>";
print "<option value='F' "; if($lit == 'F') print "selected='selected'"; print ">F</option>";
print "<option value='G' "; if($lit == 'G') print "selected='selected'"; print ">G</option>";
print "<option value='H' "; if($lit == 'H') print "selected='selected'"; print ">H</option>";
print "<option value='I' "; if($lit == 'I') print "selected='selected'"; print ">I</option>";
print "<option value='J' "; if($lit == 'J') print "selected='selected'"; print ">J</option>";
print "<option value='K' "; if($lit == 'K') print "selected='selected'"; print ">K</option>";
print "<option value='L' "; if($lit == 'L') print "selected='selected'"; print ">L</option>";
print "<option value='Ł' "; if($lit == 'Ł') print "selected='selected'"; print ">Ł</option>";
print "<option value='M' "; if($lit == 'M') print "selected='selected'"; print ">M</option>";
print "<option value='N' "; if($lit == 'N') print "selected='selected'"; print ">N</option>";
print "<option value='O' "; if($lit == 'O') print "selected='selected'"; print ">O</option>";
print "<option value='P' "; if($lit == 'P') print "selected='selected'"; print ">P</option>";
print "<option value='R' "; if($lit == 'R') print "selected='selected'"; print ">R</option>";
print "<option value='S' "; if($lit == 'S') print "selected='selected'"; print ">S</option>";
print "<option value='Ś' "; if($lit == 'Ś') print "selected='selected'"; print ">Ś</option>";
print "<option value='T' "; if($lit == 'T') print "selected='selected'"; print ">T</option>";
print "<option value='U' "; if($lit == 'U') print "selected='selected'"; print ">U</option>";
print "<option value='W' "; if($lit == 'W') print "selected='selected'"; print ">W</option>";
print "<option value='Z' "; if($lit == 'Z') print "selected='selected'"; print ">Z</option>";
//print "<option value='Ź' "; if($lit == 'Ź') print "selected='selected'"; print ">Ź</option>";
print "<option value='Ż' "; if($lit == 'Ż') print "selected='selected'"; print ">Ż</option>";
print "</select>";

$result = mysql_query("SELECT * FROM $DB_TABLES[sevens] WHERE siodemka LIKE '".$lit."%' ORDER BY siodemka ASC");
print "<div id='galeria7'>";
while($row = mysql_fetch_array($result))
    print "<a href='siodemka.php?s=".$row['nazwa_pliku']."&lit=".$lit."'>".$row['siodemka']."</a> ";
print "</div>";

$id7sort = array();
$result = mysql_query("SELECT * FROM $DB_TABLES[sevens] ORDER BY siodemka ASC");
while($row = mysql_fetch_array($result))
    $id7sort[] = $row['nazwa_pliku'];
mysql_free_result($result);

$i=0;
$prev = -1;
$next = 1;

for ($i=0; $i< count($id7sort); $i++, $prev++, $next++)
    if ($id7sort[$i] == $siodemka->nazwa_pliku)
        break;

$result = mysql_query("SELECT * FROM $DB_TABLES[sevens] WHERE nazwa_pliku='$siodemka->nazwa_pliku'");

    //    $path = "siodemki/".strtolower($row['nazwa_pliku']).".jpg";
    //    list($width, $height, $type, $attr) = getimagesize($path);
    //    print "<div class='onright' style='width:".$width."px;'><img class='onright' src=".$path." alt='".$row['siodemka']."' />";
    //    if($row['zrodlo'] != null)
    //        print "<div style='padding:1px 5px;width:".($width-10)."px;margin-top:-4px;float:right;background:black;color:#bbbbbb;text-align:right;font-style:italic;font-size:11px;font-family:Arial;'><b>Źródło zdjęcia:</b><br>".$row['zrodlo']."</div>";
    //    print "</div>";
    //}

while($row = mysql_fetch_array($result))
{
    $photo = pfs_photo_read ('sevens', $row[id]);

    print "<span class='siodemka'>";
    if($prev > -1) print "<a href='siodemka.php?s=".$id7sort[$prev]."&lit=".$lit."' style='margin-right:60px;'><img src='files/img/prev.png'></a>";
    print "<script>literki('".$row['siodemka']."')</script>";
    if ($next < count($id7sort)) print "<a href='siodemka.php?s=".$id7sort[$next]."&lit=".$lit."' style='margin-left:60px;'><img src='files/img/next.png'></a>";
    print "</span>";
    print "<h2>Znaczenie</h2>";

    print "<div class='onright' style='width:$photo->photo_width"."px;'><img class='onright' src='foto_action.php?id=$photo->id' alt='$row[siodemka]' />";

    if($row['zrodlo'] != null)
        print "<div style='padding:1px 5px;width:".($width-10)."px;margin-top:-4px;float:right;background:black;color:#bbbbbb;text-align:right;font-style:italic;font-size:11px;font-family:Arial;'><b>Źródło zdjęcia:</b><br>".$row['zrodlo']."</div>";
    print "</div>";
    print $row['znaczenie']."<br>";
    ?>

    <?
    print "<h2>Odmiana</h2>".$row['odmiana'];
    if($row['przedluzki'] == "brak") print "<h2>Przedłużki</h2>".$row['przedluzki'];
    else print "<h2>Przedłużki</h2><div class='slowa'>".$row['przedluzki']."</div>";
    if($row['pochodne'] != null) print "<h2>Wyrazy pochodne</h2><div class='slowa'>".$row['pochodne']."</div>";
    if($row['anagramy'] == "brak") print "<h2>Anagramy</h2>".$row['anagramy'];
    else print "<h2>Anagramy</h2><div class='slowa'>".$row['anagramy']."</div>";
    if($row['zblankiem'] == "brak") print "<h2>Anagramy z blankiem</h2>".$row['zblankiem'];
    else print "<h2>Anagramy z blankiem</h2><div class='slowa'>".$row['zblankiem']."</div>";
    print "<h2>Subanagramy</h2><div class='slowa'>".$row['subanagramy']."</div>";
}

?>
<div style="clear: both;"></div>

<?require_once "files/php/bottom.php"?>
</body>
</html>
