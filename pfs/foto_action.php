<?php
include_once "files/php/funkcje.php";
$sql_conn = pfs_connect ();

if (isset ($_GET['id'])) {
    $photo = pfs_select_one (array (
        table   => $DB_TABLES[photos],
        fields  => array ('photo_type', 'photo_content'),
        where   => array ( 'id' => $_GET['id'] )
    ));

    header("Content-type: $photo->photo_type");
    echo $photo->photo_content;
}
?>
