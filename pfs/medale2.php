<?include_once "files/php/funkcje.php";?>

<?
if(isset($_GET['rok']))	$rok = $_GET['rok'];
else	$rok = date('Y');
?>
<html>
<head>
	<title>Polska Federacja Scrabble :: Ranking : Klasyfikacja medalowa</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("ranking","medale");</script>
   	<style type="text/css">
		table td.lp{
			text-align: right;
		}
		table td.osoba{
			text-align: left;
		}
   	</style>
	<script>
		function zmianaRoku(sel){
			opcje = sel.options;
			for(i=0; i<opcje.length; i++)
				if(opcje[i].selected)
					rok = opcje[i].value;
			document.location="medale.php?rok="+rok;
		}
	</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Klasyfikacja medalowa")</script></h1>

<div class='alignright'>
Wybierz rok: <select onchange='zmianaRoku(this)'>
<?
for ($y=date('Y'); $y>=1993; $y--){
	print "<option value='".$y."' ";
	if($rok == $y) print "selected='selected'";
	print ">".$y."</option>";
}
?>
</select>
</div>

<div style="float: right;">
<h2>Klasyfikacja medalowa <?print $rok;?></h2>
<table class="klasyfikacja">
	<tr>
		<td>&nbsp;</td>
		<td class='osoba'>Imię i nazwisko</td>
		<td><img src="files/img/medal_gold.png" alt="1" /></td>
		<td><img src="files/img/medal_silver.png" alt="2" /></td>
		<td><img src="files/img/medal_bronze.png" alt="3" /></td>
	</tr>

<?
$sql_conn = pfs_connect ();

$result = mysql_query("SELECT zwyciezca, miejsce2, miejsce3 FROM $DB_TABLES[tours] WHERE YEAR(data_do)='".$rok."' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
	if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
		print "<td class='lp'>".$i."</td>";
	else
		print "<td class='lp'>&nbsp;</td>";
	$i++;
	$pop1 = $row['zlote'];
	$pop2 = $row['srebrne'];
	$pop3 = $row['brazowe'];
	print "<td class='osoba'>".$row['osoba']."</td>";
	print "<td>".$row['zlote']."</td>";
	print "<td>".$row['srebrne']."</td>";
	print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>
 </div>

<div style="float: left;">
<h2 id="wszech">Klasyfikacja medalowa wszech czasów (stan na rok <?print $rok;?>)</h2>
<table class="klasyfikacja">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
	<td><img src="files/img/medal_gold.png" alt="1" /></td>
	<td><img src="files/img/medal_silver.png" alt="2" /></td>
	<td><img src="files/img/medal_bronze.png" alt="3" /></td>
    <td class='suma'>suma</td>
  </tr>

<?
$result = mysql_query("SELECT osoba, SUM(zlote) as 'zlote', SUM(srebrne) as 'srebrne', SUM(brazowe) as 'brazowe' FROM $DB_TABLES[medals] WHERE rok<='".$rok."' GROUP BY osoba ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
	if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
		print "<td class='lp'>".$i."</td>";
	else
		print "<td class='lp'>&nbsp;</td>";
	$i++;
	$pop1 = $row['zlote'];
	$pop2 = $row['srebrne'];
	$pop3 = $row['brazowe'];
	print "<td class='osoba'>".$row['osoba']."</td>";
	print "<td>".$row['zlote']."</td>";
	print "<td>".$row['srebrne']."</td>";
	print "<td>".$row['brazowe']."</td>";
	$suma = (int)$row['zlote'] + (int)$row['srebrne'] + (int)$row['brazowe'];
	print "<td class='suma'>".$suma."</td></tr>";
}

mysql_free_result($result);
mysql_close($sql_conn);
?>
</table>
</div>

<?require_once "files/php/bottom.php"?>
</body>
</html>
