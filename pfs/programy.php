<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Różności : Programy sędziowskie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","programy");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Programy sędziowskie")</script></h1>

<img src="files/img/starykops.jpg" class="onright" alt="" />
Przez pierwsze cztery lata rozgrywania turniejów (1993-1996) wszystkie czynności (rozstawienie, wyniki statystyka) były prowadzone ręcznie. Obecnie to trudno zrozumieć, ale czas oczekiwania na rozstawienie do kolejnej rundy sięgał
czasem kilkudziesięciu minut i rozstawienie było odczytywane. Na II Mistrzostwach Polski wykorzystany został program autorstwa <b>Grzegorza Wiączkowskiego,</b> który potem głównie z powodów sprzętowych nie był stosowany.<br /><br />
W 1997 roku kiedy zwiększyła się radykalnie zarówno ilość turniejów jak i ich długość (ilość rozgrywanych rund), z inicjatywy <b>Sławka Latały</b> został zmodyfikowany dla potrzeb scrabble, pracujący w środowisku DOS, program do prowadzenia turniejów brydżowych tzw. <b>KoPS</b> (<b>K</b>omputerowy <b>Po</b>mocnik <b>S</b>ędziego) autorstwa <b>Jana Romańskiego</b>. Program ten pozwalał nie tylko na sprawne przeprowadzenie turnieju, druk zarówno rozstawień, protokołów i wyników, ale również aktualizował ranking i zawierał bazę turniejów i zawodników. Program sprawdzał się znakomicie do momentu, kiedy wielkość bazy i ilość rozgrywanych rund (maraton) przekroczyła możliwości programu DOSowego.<br /><br />

<img src="files/img/scrabblemanager.jpg" class="onleft" alt="" />
Modyfikacje nie pomogły na długo i na początku 2003 roku wyzwanie napisania programu spełniającego wszystkie oczekiwania podjął <b>Szymon Zaleski</b>. <b>STM</b> (<b>S</b>crabble <b>T</b>ournament <b>M</b>anager) to program pozwalający na prowadzenie turnieju różnymi systemami, posiadający praktycznie nieograniczona bazę danych z której można wyciągnąć wszystkie interesujące zestawienia dotyczące turniejów, zawodników i gier. Program oczywiście liczy i aktualizuje ranking. Tym programem są obecnie prowadzone wszystkie turnieje rankingowe PFS. Mimo swoich niewątpliwych zalet, STM ma również pewne mankamenty. Dla szybkiego działania wymaga dobrego procesora i dużej ilości pamięci RAM. Również sama instalacja programu jest nieco kłopotliwa. Program jest dalej w fazie udoskonaleń.<br /><br />

Dla potrzeb klubów scrabblowych tak rozbudowany program jest niepotrzebny. Były mistrz Polski <b>Kamil Górka</b> cicho i bez rozgłosu napisał znakomity programik pozwalający prowadzić turnieje, liczyć ranking i generować statystyki gier pomiędzy poszczególnymi zawodnikami. To idealny program dla klubów i kółek scrabblowych. Zajmuje bardzo mało miejsca, instaluje się błyskawicznie, działa szybko i nie ma wygórowanych wymagań sprzętowych. Statystyki gier zapisane są w formacie HTML i bez żadnego kłopotu można je prezentować na stronach www. Aktualną wersję programu <b>Turniej 1.4</b> jak również współpracującego
z nim programu do generowania statystyk <b>Statystyki</b> można ściągnąć bezpłatnie ze strony <a href="http://kluby.pfs.org.pl/krakow/download.htm" target="_blank">Krakowskiego Klubu Scrabble</a>.<br />
Autor zapowiada rozbudowę swojego programu o inne przydatne w sędziowaniu opcje.

<?require_once "files/php/bottom.php"?>
</body>
</html>

