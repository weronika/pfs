<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Gry i programy słowne</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("glowna","gryiprogramy");</script>
  <style type="text/css">
	#info{
		padding-bottom: 16px;
		margin-bottom: 20px;
	}
	#info img{
		float: left;
		margin-right: 40px;
	}
	img.em{
		margin: 6px;
		vertical-align: middle;
	}
  </style>
</head>


<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Gry i programy")</script></h1>
<h2>Gry</h2>
<div id="info">
	<a href="http://www.slownepotyczki.pl/"><img src="rozne/slowne_pot.jpg" alt="Słowne potyczyki" target="_blank" /></a>
	<b>Potyczki słowne</b><br /><br />
	Przejmuj słowa i gromadź punkty w grze online.
</div><br>
<h2>Programy</h2>
<div id="info">
	<a href="http://www.facebook.com/pages/Anagramistrz-by-Cybercom/337777969622606"><img src="rozne/anagramistrz.gif" alt="Anagramistrz" target="_blank" /></a>
	<b>Anagramistrz</b><br /><br />
	Aplikacja na smartfony - sprawdź swoje umiejętności w anagramowaniu wyrazów.
</div><br>

<?require_once "files/php/bottom.php"?>
</body>
</html>
