<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Scrabble w szkole</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","scrabble w szkole");</script>
 <style type="text/css">
 	p.wstep{
		font-style:italic;
	}
 </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Turniej Mikołajkowy")</script></h1>

<div class='naglowekrelacji'><span class='tytul'>Serdecznie zapraszamy do udziału w VII Turnieju Mikołajkowym Scrabble!</span><br></div>
    
     <div>
To wyjątkowa impreza, różniąca się od innych rozgrywek organizowanych w ciągu roku. Podczas tego spotkania grają ze sobą młodsi ze starszymi, gracze bardziej doświadczeni z nowicjuszami. Ideą spotkania jest dobra zabawa, integracja, zaprawienie z atmosferą turniejową. 
 <br>
W turnieju mogą brać udział tylko osoby należące do Kół Scrabble zarejestrowanych w Programie Scrabble(R) w szkole.<br>
Warunkiem uczestnictwa jest zgłoszenie szkoły do rozgrywek przez Opiekuna Koła za pośrednictwem maila wysłanego na adres <a href="mailto:mikolajki@scrabblewszkole.pl">mikolajki@scrabblewszkole.pl</a> <br>
W treści maila prosimy o podanie:<br>
1. Nazwy szkoły<br>
2. Swojego aktualnego numeru telefonu kontaktowego, adresu mailowego<br>
3. Imion i nazwisk zgłaszanych dzieci<br><br>
 
Z powodu ograniczonej ilości miejsc każda ze szkół może zgłosić od 1 do 10 uczniów. Jeżeli istnieje takie życzenie większą ilość dzieci możemy dodać do tzw. listy rezerwowej.
O zakwalifikowaniu do Turnieju decyduje data otrzymania zgłoszenia.<br><br>
 
Dla wszystkich uczestników atrakcyjne zestawy upominków!
Niektórzy wrócą do domów z pucharami! Wspaniała zabawa w świątecznych nastrojach zapewniona!<br><br>
 
Sobota, 13 grudnia 2014<br>
Zespół Szkół nr 5 im. Stefana Kisielewskiego<br>
Ul. Szczawnicka 1<br>
04-089 Warszawa<br><br>
 
Czas trwania turnieju: godziny 10:00 - 17:00<br>
 
PROGRAM:<br>
 
10:00 - 11:00 -- rejestracja uczestników<br>
 
11:00 - 13:00 -- rozpoczęcie turnieju, rozgrywki (1-2 runda)<br>
 
13:00 - 14:00 -- przerwa obiadowa<br>
 
14:00 - 16:00 -- rozgrywki (3-4 runda)<br>
 
ok. 16:30 -- zakończenie turnieju<br><br>
 
Najdogodniejszy dojazd do szkoły z centrum Warszawy autobusami linii 158 lub 525. W czasie trwania imprezy zapewniamy obiad i przekąski. Na zgłoszenia czekamy do 10 GRUDNIA 2014 lub do momentu wyczerpania limitu miejsc (100 osób).
Szkoły zostaną poinformowane o potwierdzeniu udziału w imprezie drogą mailową.<br><br>
 
Organizatorzy Turnieju Mikołajkowego 2014 to: Mattel, Polska Federacja Scrabble oraz Zespół Szkół nr 5 im. Stefana Kisielewskiego w Warszawie.
 <br><br>
Serdecznie zapraszamy!

     </div><br> <br>
     


<?require_once "files/php/bottom.php"?>
</body>
</html>

