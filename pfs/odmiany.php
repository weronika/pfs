<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Scrabble : Odmiany SCRABBLE</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","odmiany");</script>
  <style type="text/css">
  	h3{
		margin: 12px 0 4px 0;
		font-weight: bold;
		font-size: 12px;
	}

  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Odmiany SCRABBLE")</script></h1>

<h2>Belgijka</h2>
<img src="files/img/belgijka.jpg" class="onright" alt="belgijka" />
Ta odmiana gry została wymyślona przez belga Hipolita Woutersa. W grze może uczestniczyć dowolna liczba osób.
<h3>Zasady gry</h3>
Każdy ruch wykonywany jest przez wszystkich graczy z takiego samego zestawu liter. Wspólny zestaw umieszcza się na jednym stojaku i każdy z graczy próbuje ułożyć z tych liter jak najwyżej punktowany ruch. Po każdym ruchu sprawdza się, który z graczy wymyślił najdroższy ruch (wartość punktowa), a następnie układa się ten ruch na planszy (w przypadku równej liczby punktów przyjęte jest, aby wybrać najbardziej rozwojowy ruchu dla gry). Gracze natomiast otrzymują tyle punktów, ile wart był wymyślony przez nich ruch. Na stojak dolosowuje się brakującą ilość liter i ponawia procedurę wyboru ruchu. Gra kończy się, gdy wyczerpią się wszystkie litery z woreczka, lub gdy nikt nie potrafi wymyślić ruchu, który można wyłożyć na planszę. Zwycięzcą zostaje osoba, która podczas całej gry zgromadziła najwięcej punktów.
<h2>Belgijka turniejowa</h2>
Na turniejach stosuje się dokładniejszą procedurę przebiegu gry, aby ustrzec się przed wszelkimi nieprawidłościami. Wówczas gra przebiega następująco:<br />
Każdy z graczy ma przed sobą swój własny zestaw do gry oraz planszę. Osoba prowadząca belgijkę każdorazowo dolosowuje litery na stojak główny podając je jednocześnie głośno wszystkim uczestnikom, a Ci wybierają wskazane litery z własnych zestawów. Gracze umieszczają litery na swoich stojakach i w wyznaczonym czasie na ruch (np. 2-3 minuty) starają się wymyślić jak najdroższy ruch. Na ogół przyjmuje się zasadę, że przez pierwszych kilkanaście ruchów na stojaku muszą znajdować się co najmniej dwie spółgłoski i co najmniej dwie samogłoski (blank może być uznany za spółgłoskę lub samogłoskę). Jeśli stojak nie zawiera odpowiednich liter w myśl tej zasady, wówczas wszystkie litery ze stojaka trafiają do worka, a prowadzący dolosowuje na stojak nowy zestaw siedmiu liter. Do obowiązków zawodnika należy w wyznaczonym czasie zapisać wymyślony przez siebie ruch na karcie zapisu wraz z wartością punktową oraz miejscem ruchu na planszy (np. B4 pionowo). Nie wskazane jest układanie w czasie przewidzianym na ruch płytek na planszy, aby uniemożliwić innym graczom podejrzenia planowanego ruchu. Po zakończonym czasie na ruch prowadzący belgijkę wybiera najdroższy ruch i umieszcza go na planszy głównej, a każdy z graczy analogicznie na własnej. Na ogół gracze dobierani są w pary, aby w wypadku wątpliwości partnera co do poprawności ruchu mógł on go zakwestionować i zgłosić sędziemu. Gra podzielona jest na rundy (jedna runda to przeważnie 5 ruchów). Po każdej rundzie gracze sumują zdobyte przez siebie punkty za ruchy i podają ich sumę sędziemu. Wygrywa osoba, która w ciągu wszystkich rund zgromadzi największą ilość punktów.

<h2>Karpatka</h2>
Karpatka to jedna z najciekawszych odmian scrabble. Wymyślona przez Macieja Czupryniaka i Andrzeja Lożyńskiego podczas wczasów scrabblowych w Karpaczu w 1998 roku , z późniejszymi modyfikacjami autorstwa Pawła Stanosza i Dariusza Banaszka. 
<h3>Zasady gry</h3>
W grze uczestniczą dwa dwuosobowe zespoły. Każdy z graczy ma swój stojak, wszyscy grają przy wspólnej planszy oraz przy użyciu jednego kompletu liter. Każda drużyna tak trzyma stojaki, aby partner widział litery współgracza. Drużyny wykonują ruchy na przemian, raz jedna para (dwa ruchy z obu stojaków z osobna stanowią ruch drużyny), raz druga. Wyjątek stanowi pierwsza kolejka, kiedy to rozpoczynająca drużyna ma prawo wykonać tylko jeden ruch. Kolejność ruchów graczy w drużynie przypadających na jedną kolejkę jest dowolna. Podstawową zasadą karpatki jest to, że gracze porozumiewają się ze sobą tylko i wyłącznie przy pomocy własnych stojaków z literami. Nie można rozmawiać czy gestykulować, dlatego ważnym elementem gry jest wymyślenie zrozumiałego i komunikatywnego systemu znaków, którym porozumiewają się gracze. Gra kończy się w momencie, gdy któraś z drużyn pozbędzie się wszystkich liter, a woreczek jest pusty lub gdy obie drużyny nie mogą już wykonać ruchu. Dopuszczalna liczba wymian wynosi trzy dla pary. Na ogół gra się na 25 lub 30 minut dla drużyny. 

<h2>Siódemki</h2>
Nieco niekonwencjonalną odmianą gry w scrabble są siódemki. A to dlatego, że nie można wykonać ruchu z mniejszej liczby płytek niż
siedem (czyli wszystkich ze stojaka). Pomysłodawcą tej gry jest Andrzej Rechowicz. 
<h3>Zasady gry</h3>
Gra toczy się pomiędzy dwójką graczy i składa się z 20 kolejek, na które każdy gracz ma określoną ilość czasu (zwykle 10 minut). W każdej kolejce gracz może zrobić jedną z trzech rzeczy: wymiana dowolnej ilości płytek, pas bądź też ułożenie wszystkie siedmiu liter na planszy. Jeśli przeciwnik zaakceptuje nasz wyraz bądź też - w wypadku sprawdzenia - znajduje się on w słowniku gracz, który ułożył ten wyraz uzyskuje jeden punkt. Zwycięża ta osoba, która w przeciągu 20 kolejek uzyskała więcej punktów.

<h2>Szarlotka</h2>
Szarlotka to kolejna gra, której pomysł narodził się na wczasach scrabblowych (tym razem w Szarlocie), stąd też jej nazwa. Ta odmiana scrabble jest bardzo podobna do karpatki.
<h3>Zasady gry</h3>
Zasady gry są niemal identyczne jak w przypadku karpatki. Jedyną różnicą jest to, że nie ma tu drużyn dwuosobowych tylko każdy z graczy ma do dyspozycji dwa stojaki i to on wykonuje z nich oba ruchy. Drużyna zastąpiona jest więc przez jednego gracza.

<h2>Prezesówka</h2>
Jak nie trudno przypuszczać to kolejna gra, której pomysł narodził się na wczasach scrabblowych. Różni się ona od swych poprzedniczek przede wszystkim tym, że wymaga dwóch planszy (a najlepiej jednej specjalnej) i dwóch kompletów liter.
<h3>Zasady gry</h3>
Dwójka graczy gra na specjalnie przygotowanej planszy, powstałej przez połączenie dwóch zwykłych planszy do gry tak, aby uzyskać prostokąt o 
wymiarach 15 na 29 pól. Każdy gracz ma jeden stojak, z którego wykonuje ruchy oraz własny worek z literami. Pierwszy gracz rozpoczyna kładąc wyraz przechodzący przez jedno z dwóch pól oznaczonych gwiazdką (środki połówek planszy), drugi gracz musi wykorzystać drugie pole z gwiazdką. Po wyczerpaniu się liter w jednym z worków, gracz, który wyczerpał wszystkie litery może grać dalej kontynuując losowanie liter z worka przeciwnika. Gra kończy się po wyczerpaniu liter z obu worków bądź też braku ruchu ze strony obu graczy. Przyjęty czas dla jednego zawodnika na grę to 40 minut.

<h2>Superscrabble</h2>
Superscrabble czyli „mózg paruje”. W skrócie - szarlotka na prezesówce: dwóch graczy gra na podwójnej planszy (15x29), każdy ma 2 stojaki i swój worek. Pierwszy gracz rozpoczyna kładąc wyraz przechodzący przez jedno z dwóch pól oznaczonych gwiazdką (środki połówek planszy), drugi gracz ma 2 ruchy, z czego w jednym musi wykorzystać drugie pole z gwiazdką. Odmiana najbardziej wymagająca oraz najbardziej „punktodajna” - wynik zwycięzcy często przekracza 1000 pkt.

<?require_once "files/php/bottom.php"?>
</body>
</html>

