<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Słownik : Aktualizacje do OSPS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" />
	<![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script>
	<![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script>
	<![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","osps");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Aktualizacje do OSPS")</script></h1>

Do pobrania są pliki aktualizacyjne z 16 października 2012 (UPDATE 32), zawierające 
porcję poprawek.<h2>OSPS wersja 2.2</h2>
<img src="files/img/osps2maly.gif" class="onright" alt="osps" />
<a href="osps/patcher_2_22.exe">ŁATKA (patcher_2_22.exe)</a> (350 kB)<br />
<i>(Łatka usuwa różnego rodzaju błędy, które pojawiały się w analizatorze)</i>
<ol>
	<li>Uruchom ściągnięty <b>patcher_2_22.exe</b> (powstanie katalog „patcher” z plikami „patcher.exe” i „data”)</li>
    <li>Uruchom <b>patcher.exe</b> i wskaż położenie pliku wykonywalnego <b>osps.exe</b></li>
</ol>
<br />
<b>Uwaga!</b> Przed aktualizacją słownika należy zainstalować łatkę poprawiającą w kilku miejscach funkcjonalność programu i pozwalającą na obsługę zmienionego formatu update'u.<br /><br />

<a href="osps/update32.upd">AKTUALIZACJA (update32.upd)</a> (3,25 MB)<br />
<ol>
	<li>Uruchom, spatchowany uprzednio <b>osps.exe</b></li>
    <li>Otwórz moduł USTAWIENIA (ikona z kółkami zębatymi lub F12)</li>
    <li>Kliknij „Uaktualnij” i wskaż położenie pliku <b>update32.upd</b></li>
</ol>

<a href="osps/UPDATE_32.xls">OPIS UPDATE'U 32</a><br />
<ol>
	<li>Lista słów dodanych i usuniętych w updacie 32</li>
</ol>

<h2>OSPS wersja 1.0</h2>
<img src="files/img/osps1.gif" class="onright" alt="osps stary" />
<b>Uwaga!</b> Ostatni update zawiera też materiał ze wszystkich poprzednich uzupełnień. Nie trzeba szukać żadnych poprzednich update'ów, by mieć aktualną wersję słownika.<br /><br />
<b>Update 32 do OSPS 1.0 w przygotowaniu</b> <br> <br>
<a href="../osps/update_30_osps1.exe">UPDATE_30_OSPS1.EXE</a> (2 234 kB)<br /><br />
Po kliknięciu w pierwszy link, w zależności od sposobu skonfigurowania przeglądarki, uruchomi się program aktualizujący lub będziemy mieli możliwość zapisania go na dysku.<br />
Aby zaktualizować OSPS, należy uruchomić program <b>update_30_osps1.exe</b> na komputerze, na którym korzystamy z OSPS. Można to zrobić online lub po zapisaniu
<b>update_30_osps1.exe</b> na dysku.
Podczas procesu aktualizacji w katalogu \Program Files\Osps\ zostaną utworzone
trzy pliki:
<ul>
	<li>usun.txt - zawierający wszystkie słowa usunięte z OSPS</li>
    <li>dodaj.txt - zawierający wszystkie słowa dodane do OSPS</li>
    <li>tematy.txt - zawierający formy podstawowe wszystkich słów dodanych do OSPS w tej samej kolejności jak w pliku dodaj.txt</li>
</ul>
<p>Update w każdej kolejnej wersji działa tak samo zarówno u tych, którzy uruchamiali jego poprzednie wersje, jak i u tych, którzy tego nie robili.
Update działa dopiero po powtórnym uruchomieniu OSPS.

</p>

<h2>Inne pliki do ściągnięcia</h2>
<p>
<a href="osps/UPDATE_32.xls">UPDATE_32.XLS</a> - opis zmian wprowadzonych w 
32. updacie.<br>
<a href="osps/UPDATE_31.xls">UPDATE_31.XLS</a> - opis zmian wprowadzonych w 
31. updacie.<br>
<a href="osps/UPDATE_30.xls">UPDATE_30.XLS</a> - opis zmian wprowadzonych w 
30. updacie.<br>
<a href="osps/update29.xls">UPDATE29.XLS</a> - opis zmian wprowadzonych w 29. 
updacie.<br>
<a href="osps/opis28.txt" target="_blank">OPIS28.TXT </a> - opis zmian wprowadzonych w 28. updacie.<br />
<a href="osps/opis27.txt" target="_blank">OPIS27.TXT</a> - opis zmian wprowadzonych w 27. updacie<br />
<a href="osps/opis26.txt" target="_blank">OPIS26.TXT</a> - opis zmian wprowadzonych w 26. updacie<br />
<a href="osps/opis25.txt" target="_blank">OPIS25.TXT</a> - opis zmian wprowadzonych w 25. updacie<br />
<a href="osps/opis24.txt" target="_blank">OPIS24.TXT</a> - opisy zmian wprowadzonych w 24. i 23. updacie<br />
<a href="osps/o-isjp00.txt">O-ISJP00.TXT</a> (63 kB) - opis zmian, które zaszły w lipcu 2000 r., wraz z uwzględnieniem materiału zawartego w „Innym słowniku języka polskiego PWN”<br />
<a href="osps/o-spp99.txt">O-SPP99.TXT</a> (24 kB) - poprawiony (wersja z 25 maja 2000 r.) opis zmian, które zaszły wraz z uwzględnieniem materiału zawartego w „Nowym słowniku poprawnej polszczyzny PWN”<br />
<a href="osps/o-osps.txt">O-osps.txt</a> - istotne decyzje podjęte przy opracowywaniu Oficjalnego Słownika Polskiego Scrabblisty<br />
<a href="osps/dodatki.zip">DODATKI.ZIP</a> (18 kB) zawierający następujące pliki tekstowe:<br />
	</p>
	<ul>
    	<li>O-osps.txt - istotne decyzje podjęte przy opracowywaniu Oficjalnego Słownika Polskiego Scrabblisty</li>
		<li>Im-b.txt - lista czasowników, których przechodniość została uznana, a zatem posiadają formy imiesłowu biernego, wyszczególnione w drugiej kolumnie</li>
        <li>Imp-3.txt - lista wszystkich czasowników grupy koniugacyjnej III posiadających formy imiesłowu przymiotnikowego przeszłego</li>
        <li>P-odczas.txt - lista wszystkich czasowników posiadających pochodne formy przymiotnikowe</li>
        <li>Mnogie.txt - lista wszystkich rzeczowników zanotowanych w słownikach w liczbie mnogiej, dla których uznano formy liczby pojedynczej</li>
        <li>Defekt.txt - lista czasowników defektywnych</li>
        <li>Dorosz.txt - lista słów dołączonych ze „Słownika języka polskiego” pod redakcją Witolda Doroszewskiego</li>
        <li>Syn-zrd.txt - lista słów „ukrytych” w hasłach jako synonimy bądź części haseł wielowyrazowych</li>
</ul>

<?require_once "files/php/bottom.php"?>
</body>
</html>
