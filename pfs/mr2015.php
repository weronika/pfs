<?
require_once "files/php/funkcje.php";

$mr_pomorze_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2015', 'region' => $TOUR_REGION[pomorze]),
    order   => array ( 'data_od' )
));
$mr_cnwschod_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2015', 'region' => $TOUR_REGION[cnwschod]),
    order   => array ( 'data_od' )
));
$mr_poludnie_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2015', 'region' => $TOUR_REGION[poludnie]),
    order   => array ( 'data_od' )
));

$mr_pomorze_results = pfs_select (array (
    table   => $DB_TABLES[mr2015_pomorski],
    order   => array ( '!suma' )
));
$mr_cnwschod_results = pfs_select (array (
    table   => $DB_TABLES[mr2015_centralny],
    order   => array ( '!suma' )
));
$mr_poludnie_results = pfs_select (array (
    table   => $DB_TABLES[mr2015_poludniowy],
    order   => array ( '!suma' )
));
?>
<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Mistrzostwa Regionów 2015</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","mr2015")</script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Mistrzostwa Regionów 2015")</script></h1>

<h2>Mistrzostwa Regionów 2015</h2>
Cykl turniejów Mistrzostwa Regionów 2015 stanowi zawody towarzyszące cyklowi Grand Prix 2015
<h2>Klasyfikacje</h2>
W Mistrzostwach Regionów 2015 prowadzone będą trzy niezależne klasyfikacje – oddzielnie w każdym regionie. Cykl Mistrzostwa Regionów 2015 obejmuje:<br> <br><ol>
<li>Region Pomorze
</li><li>Region Południe
</li><li>Region Centrum i Mazury
</li></ol>
<h2>Turnieje</h2>
Cykl Mistrzostwa Regionów 2015 obejmuje 15 turniejów podzielonych na 3 regiony:
<table class="linki ramkadolna">
<tbody><tr><td colspan=2><b>Region Pomorze</b></td></tr>
<?php
$cnt = 1;
foreach ($mr_pomorze_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>
<tr><td colspan=2></td></tr><td colspan=2><b>Region Południe</b></td></tr>
<?php
$cnt = 1;
foreach ($mr_poludnie_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>
<tr><td colspan=2></td></tr><td colspan=2><b>Region Centrum i Mazury</b></td></tr>
<?php
$cnt = 1;
foreach ($mr_cnwschod_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>
</tbody></table>
<br><br>

<h2>Zasady punktacji</h2>
<ol>
        <li>Punkty do klasyfikacji Mistrzostw Regionu zdobywa tylu uczestników, ile wynosi część całkowita liczby obliczonej według wzoru: liczba uczestników, którzy uczestniczyli co najmniej w 5 rundach turnieju, dzielona przez 3.
	</li><li>Zawodnik, który zajmie ostatnie punktowane miejsce, otrzymuje 1 punkt, każdy kolejny o jeden punkt więcej.
	</li><li>Minimalna liczba uczestników zdobywających punkty do klasyfikacji Mistrzostw Regionu wynosi 20 (tzn. w przypadku turnieju, w którym uczestniczyło mniej niż 60 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 20 otrzymuje 1 punkt, za 19–2, itd., a za 4–17, za 3– 18, za 2–19, za 1–20).
	</li><li>Maksymalna liczba uczestników zdobywających punkty do klasyfikacji Mistrzostw Regionu wynosi 30 (tzn. w przypadku turnieju, w którym uczestniczyło więcej niż 90 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 30 otrzymuje 1 punkt, za 29–2, itd., za 4–27, za 3–28, za 2–29 i za 1–30).
	</li><li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Mistrzostwa Regionów 2015 do końcowej klasyfikacji zalicza się cztery najlepsze wyniki z pięciu turniejów w danym regionie.
	</li><li>Przy równej liczbie punktów w klasyfikacji końcowej o kolejności decyduje wyższa suma punktów zdobytych we wszystkich turniejach rozpatrywanego regionu Mistrzostw Regionów 2015 (bez odpisywania), a jeśli to nie da efektu (gracze mieli identyczny wynik), dalszym kryterium będzie wyższa zdobycz punktowa w najlepszym turnieju (w przypadku braku rozstrzygnięcia, w kolejnym najlepszym turnieju, itd.). Jeśli również to nie przyniesie rozstrzygnięcia, zawodnicy zajmują miejsce ex-aequo, a nagrody finansowe za ich miejsca sąsumowane i dzielone po równo.
</li></ol>

<h2>Nagrody</h2>
Nagrody w cyklu Mistrzostwa Regionów 2015 wynoszą:<br><br>
<p><b>Region Pomorze</b></p>
    <ul>
    <li>za pierwsze miejsce – 300 zł</li>
    <li>za drugie miejsce – 200 zł</li>
    <li>za trzecie miejsce – 100 zł</li>
</ul><br>
<p><b>Region Południe</b></p>
<ul>
    <li>za pierwsze miejsce – 300 zł</li>
    <li>za drugie miejsce – 200 zł</li>
    <li>za trzecie miejsce – 100 zł</li>
</ul><br>
<p><b>Region Centrum i Mazury</b></p>
<ul>
    <li>za pierwsze miejsce – 300 zł</li>
    <li>za drugie miejsce – 200 zł</li>
    <li>za trzecie miejsce – 100 zł</li>
</ul>

<a name="klasyfikacja_pomorze"></a>
<h2>Klasyfikacja Mistrzostw Regionu Pomorze</h2>
<?
print_gp_table($mr_pomorze_tours, $mr_pomorze_results)
?>
<a name="klasyfikacja_poludnie"></a>
<h2>Klasyfikacja Mistrzostw Regionu Południe</h2>
<?
print_gp_table($mr_poludnie_tours, $mr_poludnie_results)
?>
<a name="klasyfikacja_pnwschod"></a>
<h2>Klasyfikacja Mistrzostw Regionu Centrum i Mazury</h2>
<?
print_gp_table($mr_cnwschod_tours, $mr_cnwschod_results)
?>
<?include "files/php/bottom.php"?>
</body>
</html>
