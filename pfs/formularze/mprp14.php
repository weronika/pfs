<?
include "../../files/php/funkcje.php";
$turniej = "IV Otwarte Mistrzostwa Polski Radców Prawnych w Scrabble";
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Zgłoszenie do turnieju <? print $turniej; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../../files/img/favicon.ico" />
    <link rel="stylesheet" href="../../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../../files/js/jquery.js"></script>
    <script type="text/javascript" src="../../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../../files/js/java.js"></script>
    <script>jSubmenu("turnieje","kalendarz");</script>
    <style type="text/css">
        input, textarea, select { margin: 3px 0 15px 0; padding: 2px; }
    </style>
</head>
<body>
<? include "../../files/php/menu.php"; ?>
<h1 id="formularz">Zgłoszenie do turnieju <? print $turniej; ?></h1>

<?

if (empty ($_POST['submit'])) {
    print "<form method='post'>

    Imię i nazwisko:<br>
    <input name='osoba' type='text' maxlength='50' size='30'><br>

    Adres korespondencyjny:<br>
    <input name='adres' type='text' maxlength='80' size='50'><br>

    E-mail:<br>
    <input name='email' type='text' maxlength='50' size='30'><br>

    Telefon/fax:<br>
    <input name='kontakt' type='text' maxlength='50' size='30'><br>

    Zawód:<br>
    <input name='zawod' type='text' maxlength='50' size='30'><br>

    Miejsce pracy (wykonywania zawodu):<br>
    <input name='praca' type='text' maxlength='80' size='50'><br>

   Opcja noclegu (wybierz właściwe):<br>
       <select name='nocleg'>
        <option value='niewybrano'></option>
        <option value='tak'>Będę korzystał</option>
        <option value='nie'>Nie będzie korzystał</option>
	</select>
    z noclegu z 23/24.05.2014 r. za dodatkową opłatą w wysokości 85 zł,<br>którą uiszczę wraz ze zgłoszeniem uczestnictwa w Majówce z Mecenasem 2014.  
	<br><br>

    Uwagi:<br>
    <textarea name='uwagi' rows='3' cols='60'></textarea><br>

    <input type='submit' name='submit' value='Zgłoś się' class='przycisk' >
    </form>";
}

else {
    $message = "
        Imię i nazwisko: $_POST[osoba]<br>
        Adres korespondencyjny: $_POST[adres]<br><br>

        E-mail: $_POST[email]<br>
        Telefon/fax: $_POST[kontakt]<br>
        Zawód: $_POST[zawod]<br>
	    Miejsce pracy (wykonywania zawodu): $_POST[praca]<br>
        Nocleg: $_POST[nocleg]<br>

        Uwagi: $_POST[uwagi]
    ";

    if (mail_utf8 (
        "$_POST[osoba] <$_POST[email]>",
        "biuro@oirp.bydgoszcz.pl ; grafin@post.pl",
        "Zgłoszenie do turnieju $turniej",
        $message
    )) {
        print "Dziękujemy za zgłoszenie do turnieju <b>$turniej</b>.";
    }
}
?>

<? include "../../files/php/bottom.php"; ?>
</body>
</html>
