<?
require '../files/php/funkcje.php';

if ($_GET['idt']) {
    $tour = pfs_select_one (array (
        fields  => array ( 'nazwa', 'id' ),
        table   => $DB_TABLES[tours],
        where   => array ( 'id' => $_GET['idt'] )
    ));
}

?>
<html>
<head>
    <title>Polska Federacja Scrabble : Zgłoszenie do turnieju <? echo $tour->nazwa; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../files/img/favicon.ico" />
    <link rel="stylesheet" href="../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../files/js/jquery.js"></script>
    <script type="text/javascript" src="../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../files/js/java.js"></script>
    <script type="text/javascript">jSubmenu("turnieje","kalendarz");

    function validateEmail(e){
        with (e)
        {
            apos = value.indexOf("@");
            dotpos = value.lastIndexOf(".");
            if (apos<1 || dotpos-apos<2 || dotpos<2){
                 alert("Błędnie wypełniono adres e-mail.");
                  return false;
            }
            else
                return true;
        }
    }

    function validate(f){
        with (f){
            with(osoba){
              if (value==null || value=="" || value.indexOf(" ") < 0 || value.indexOf(" ") == value.length-1){
                  alert("Błędnie wypełniono imię i nazwisko.");
                  return false;
              }
            }
            with(miasto){
              if (value==null || value==""){
                  alert("Błędnie wypełniono miejscowość.");
                  return false;
              }
            }
            with(email){
              return validateEmail(email);
            }
        return true;
        }
    }
    </script>

     <style type="text/css">
    input, textarea, select{
        margin: 3px 0 15px 0;
        padding: 2px;
        color: #777;
    }
    .checkbox{
        vertical-align: middle;
        margin: 8px;
    }
    div{
        margin: 10px 0;
    }
    table td{
        padding-right: 40px;
    }
  </style>
</head>
<body>
<? require "../files/php/menu.php"; ?>

<h1 id="formularz">Zgłoszenie do turnieju<br><span><? echo $tour->nazwa; ?></span></h1>

<form method='post' action='krok2.php' onsubmit="return validate(this)" >
<input type='hidden' name='idt' value=<? echo $tour->id; ?>>
    <table>
        <tr><td>Imię i nazwisko:</td><td><input type='text' name='osoba'><td></td></tr>
        <tr><td>Miasto:</td><td><input type='text' name='miasto'><td></td></tr>
        <tr><td>E-mail:</td><td><input type='text' name='email'><td></td></tr>
        <tr><td>Uwagi:</td><td><textarea name='uwagi' rows='2' cols='40'></textarea><td></td></tr>
        <tr><td></td><td><input type='submit' value='Zapisz się!' class='przycisk'><td></td></tr>
    </table>
</form>


<? include "../files/php/bottom.php"; ?>
</body>
</html>
