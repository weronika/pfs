<?
require "../files/php/funkcje.php";

function generujCiag() {
    $znaki = '1234567890qwertyuiopasdfghjkklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    $ciag  = '';
    for ($i = 0; $i < 24; $i++){
        $ciag .= $znaki[ rand () % (strlen ($znaki)) ];
    }
    return $ciag;
}

if ($_POST['idt']) {
    $tour = pfs_select_one (array (
        fields  => array ( 'nazwa', 'id' ),
        table   => $DB_TABLES[tours],
        where   => array ( 'id' => $_POST['idt'] )
    ));
}

?>
<html>
<head>
    <title>Polska Federacja Scrabble : Zgłoszenie do turnieju <? echo $tour->nazwa; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../files/img/favicon.ico" />
    <link rel="stylesheet" href="../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../files/js/jquery.js"></script>
    <script type="text/javascript" src="../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../files/js/java.js"></script>
    <script>jSubmenu("turnieje","kalendarz");</script>
     <style type="text/css">
    input, textarea, select{
        margin: 3px 0 15px 0;
        padding: 2px;
    }
    .checkbox{
        vertical-align: middle;
        margin: 8px;
    }
    div{
        margin: 10px 0;
    }
    table td{
        padding-right: 70px;
    }
  </style>
</head>
<body>
<? require "../files/php/menu.php"; ?>

<h1 id="formularz">Zgłoszenie do turnieju<br><span><? echo $tour->nazwa;?></span></h1>

<?php
    if($_POST){
        $idt    = $_POST[idt];
        $osoba  = $_POST[osoba];
        $miasto = $_POST[miasto];
        $email  = $_POST[email];
        $uwagi  = $_POST[uwagi];
        $query  = sprintf ("SELECT id FROM $DB_TABLES[registrations] WHERE id_turnieju='%d' AND UPPER(osoba)=UPPER('%s') AND UPPER(email)=UPPER('%s');",
            $idt, mysql_real_escape_string ($osoba), mysql_real_escape_string ($email));
        $result = mysql_query($query);
        $count  = mysql_fetch_row($result);
        if($count >= 1) {
            print "Ta osoba jest już zapisana do tego turnieju.<br>Sprawdź skrzynkę, której adres podałeś przy zgłoszeniu i kliknij link zawarty w zaproszeniu, aby znaleźć się na liście uczestników. <br><br>";
        }
        else {
            $ciag = generujCiag ();
            $query = sprintf("INSERT INTO $DB_TABLES[registrations](id_turnieju, osoba, miasto, email, uwagi, potwierdzenie) VALUES ('%d', '%s','%s','%s','%s','%s');",
                    $idt, mysql_real_escape_string($osoba), mysql_real_escape_string($miasto), mysql_real_escape_string($email), mysql_real_escape_string($uwagi), $ciag);

            mysql_query($query);
            $id = mysql_insert_id();
            $subject = "Zgłoszenie do turnieju $tour->nazwa";
            $message = "Witaj! Zgłosiłeś swój udział w turnieju.<br><br>$osoba<br>$miasto<br>Uwagi: $uwagi<br><br>
                Kliknij poniższy link, aby potwierdzić prawdziwość powyższych danych i zapisać się na turniej oraz zaakceptować umieszczenie powyższych danych na stronie turnieju:<br><br>

                <a href='http://www.pfs.org.pl/formularze/krok3.php?id=$id&ci=$ciag'> http://www.pfs.org.pl/formularze/krok3.php?id=$id&ci=$ciag </a><br><br>

                Jeśli nie zgłaszałeś się do turnieju, nie odpowiadaj na tego maila i nie klikaj powyższego linku.<br><br>

                --<br>
                Polska Federacja Scrabble<br>
                http://www.pfs.org.pl/";

            if (mail_utf8 ("pfs@pfs.org.pl", $email, $subject, $message)) {
                print "Dziękujemy za zgłoszenie do turnieju <b>$tour->nazwa</b>!<br><br>Na Twój adres e-mail została wysłana wiadomość - sprawdź skrzynkę i kliknij link zawarty w wiadomości,<br>aby potwierdzić prawdziwość danych i znaleźć się na liście uczestników.<br><br>";
                print "<a href='krok1.php?idt=$idt'>Zapisz kolejną osobę</a>";
            }

            else {
                print "Wystąpił błąd, <a href='krok1.php?idt=$idt'>spróbuj jeszcze raz</a>.";
            }

            if($uwagi != ''){
                $subject = "Uwagi do zgłoszenia ($tour->nazwa)";
                $message = $osoba."<br>".$miasto."<br><br>Uwagi: ".$uwagi;
                mail_utf8 ($email, "pfs@pfs.org.pl", $subject, $message);
            }
        }
    }
    else{
        print "Wystąpił błąd, <a href='krok1.php?idt=$idt'>spróbuj jeszcze raz</a>.";
    }

?>

<? require "../files/php/bottom.php"; ?>
</body>
</html>
