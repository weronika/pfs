<?
include "../../files/php/funkcje.php";
$turniej = "X Klubowe Mistrzostwa Polski";
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Zgłoszenie do turnieju <? print $turniej; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../../files/img/favicon.ico" />
    <link rel="stylesheet" href="../../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../../files/js/jquery.js"></script>
    <script type="text/javascript" src="../../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../../files/js/java.js"></script>
    <script>jSubmenu("turnieje","kalendarz");</script>
    <style type="text/css">
        input, textarea, select { margin: 3px 0 15px 0; padding: 2px; }
    </style>
</head>
<body>
<? include "../../files/php/menu.php"; ?>
<h1 id="formularz">Zgłoszenie do turnieju <? print $turniej; ?></h1>

<?

if (empty ($_POST['submit'])) {
    print "<form method='post'>

    Nazwa klubu:<br>
    <input name='klub' type='text' maxlength='35' size='30'><br>

    Adres kontaktowy e-mail:<br>
    <input name='email' type='text' maxlength='35' size='30'><br>

    Imię i nazwisko kapitana:<br>
    <input name='kapitan' type='text' maxlength='35' size='30'><br>


   Skład ilościowy drużyny:<br>
       <select name='iloscosobwskladzie'>
        <option value='niewybrano'></option>
        <option value='4osoby'>Czteroosobowy</option>
        <option value='5osob'>Pięcioosobowy (cztery osoby + jedna osoba rezerwowa)</option>
	</select>
	<br>


    Skład osobowy drużyny:<br>
    Pierwszy zawodnik (imię i nazwisko): <input name='zawodnik1' type='text' maxlength='50' size='30'><br>
    Drugi zawodnik (imię i nazwisko): <input name='zawodnik2' type='text' maxlength='50' size='30'><br>
    Trzeci zawodnik (imię i nazwisko): <input name='zawodnik3' type='text' maxlength='50' size='30'><br>
	Czwarty zawodnik (imię i nazwisko): <input name='zawodnik4' type='text' maxlength='50' size='30'><br>
	Ewentualnie piąty zawodnik (imię i nazwisko): <input name='zawodnik5' type='text' maxlength='50' size='30'><br>

    Uwagi:<br>
    <textarea name='uwagi' rows='3' cols='60'></textarea><br>

    <input type='submit' name='submit' value='Zgłoś klub' class='przycisk' >
    </form>";
}

else {
    $message = "
        Nazwa klubu: $_POST[klub]<br>
        Kapitan: $_POST[kapitan]<br><br>

        Ilość osób w drużynie: $_POST[iloscosobwskladzie]<br>
	    Skład:<br> $_POST[zawodnik1]<br>
        $_POST[zawodnik2]<br>
        $_POST[zawodnik3]<br>
        $_POST[zawodnik4]<br>
        $_POST[zawodnik5]<br>


        Uwagi: $_POST[uwagi]
    ";

    if (mail_utf8 (
        "$_POST[klub] <$_POST[email]>",
        "zarzad@pfs.org.pl",
        "Zgłoszenie do turnieju $turniej",
        $message
    )) {
        print "Dziękujemy za zgłoszenie do turnieju <b>$turniej</b>.";
    }
}
?>

<? include "../../files/php/bottom.php"; ?>
</body>
</html>
