<?
include "../../files/php/funkcje.php";
$turniej = "II Mistrzostwa Gór Świętokrzyskich";
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Zgłoszenie do turnieju <? print $turniej; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../../files/img/favicon.ico" />
    <link rel="stylesheet" href="../../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../../files/js/jquery.js"></script>
    <script type="text/javascript" src="../../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../../files/js/java.js"></script>
    <script>jSubmenu("turnieje","kalendarz");</script>
    <style type="text/css">
        input, textarea, select { margin: 3px 0 15px 0; padding: 2px; }
    </style>
</head>
<body>
<? include "../../files/php/menu.php"; ?>
<h1 id="formularz">Zgłoszenie do turnieju <? print $turniej; ?></h1>

<?

if (empty ($_POST['submit'])) {
    print "<form method='post'>

    Imię i nazwisko:<br>
    <input name='osoba' type='text' maxlength='50' size='30'><br>

    Miasto:<br>
    <input name='miasto' type='text' maxlength='50' size='30'><br>

    Adres e-mail:<br>
    <input name='email' type='text' maxlength='50' size='30'><br>

   Status uczestnika:<br>
       <select name='status'>
        <option value='bezzniżek'>zwykły uczestnik</option>
        <option value='członekPFS'>członek PFS</option>
        <option value='młodzież'>młodzież</option>
        <option value='debiutant'>debiutant</option>
	</select>
	<br><br>
	
   Zupa:<br>
       <select name='zupa'>
        <option value='niewybrano'></option>
        <option value='pomidorowa'>pomidorowa</option>
        <option value='rosół'>rosół</option>
	</select>
	<br><br>

   Drugie danie:<br>
       <select name='drugie'>
        <option value='niewybrano'></option>
        <option value='schabowy'>Kotlet schabowy</option>
        <option value='kolejarski'>Kotlet kolejarski (schab + pieczarki + ketchup + zapiekany ser)</option>
	</select>
	<br><br>

   Dodatki do 2 dania:<br>
       <select name='dodatki'>
        <option value='niewybrano'></option>
        <option value='puree'>Ziemniaki puree i surówki</option>
        <option value='opiekane'>Ziemniaki opiekane i surówki</option>
	</select>
	<br><br>

    Uwagi:<br>
    <textarea name='uwagi' rows='3' cols='60'></textarea><br>

    <input type='submit' name='submit' value='Zgłoś się' class='przycisk' >
    </form>";
}

else {
    $message = "
        Imię i nazwisko: $_POST[osoba]<br>
        Miasto: $_POST[miasto]<br><br>

	Status uczestnika: $_POST[status]<br>
	Zupa: $_POST[zupa]<br>
	Drugie: $_POST[drugie]<br>
	Dodatki: $_POST[dodatki]<br>
	

        Uwagi: $_POST[uwagi]
    ";

    if (mail_utf8 (
        "$_POST[osoba] <$_POST[email]>",
        "j.gorka@pfs.org.pl",
        "Zgłoszenie do turnieju $turniej",
        $message
    )) {
        print "Dziękujemy za zgłoszenie do turnieju <b>$turniej</b>.";
    }
}
?>

<? include "../../files/php/bottom.php"; ?>
</body>
</html>
