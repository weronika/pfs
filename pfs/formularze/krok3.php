<?
require "../files/php/funkcje.php";

if($_GET){
    $id = $_GET['id'];
    $ci = $_GET['ci'];
    if(isset($id)){
        $result = mysql_query("SELECT * FROM $DB_TABLES[registrations] WHERE id=$id");
        while($row = mysql_fetch_array($result)){
            $idt  = $row['id_turnieju'];
            $ciag = $row['potwierdzenie'];
        }
        if(isset($idt)){
            $result = mysql_query("SELECT nazwa FROM $DB_TABLES[tours] WHERE id=$idt");
            while($row = mysql_fetch_array($result))
                $nazwa = $row['nazwa'];
        }
    }
}

?>
<html>
<head>
    <title>Polska Federacja Scrabble : Potwierdzenie zgłoszenia do turnieju <?echo $nazwa;?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="../files/img/favicon.ico" />
    <link rel="stylesheet" href="../files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="../files/js/jquery.js"></script>
    <script type="text/javascript" src="../files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="../files/js/java.js"></script>
    <script>jSubmenu("turnieje","kalendarz");</script>

     <style type="text/css">
    input, textarea, select{
        margin: 3px 0 15px 0;
        padding: 2px;
    }
    .checkbox{
        vertical-align: middle;
        margin: 8px;
    }
    div{
        margin: 10px 0;
    }
    table td{
        padding-right: 70px;
    }
  </style>
</head>
<body>
<? require "../files/php/menu.php"?>

<h1 id="formularz">Potwierdzenie zgłoszenia do turnieju<br><span><?echo $nazwa;?></span></h1>

<?php
    if(isset($id) && isset($ci) && isset($ciag)) {
        if($ci == $ciag){
            $query = sprintf("UPDATE $DB_TABLES[registrations] SET potwierdzenie=1 WHERE id='%d'", $id);
            mysql_query($query);
            print "Gratulacje, zostałeś zapisany do turnieju.<br><br><a href='krok1.php?idt=$idt'>Zapisz kolejną osobę</a>";
        } else if($ciag=="1")
            print "Ten adres e-mail został już potwierdzony.<br><br><a href='krok1.php?idt=$idt'>Zapisz kolejną osobę</a>";
        else
            print "$ci<br>$ciag<br>Wystąpił błąd.<br>Zgłoś się do administratora strony: <a href='mailto:pfs@pfs.org.pl'>pfs@pfs.org.pl</a>";
    }
    else
        print "Wystąpił błąd.<br>Zgłoś się do administratora strony: <a href='mailto:pfs@pfs.org.pl'>pfs@pfs.org.pl</a>";
?>

<?include "../files/php/bottom.php"?>
</body>
</html>
