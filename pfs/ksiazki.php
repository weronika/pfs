﻿<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Różności : Książki o Scrabble</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("roznosci","ksiazki");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Książki o Scrabble")</script></h1>

<h2>„Grajmy w scrabble”</h2>
<img src="files/img/grajmywscrabble.jpg" class="onleft" alt="Grajmy w Scrabble" />
<b>„Grajmy w scrabble”<br />Wojciech Usakiewicz, Michał Derlacki</b><br />Wyd. Kleks, Bielsko-Biała 1998<br /><br />
W grudniu 1998 r. ukazała się pierwsza polska książka o Scrabble — „Grajmy w Scrabble” Wojciecha Usakiewicza i Michała Derlackiego. Na ponad 250 stronach można znaleźć podstawowe informacje o grze, będące rozszerzeniem tego, co znajduje się w instrukcji, porady taktyczne, analizę własności poszczególnych liter, historię Scrabble w Polsce, opis nietypowych sposobów gry. W osobnej części — listę słów dwu- i trzyliterowych z przedłużeniami. Ponadto czytelnik znajdzie sporo ciekawych zadań.<br />
Książka jest godna polecenia wszystkim scrabblistom — zarówno tym, stawiającym pierwsze kroki, jak i zaawansowanym.<br /><br />

Nakład książki został wyczerpany.<br>

<!-- Wysyłkową sprzedaż książki (cena ok. 20 złotych) prowadzi:<br /><br />
Wydawnictwo KLEKS<br />
ul. Olszówka 62, 43-309 Bielsko-Biała<br />
tel. (+33) 8 222 555, (+33) 8 222 666, (+33) 8 123 409, (+33) 8 110 025<br />
bezpłatna infolinia: 0 800 163 107<br />
e-mail: <a onClick="sendMail('wydawnictwo','kleks.pl')">wydawnictwo@kleks.pl</a><br /><br />
Książkę można także kupić na turniejach scrabble (zwłaszcza w Bielsku-Białej). -->

<?require_once "files/php/bottom.php"?>
</body>
</html>

