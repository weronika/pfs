﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Walne Zgromadzenie Członków Polskiej Federacji Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Walne Zgromadzenie Członków Polskiej Federacji Scrabble</h1>

<p>Szanowni Członkowie Polskiej Federacji Scrabble,<br><br>
W imieniu Zarządu PFS serdecznie zapraszam do udziału w Walnym Zgromadzeniu, które odbędzie się <b>16 kwietnia w Warszawie</b>, przy okazji <a href="../turniej.php?id=554">XV Mistrzostw Warszawy</a>. Podsumujemy pierwszy rok działalności Zarządu, omówimy bieżącą sytuację oraz plany federacji na przyszłość.<br>
Będzie oczywiście okazja podzielić się pomysłami i propozycjami z innymi członkami PFS. Zapraszamy do aktywnego udziału w Walnym Zgromadzeniu. Szczegóły w tej sprawie reguluje poniższa uchwała.<br><br>
<i>Prezes Polskiej Federacji Scrabble <br>
Krzysztof Sporczyk</i></p>

<p><b>Uchwała nr 13/2011 z dnia 22.03.2011 w sprawie zwołania Walnego Zgromadzenia Członków PFS</b>
<p>Zarząd Polskiej Federacji Scrabble działając na podstawie §20 Statutu PFS postanawia zwołać Walne Zgromadzenie w terminie 16 kwietnia 2011 roku w Warszawie.<br>
Miejsce - Centrum Promocji Kultury w Dzielnicy Praga Południe m. st. Warszawy, ul. Podskarbińska 2.<br>
Pierwszy termin - godz. 19:00, drugi termin 19:15.</p>
<p>Proponowany porządek obrad:
<ol>
<li>Otwarcie i wybór przewodniczącego Walnego Zgromadzenia,
<li>Wybór Prezydium, protokolanta oraz Komisji: Mandatowo-Skrutacyjnej i Wnioskowej,
<li>Zatwierdzenie porządku obrad,
<li>Uchwalenie regulaminu obrad, sprawozdanie Komisji Mandatowej,
<li>Sprawozdanie Zarządu
<li>Sprawozdanie Komisji Rewizyjnej,
<li>Głosowanie nad sprawozdaniem Zarządu i udzieleniem absolutorium
<li>Dyskusja programowa,
<li>Sprawozdanie Komisji Wnioskowej i podjęcie Uchwał,
<li>Sprawozdanie Komisji Mandatowo-Skrutacyjnej,
<li>Wolne wnioski,
<li>Zamknięcie obrad.</li></ol>
</p>


<h2 id="kont">Kontynuacja Walnego Zgromadzenia Członków Polskiej Federacji Scrabble</h2>

<p>Walne Zgromadzenie Członków Polskiej Federacji Scrabble będzie kontynuowane (od miejsca przerwania, tj. od punktu 7. przyjętego prządku obrad) po zakończeniu pierwszego dnia turnieju <a href="../turniej.php?id=552">VI Mistrzostwa Milanówka „Truskawki w Milanówku”</a>, tj. w dn. 4.06.2011 ok. godz.: 19:10 
<p>Porządek obrad przyjęty na Walnym Zgromadzeniu Członków Polskiej Federacji Scrabble w dn. 16.04.2011

<ol>
<i><li>Otwarcie i wybór przewodniczącego Walnego Zgromadzenia, 
<li>Wybór Prezydium, protokolanta oraz Komisji: Mandatowo-Skrutacyjnej i Wnioskowej, 
<li>Zatwierdzenie porządku obrad, 
<li>Uchwalenie regulaminu obrad, sprawozdanie Komisji Mandatowej, 
<li>Sprawozdanie Zarządu, 
<li>Sprawozdanie Komisji Rewizyjnej,</i>
<li>Pytania do Zarządu i wolne wnioski,
<li>Działalność i finansowanie programu „Scrabble w szkole”,
<li>Głosowanie nad sprawozdaniem Zarządu i udzieleniem absolutorium,
<li>Głosowanie nad udzieleniem absolutorium każdemu z członków Zarządu z osobna,
<li>Odwołanie członków Zarządu którzy nie otrzymali absolutorium,
<li>Powołanie nowych członków Zarządu w miejsce odwołanych,
<li>Dyskusja programowa, 
<li>Sprawozdanie Komisji Wnioskowej i podjęcie Uchwał, 
<li>Sprawozdanie Komisji Mandatowo-Skrutacyjnej, 
<li>Dyskusja na temat Mistrzostw Polski w Scrabble po angielsku oraz wystąpienia zaproszonych gości,
<li>Wolne wnioski, 
<li>Zamknięcie obrad.</li></ol>
<br>
W przypadku niezakończenia Walnego Zgromadzenia Członków PFS do godz. 23:15 (ostatni pociąg z Milanówka do Warszawy odjeżdża o godz: 23:34) obrady zostaną przerwane. Kontynuacja Walnego Zgromadzenia Członków nastąpi w najbliższym możliwym terminie.<br><br><br>
<A href="pelnomocnictwo.doc">Wzór pełnomocnictwa (doc)</a><br>
<A href="pelnomocnictwo.pdf">Wzór pełnomocnictwa (pdf)</a>

<?require_once "../files/php/bottom.php"?>
</body>
</html>

