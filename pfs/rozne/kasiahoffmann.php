<html>
	<title>Podziękowania od Kasi Hoffmann</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<link rel="shortcut icon" href="../pliki_strony/img/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../pliki_strony/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../pliki_strony/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../pliki_strony/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../pliki_strony/styleswitcher.js"></script>
	<script type="text/javascript" src="../pliki_strony/java.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../pliki_strony/pngfix.js"></script><![endif]-->
</head> 
 
<body>
<?include "../pliki_strony/menu.php"?>
<h1>Podziękowania od Kasi Hoffmann</h1>

<img alt="Kasia Hoffmann" src="kasiahoffmann2.jpg" class="onleft">

Szanowne Scrabblistki i Scrabbliści,<br><br>
W styczniu 2008 roku u mojej 15-letniej córki Kasi zdiagnozowano nowotwór złośliwy kości i płuc. Świat nam wszystkim wywrócił się do góry nogami. Jak się okazało, choroba była już niezwykle zaawansowana, rokowania były dramatyczne... operacja płuc, amputacja lewej nogi, 25% szans na przeżycie... Zaczęła się nasza walka z rakiem, czasem, bólem, strachem. Razem z Kasią "zamieszkałyśmy" na ponad rok na 7 piętrze CZD na oddziale onkologii, gdzie córka została poddana bardzo intensywnej i agresywnej chemioterapii.<br><br>
<img alt="Kasia Hoffmann" src="kasiahoffmann1.jpg" class="onright">
Na dzień dzisiejszy, po 20 miesiącach leczenia, Kasia nadal toczy swój bój z tą bezlitosną chorobą, a my wszyscy razem z nią. Córka jest po 26 chemioterapiach, kilku operacjach, ma endoprotezę lewej kończyny. Po kilku miesiącach rehabilitacji nauczyła się na "nowo" chodzić. I dla mnie jest moim osobistym bohaterem. Niezwykle dzielna, nigdy nie płakała, to ona pocieszała mnie, gdy było najciężej.<br><br>
Chciałam za pośrednictwem PFSu podziękować Wszystkim Państwu, którzy swój 1% podatku i inne darowizny przekazali na subkonto mojej Kasi, na jej leczenie i rehabilitację. Niestety Fundacja nie podaje listy darczyńców, a wiem, że takich osób było bardzo dużo, zarówno człownków PFSu jak i scrabblistów grających na cronixie. Dziękuję za wsparcie, i tym wszystkim , którzy o nas myśleli i pamiętali. Dziękuję w imieniu Kasi, swoim i mojego męża Michała Derlackiego.<br><br>
<center>Jolanta Hoffmann-Derlacka</center>


<?include "../pliki_strony/bottom.php"?>
</body>
</html>