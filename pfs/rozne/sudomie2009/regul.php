<?
$turniej = "Wczasy scrabblowe - Sudomie 2009 - Regulamin";
$data1   = "Si?demki - 28.06.2009 - niedziela wiecz?r";
$data2   = "Liga wczasowa - 29.06.2009-01.07.2009 - poniedzia?ek-?roda";
$data3   = "Kolejne zawody - 29.06.2009 - poniedzia?ek wiecz?r";
$name    = substr(__FILE__,50);
?>

<html>
<head>
	<title><?print $turniej;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
	<meta name="Author" content="Grzegorz Wi?czkowski">
	<meta name="Author" content="Andrzej Lo?y?ski">
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../../../files/css/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../../../files/js/styleswitcher.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
</head>

<body>
<?include "../../../files/php/menu.php"?>
<?
print "<h1>".$turniej."</h1>";
?>

<?include "picture.php"?>

<div style="float:left;">

<?include "http://www.pfs.org.pl/turnieje/2009/sudomie/menu.php?name=".$name; ?>

<ol>
<li>Turnieje</li>
<ul>
<li>wariatka</li>
<li>belgijka</li>
<li>si?demki</li>
<li>karpatka</li>
<li>blitz</li>
<li>szarlotka</li>
<li>superscrabble</li>
<li>prezes?wka</li>
<li>scrabblobieg</li>
</ul>
<li>Liga</li>
<li>Turniej o Puchar W?jta</li>
</ol>

<br/><b>Punktacja d?ugofalowa</b><br/>
<ol>
<li>Turnieje - I miejsce = liczba zawodnik?w z obni?k? o 1 + 3-2-1 za pierwsze trzy miejsca.</li>
<li>Liga - I miejsce = liczba zawodnik?w x 4 z obni?k? o 4 + 7-5-3-1 za pierwsze cztery miejsca.</li>
<li>Turniej o Puchar W?jta - I miejsce = liczba zawodnik?w x 2 z obni?k? o 2 + 5-3-1 za pierwsze trzy miejsca.</li>
<li>Do punktacji zaliczane s? wszystkie turnieje.</li>
<li>Przy r?wnej licznie punkt?w o wy?szym miejscu decyduje lepsze miejsce w ligowej punktacji indywidualnej.</li>

</div>


<?include "../../../files/php/bottom.php"?>
</body>
</html>