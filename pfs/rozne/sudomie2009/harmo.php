<?
$turniej = "Wczasy scrabblowe - Sudomie 2009 - Harmonogram";
$data1   = "Si?demki - 28.06.2009 - niedziela wiecz?r";
$data2   = "Liga wczasowa - 29.06.2009-01.07.2009 - poniedzia?ek-?roda";
$data3   = "Kolejne zawody - 29.06.2009 - poniedzia?ek wiecz?r";
$name    = substr(__FILE__,50);
?>

<html>
<head>
    <title><?print $turniej;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
    <meta name="Author" content="Grzegorz Wi?czkowski">
    <meta name="Author" content="Andrzej Lo?y?ski">
    <link rel="stylesheet" href="../../../files/css/style.css" type="text/css"  title="default"/>
    <link rel="alternate stylesheet" href="../../../files/css/stylelight.css" type="text/css" title="light"/>
    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
    <script type="text/javascript" src="../../../files/js/styleswitcher.js"></script>
    <!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
    <style>
        td.rundy {
            border-color:white;
            border-style:solid;
            border-width:1px;
            text-align:center;
            vertical-align:middle;
            padding: 10px 25px 10px 25px;
        }
        th.rundy {
            border-color:white;
            border-style:solid;
            border-width:1px;
            text-align:center;
            vertical-align:middle;
            color: yellow;
            font-weight:bold;
            padding: 10px 25px 10px 25px;
        }
        table.rundy {
            border-color:yellow;
            border-style:solid;
            border-width:2px;
        }
    </style>
</head>

<body>
<?include "../../../files/php/menu.php"?>
<?
print "<h1>".$turniej."</h1>";
?>

<?include "picture.php"?>

<div style="float:left;">

<div align="center">

<table class="klasyfikacja">
<tr>
<td class="rundy">dzie?</td>
<td class="rundy">rano<br/>11:00</td>
<td class="rundy">popo?udnie<br/>15:00</td>
<td class="rundy">wiecz?r<br/>20:00</td>
</tr>
<tr>
<td class="rundy">sobota<br/>27 czerwca</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">turniej rozpoznawczy</td>
</tr>
<tr>
<td class="rundy">niedziela<br/>28 czerwca</td>
<td class="rundy">turniej o Puchar W?jta</td>
<td class="rundy">turniej o Puchar W?jta</td>
<td class="rundy">si?demki</td>
</tr>
<tr>
<td class="rundy">poniedzia?ek<br/>29 czerwca</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">liga</td>
<td class="rundy">szarlotka</td>
</tr>
<tr>
<td class="rundy">wtorek<br/>30 czerwca</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">liga</td>
<td class="rundy">superscrabble</td>
</tr>
<tr>
<td class="rundy">?roda<br/>1 lipca</td>
<td class="rundy">scrabblobieg</td>
<td class="rundy">liga</td>
<td class="rundy">prezes?wka</td>
</tr>
<tr>
<td class="rundy">czwartek<br/>2 lipca</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">blitz<br/>belgijka</td>
<td class="rundy">wariatka<br/>(zamiast karpatki)</td>
</tr>
<tr>
<td class="rundy">pi?tek<br/>3 lipca</td>
<td class="rundy">wolne<br/>(zamiast wariatki)</td>
<td class="rundy">wariatka</td>
<td class="rundy">Prize Giving Ceremony</td>
</tr>
<tr>
<td class="rundy">sobota<br/>4 lipca</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">szwajcar</td>
<td class="rundy">?</td>
</tr>
<tr>
<td class="rundy">niedziela<br/>5 lipca</td>
<td class="rundy">happy end</td>
<td class="rundy">&nbsp;</td>
<td class="rundy">&nbsp;</td>
</tr>
</table>
</div>

</div>


<?include "../../../files/php/bottom.php"?>
</body>
</html>