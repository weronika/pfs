<?include_once "../../../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Turnieje : Partie XVII Mistrzostw Polski</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<link rel="shortcut icon" href="../../../files/img/favicon.ico" />
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../../../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../../../files/js/jquery.js"></script>
	<script type="text/javascript" src="../../../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../../../files/js/java.js"></script>
	<script>jSubmenu("turnieje","archiwum")</script>
</head>

<body>
<?require_once "../../../files/php/menu.php"?>
<h1><script>naglowek("Partie XVII Mistrzostw Polski")</script></h1>

<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=46">Runda 15: Kamil G�rka - �ukasz Bobowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=45">Runda 15: Dariusz Kosz - Mateusz �bikowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=43">Runda 15: Micha� Makowski - Marcin Mroziuk</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=42">Runda 14: Kamil G�rka - Janusz Kaczor</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=41">Runda 13: Rafa� Lenartowski - Rafa� D�browski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=40">Runda 12: Micha� Makowski - Dariusz Kosz</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=39">Runda 11: Mateusz �bikowski - Micha� Makowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=38">Runda 10: �ukasz Tuszy�ski - Dariusz Kosz</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=37">Runda 9: Dariusz Kosz - Bartosz Morawski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=36">Runda 8: Marek Reda - Kamil G�rka</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=35">Runda 7: Rafa� Lenartowski - Micha� Makowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=34">Runda 6: Dariusz Kosz - Kamil G�rka</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=33">Runda 5: Dariusz Puton - Micha� Makowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=32">Runda 4: Mateusz �bikowski - Rafa� Lenartowski</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=31">Runda 3: Rafa� Lenartowski - Dariusz Kosz</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=30">Runda 2: Micha� Makowski - Kamil G�rka</a><br>
<a href="http://www.pfs.org.pl/turnieje/zapis.php?id=29">Runda 1: Marcin Mroziuk - Kamil G�rka</a>


<?require_once "../../../files/php/bottom.php"?>
</body>
</html>

