﻿RANKING SWS (z dnia 2 marca 2013 r.)

MIEJSCE                                           RANKING     SKALPY / GRY
--------------------------------------------------------------------------
  1  Dominik Urbacki          XIV_LO_WARSZAWA    187 (187.31)   2435 / 13 
  2  Arkadiusz Janota         UKS_ANAGRAM        165 (164.50)    658 / 4  
  3  Wojciech Urbański        MKS_ERROR          163 (163.14)   1142 / 7  
  4  Bartłomiej Pawlak        REKINY             157 (156.76)   2665 / 17 
  5  Michał Dąbrowski         KLUB_GIER_LOGICZNYCH 152 (151.89)   2734 / 18 
  6  Mariusz Rostkowski       REKINY             150 (150.13)   2252 / 15 
  7  Szymon Dąbrowski         KLUB_GIER_LOGICZNYCH 149 (149.08)   3727 / 25 
  8  Juliusz Czupryniak       GIM_112_WARSZAWA   148 (148.06)   2665 / 18 
  9  Paweł Mazurek            ZSA_KŁODZKO        148 (148.05)   2961 / 20 
 10  Maciej Sylwestrzuk       LO_CIECHANOWIEC    145 (145.46)   1891 / 13 
 11  Julia Kowalska           LITERKA            145 (145.00)   1160 / 8  
 12  Rafał Błaszkiewicz       SKS_1_MILANÓWEK    145 (144.80)    724 / 5  
 13  Jacek Olczyk             GIM_99_WARSZAWA    144 (144.12)   1153 / 8  
 14  Krzysztof Lelonek        UKS_ANAGRAM        143 (143.00)    572 / 4  
 15  Artur Dachowski          UKS_ANAGRAM        142 (142.25)    569 / 4  
 16  Hubert Sikorski          SKS_1_MILANÓWEK    142 (142.20)    711 / 5  
 17  Aleksandra Brzeska       GRAJMY_W_SCRABBLE  142 (141.50)   1415 / 10 
 18  Monika Koziestańska      ZAGRAM!            139 (139.14)    974 / 7  
 19  Hubert Smoliński         LITERKA            139 (138.71)   2358 / 17 
 20  Mateusz Marciniewicz     SARDYNKI           138 (138.40)    692 / 5  
 21  Maria Wasińska           LITERKA            137 (137.10)   1371 / 10 
 22  Weronika Dziatkowska     SARDYNKI           134 (134.46)   1748 / 13 
 23  Tomasz Zagórski          ZSA_KŁODZKO        132 (132.00)    528 / 4  
 24  Michał Borowski          GIM_99_WARSZAWA    131 (131.38)   1051 / 8  
 25  Wojciech Stawicki        LITERKA            131 (131.00)    917 / 7  
 26  Justyna Mikołajczyk      LITERKA            130 (130.12)   1041 / 8  
 27  Weronika Rozpiórska      LITERKA            129 (128.50)   1285 / 10 
 28  Weronika Wiśniewska      SKS_1_MILANÓWEK    128 (128.25)   1026 / 8  
 29  Maria Czyżewska          SCRABBLOŁAMACZE    128 (127.50)    510 / 4  
 30  Olga Jóźwiak             LITERKA            126 (126.35)   2148 / 17 
 31  Natalia Malinowska       REKINY             126 (125.75)    503 / 4  
 32  Martyna Bloch            LITERKA            125 (125.00)   2125 / 17 
 33  Oskar Glegoła            SCRABBLOŁAMACZE    125 (124.79)   2371 / 19 
 34  Patryk Grzelak           SCRABBLOŁAMACZE    125 (124.71)    873 / 7  
 35  Małgorzata Budziszewska  ZAGRAM!            124 (124.29)    870 / 7  
 36  Łukasz Pawlak            REKINY             124 (123.59)   2101 / 17 
 37  Konrad Pajączek          MATRIKS            123 (123.43)   1728 / 14 
 38  Stanisław Więckowski     REKINY             123 (122.82)   2088 / 17 
 39  Marcin Pawlak            LITERKA            122 (122.00)   2074 / 17 
 40  Bartłomiej Papiernik     REKINY             121 (120.86)    846 / 7  
 41  Patrycja Szewczyk        REKINY             118 (117.50)    470 / 4  
 42  Kamil Olejniczak         LITERKA            116 (115.88)    927 / 8  
 43  Paweł Dzierżyński        SKS_1_MILANÓWEK    115 (115.40)    577 / 5  
 44  Natalia Felczak          REKINY             115 (115.25)    461 / 4  
 45  Sara Ewkowska            LITERKA            112 (112.17)    673 / 6  
 46  Piotr Ozimek             REKINY             112 (112.05)   2465 / 22 
 47  Maksymilian Piątek       SCRABBLOŁAMACZE    112 (111.83)   1342 / 12 
 48  Kamila Jasińska          ZAGRAM!            111 (111.29)    779 / 7  
 49  Michał Januszewski       REKINY             110 (110.25)    441 / 4  
 50  Matylda Berus            SARDYNKI           110 (110.00)    550 / 5  
 51  Julia Linke              SARDYNKI           110 (110.00)    550 / 5  
 52  Jan Pląsek               GIM_9_WARSZAWA     110 (110.00)    550 / 5  
 53  Natalia Podleś           TPD-WAWER          110 (110.00)    550 / 5  
 54  Dominika Kania           MZS_2_BĘDZIN       110 (109.75)    439 / 4  
 55  Sari Al-Tabich           SCRABBLOŁAMACZE    110 (109.50)    438 / 4  
 56  Marta Pawlak             LITERKA            108 (108.00)    864 / 8  
 57  Jan Żuk-Widmański        SARDYNKI           108 (107.72)   1939 / 18 
 58  Emilia Dębicka           REKINY             107 (107.00)    428 / 4  
 59  Witold Orłowski          REKINY             106 (106.25)    425 / 4  
 60  Mikołaj Wus              GIM_76_WARSZAWA    106 (106.20)   1062 / 10 
 61  Krystian Hiszpański      LITERKA            106 (106.12)    849 / 8  
 62  Daniel Kędziora          REKINY             106 (105.50)    422 / 4  
 63  Judyta Maj               MZS_2_BĘDZIN       105 (104.50)    418 / 4  
 64  Maciej Skotniczny        ZSA_KŁODZKO        104 (103.86)    727 / 7  
 65  Piotr Niciarz            MZS_2_BĘDZIN       104 (103.50)    414 / 4  
 66  Adrianna Kęska           REKINY             103 (103.25)    413 / 4  
 67  Franciszek Uliński       SCRABBLE_Z_OCHOTĄ  103 (103.11)   1856 / 18 
 68  Michał Żakowski          REKINY             102 (102.00)    408 / 4  
 69  Katarzyna Winiarska      KIŚLE              102 (101.91)   1121 / 11 
 70  Julia Kałębasiak         LITERKA            102 (101.57)    711 / 7  
 71  Janek Domino             SARDYNKI           101 (101.45)   1116 / 11 
 72  Wiktoria Kostka          SKS_1_MILANÓWEK    101 (100.62)    805 / 8  
 73  Katarzyna Chojnowska     MATRIKS            101 (100.50)   1407 / 14 
 74  Kamila Albowicz          WARSZAWA           100 (100.00)    800 / 8  
 75  Antonina Bachmat         TPD-WAWER          100 (100.00)   1200 / 12 
 76  Zuzanna Dąbek            SCRABBLOŁAMACZE    100 (100.00)   1200 / 12 
 77  Paulina Kiełbasa         REKINY             100 (100.00)    400 / 4  
 78  Tomasz Kuciński          KIŚLE              100 (100.00)    400 / 4  
 79  Kamil Owczarek           REKINY             100 (100.00)    400 / 4  
 80  Hanna Szczypińska        REKINY             100 (100.00)    400 / 4  
 81  Piotr Świderski          SCRABBLAKI_W       100 (100.00)    400 / 4  
 82  Julia Zawal              SCRABBLE_Z_OCHOTĄ   97 ( 97.38)    779 / 8  
 83  Julia Korowajska         LITERKA             96 ( 96.00)    768 / 8  
 84  Julia Goszcz             TPD-WAWER           95 ( 94.83)   1138 / 12 
 85  Piotr Kardaszewski       SARDYNKI            95 ( 94.62)   1230 / 13 
 86  Lena Wróbel              SARDYNKI            93 ( 93.00)   1023 / 11 
 87  Sara Ben_Nasr            SCRABBLAKI_W        93 ( 92.86)    650 / 7  
 88  Kamila Majchrzak         PSP_28_RADOM        93 ( 92.62)    741 / 8  
 89  Oskar Katana             PSP_28_RADOM        93 ( 92.60)    463 / 5  
 90  Jan Siekierski           SKS_1_MILANÓWEK     92 ( 92.20)    461 / 5  
 91  Małgorzata Wieczorek     SARDYNKI            92 ( 92.17)    553 / 6  
 92  Julia Głaszczka          TPD-WAWER           91 ( 91.00)    637 / 7  
 93  Zuzanna Nowicka          SARDYNKI            91 ( 91.00)    546 / 6  
 94  Paulina Jankowska        SCRABBLOŁAMACZE     91 ( 90.88)    727 / 8  
 95  Karolina Cieślak         PSP_28_RADOM        90 ( 90.00)    450 / 5  
 96  Joanna Korabik           TPD-WAWER           90 ( 90.00)    450 / 5  
 97  Ewelina Linkiewicz       TPD_WAWER           90 ( 90.00)    450 / 5  
 98  Marta Sadlej             SSP_13_WARSZAWA     90 ( 90.00)    450 / 5  
 99  Patryk Rosiński          SARDYNKI            90 ( 89.91)    989 / 11 
100  Artur Radecki            MZS_2_BĘDZIN        88 ( 88.00)    352 / 4  
101  Bartosz Art              SARDYNKI            88 ( 87.92)   1143 / 13 
102  Karolina Majchrzak       PSP_28_RADOM        87 ( 87.00)    696 / 8  
103  Joanna Kulawiak          SCRABBLE_Z_OCHOTĄ   86 ( 85.67)   1028 / 12 
104  Paweł Gierej             TPD-WAWER           85 ( 85.25)    341 / 4  
105  Filip Korczak            SCRABBLOŁAMACZE     85 ( 84.78)    763 / 9  
106  Michał Juda              SP_141_WARSZAWA     84 ( 84.43)    591 / 7  
107  Jan Wieczorek            SARDYNKI            84 ( 84.20)    421 / 5  
108  Ada Szara                OSIEDLE_KABATY      83 ( 83.38)    667 / 8  
109  Wiktoria Grzelak         MZS_2_BĘDZIN        83 ( 83.25)    333 / 4  
110  Wiktor Adamiak           SCRABBLE_Z_OCHOTĄ   81 ( 80.69)   1049 / 13 
111  Tomasz Masiarz           PSP_28_RADOM        80 ( 80.12)    641 / 8  
112  Anna Poniatowska         TPD-WAWER           79 ( 78.57)    550 / 7  
113  Tomasz Knap              OSIEDLE_KABATY      78 ( 78.25)    626 / 8  
114  Jarosław Księżak         REKINY              78 ( 78.00)    312 / 4  
115  Agnieszka Maciak         TPD-WAWER           78 ( 77.50)    310 / 4  
116  Klaudia Łubnicka         TPD-WAWER           76 ( 75.75)    303 / 4  
117  Milena Bachmat           TPD-WAWER           75 ( 75.00)    300 / 4  
118  Dominika Bednarek        TPD-WAWER           75 ( 75.00)    300 / 4  
119  Jakub Dworczyk           MZS_2_BĘDZIN        75 ( 75.00)    300 / 4  
120  Michał Franas            SARDYNKI            75 ( 75.00)    300 / 4  
121  Mateusz Hyla             MZS_2_BĘDZIN        75 ( 75.00)    300 / 4  
122  Kamil Szatniewski        MZS_2_BĘDZIN        75 ( 75.00)    300 / 4  
123  Tosia Szara              OSIEDLE_KABATY      70 ( 70.00)    350 / 5  
124  Anna Knap                OSIEDLE_KABATY      65 ( 64.75)    518 / 8  
125  Piotr Hołowiak           SARDYNKI            64 ( 63.75)    255 / 4  
126  Jakub Szara              OSIEDLE_KABATY      63 ( 63.12)    505 / 8  
127  Natalia Gotowicz         WARSZAWA            63 ( 62.50)    500 / 8  
128  Wiktor Gross             SCRABBLE_Z_OCHOTĄ   57 ( 57.25)    229 / 4  
129  Przemysław Bielski       SCRABBLE_Z_OCHOTĄ   56 ( 56.00)    280 / 5  
130  Konrad Krzykowski        SCRABBLE_Z_OCHOTĄ   55 ( 54.80)    274 / 5  
131  Zofia Ziętek             SCRABBLOŁAMACZE     53 ( 53.25)    213 / 4  
132  Igor Sumka               SARDYNKI            53 ( 52.71)    369 / 7  
133  Antoni Kawęcki           TPD-WAWER           52 ( 51.86)    363 / 7  
134  Weronika Skarżyńska      WARSZAWA            50 ( 50.00)    200 / 4  
135  Karolina Wichowska       PSP_28_RADOM        50 ( 50.00)    250 / 5  

POCZEKALNIA

MIEJSCE                                           RANKING     SKALPY / GRY
--------------------------------------------------------------------------
136  Mikołaj Czajkowski       ŚWIETLICOWE_SCRABBLE 160 (160.00)    480 / 3  
137  Gabriela Tarłowska       LITERKA            135 (135.00)    405 / 3  
138  Mateusz Czajkowski       ŚWIETLICOWE_SCRABBLE 127 (127.00)    381 / 3  
139  Anna Redeł               WARSZAWA           127 (126.67)    380 / 3  
140  Marcin Sułkowski         WARSZAWA           126 (126.00)    378 / 3  
141  Paweł Worobiej           JÓZEFÓW            125 (125.00)    375 / 3  
142  Robert Kasprzak          JÓZEFÓW            117 (116.67)    350 / 3  
143  Natalia Michalak         LITERKA            117 (116.67)    350 / 3  
144  Kacper Oniks             JÓZEFÓW            117 (116.67)    350 / 3  
145  Zuzanna Rokicka          MYŚLICIELE         117 (116.67)    350 / 3  
146  Emilia Spanialska        MYŚLICIELE         117 (116.67)    350 / 3  
147  Sara Zgirska             GIM_99_WARSZAWA    117 (116.67)    350 / 3  
148  Paweł Jełowicki          WARSZAWA            98 ( 98.33)    295 / 3  
149  Igor Suski               JÓZEFÓW             98 ( 98.33)    295 / 3  
150  Adrianna Płuciennik      LITERKA             89 ( 89.33)    268 / 3  
151  Damian Drożdż            TPD-WAWER           83 ( 83.33)    250 / 3  
152  Kamila Drożdż            WARSZAWA            83 ( 83.33)    250 / 3  
153  Kewin Juda               WARSZAWA            83 ( 83.33)    250 / 3  
154  Zuzanna Kozon            MYŚLICIELE          83 ( 83.33)    250 / 3  
155  Matylda Majewska         WARSZAWA            83 ( 83.33)    250 / 3  
156  Zofia Niemczyk           WARSZAWA            83 ( 83.33)    250 / 3  
157  Milena Orłowska          ZAGRAM!             83 ( 83.33)    250 / 3  
158  Milena Rybicka           LITERKA             83 ( 83.33)    250 / 3  
159  Wiktoria Sinołęcka-Wypych SCRABBLE_Z_OCHOTĄ   83 ( 83.33)    250 / 3  
160  Krzysztof Wojtaszko      WARSZAWA            83 ( 83.33)    250 / 3  
161  Marzena Bałękowska       PSP_28_RADOM        65 ( 65.00)    195 / 3  
162  Paweł Wiśniewski         TPD-WAWER           59 ( 59.33)    178 / 3  
163  Arkadiusz Albowicz       WARSZAWA            52 ( 51.67)    155 / 3  
164  Igor Malarski            WARSZAWA            50 ( 50.00)    150 / 3  
165  Natalia Mitas            MZS_2_BĘDZIN        50 ( 50.00)    150 / 3  
166  Igor Ungier              SARDYNKI            88 ( 88.00)     88 / 1  

