<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Słownik : Konkurs na organizację imprez PFS w 2015 roku</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Konkurs ofert na organizację imprez centralnych w 2015 roku</h1>

Zarząd Polskiej Federacji Scrabble ogłasza konkurs na organizację następujących imprez w 2015 roku:
<ul>
<li>XXIII Mistrzostw Polski (faza eliminacyjna i/lub faza finałowa wraz z turniejem towarzyszącym – przewidywany termin: listopad)</li>
<li>XXI Pucharu Polski</li>
<li>X Klubowych Mistrzostw Polski</li>
<li>XV Turnieju LeMans</li>
</ul>

Oferta powinna zawierać:
<ul>
<li>dane organizatora</li>
<li>nazwę turnieju</li>
<li>proponowany termin</li>
<li>opis miejsca rozgrywek</li>
<li>informacje o planowanej wysokości wpisowego i nagrodach</li>
<li>wstępne informacje dotyczące liczby i ceny noclegów i wyżywienia</li>
<li>wstępne informacje dotyczące oprawy medialnej i sponsorskiej turnieju</li>
<li>proponowaną wysokość dofinansowania ze strony PFS</li>
</ul>

Oferty można składać na adres poczty internetowej PFS <a href="mailto:pfs@pfs.org.pl">pfs@pfs.org.pl</a> do:<br>
- 28 lutego w przypadku X Klubowych Mistrzostw Polski,<br>
- 31 marca w przypadku XV Turnieju Le Mans i XXI Pucharu Polski,<br>
- 31 maja w przypadku XXIII Mistrzostw Polski.<br><br>
W przypadku zgłoszenia dwóch lub więcej ofert na ten sam turniej, ostateczną decyzję podejmie Zarząd PFS, kierując się zarówno terminem, jak i możliwościami organizatorów, mając przede wszystkim na względzie jak najlepsze warunki rozegrania turnieju.<br>
Zarząd ogłosi wybór organizatorów wzmiankowanych turniejów w terminie do 14 dni od daty zamknięcia przyjmowania zgłoszeń.<br>
Zarząd zastrzega sobie możliwość skrócenia terminu konkursu w jednej z wymienionych kategorii w przypadku otrzymania satysfakcjonującej oferty, spełniającej wymogi organizacyjne i/lub wskazania odpowiednio wcześniejszego terminu organizacji imprezy.<br><br>

Regulaminy powyższych turniejów zostaną uchwalone przez Zarząd PFS po konsultacjach z organizatorami zgłoszonych imprez.<br>
Sprzęt niezbędny do przeprowadzenia turniejów dostarczy Zarząd PFS.<br>
Sędziego na poszczególne imprezy wybierze i opłaci Zarząd PFS.<br>
Wszystkie imprezy centralne otrzymają dofinansowanie ze środków PFS.<br><br>
Wybrany organizator zobowiązany będzie do przedstawienia Zarządowi PFS rozliczenia z wykorzystania środków pozyskanych z otrzymanego dofinansowania w terminie 30 dni od dnia
zakończeniu turnieju.

<?require_once "../files/php/bottom.php"?>
</body>
</html>

