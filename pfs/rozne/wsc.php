﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Mistrzostwa Świata w Scrabble po angielsku / World Scrabble Championship</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>

<?require_once "../files/php/menu.php"?>
<h1>Mistrzostwa Świata w Scrabble po angielsku / World Scrabble Championship</h1>

<center>
<table style="font-size: large; text-align: center ">
<tr>
<td style="width:50%">
<a href="http://www.wscgames.com/" target="_blank"><img src="http://www.wscgames.com/images/logos/2011-wsc.gif" height="150" align="center"></a>
</td>
<td style="width:50%">
<a href="http://www.scrabble.onet.pl/" target="_blank"><img src="http://pfs.org.pl/rozne/wsc_onet.jpg" height="150"></a>
</td>
</tr>

<tr>
<td style="width:50%">
Oficjalna strona WSC 2011
</td>

<td style="width:50%">
Polska strona o WSC 2011
</td>
</tr>
</table>

<br><br>

<p style="font-size: xx-large ">
Wydarzenia towarzyszące WSC 2011</p><br>

<center>
<table style="font-size: large; text-align: center ">
<tr>
<td style="width:33%">
2.10.2011
</td>

<td style="width:33%">
10.10.2011
</td>

<td style="width:33%">
16.10.2011
</td>

</tr>


<tr>
<td style="width:33%">
<a href="http://www.pfs.org.pl/rozne/wscturniejeempik.php" target="_blank"><img src="http://pfs.org.pl/rozne/wsc_turnieje_empik.jpg" height="150"></a>
</td>
<td style="width:33%">
<a href="http://www.pfs.org.pl/rozne/wscempikjunior.php" target="_blank"><img src="http://pfs.org.pl/rozne/wsc_empik_junior.jpg" height="150"></a>
</td>
<td style="width:33%">
<a href="http://www.pfs.org.pl/rozne/wscdzienscrabble.php" target="_blank"><img src="http://pfs.org.pl/rozne/wsc_dzien_scrabble.jpg" height="150"></a>
</td>
</tr>

<tr>
<td style="width:33%">
Turnieje w empik school
</td>

<td style="width:33%">
Scrabble w Empik Junior
</td>

<td style="width:33%">
Dzień Scrabble
</td>

</tr>
</table>

<br><br><br>

<p style="font-size: large ">
<a href="http://www.pfs.org.pl/wolontariatWSC.php" target="_blank">Zostań wolontariuszem podczas Mistrzostw Świata w Scrabble</a>
</p><br>


<div style="width:90%;" >





</div>








<?require_once "../files/php/bottom.php"?>
</body>
</html>

