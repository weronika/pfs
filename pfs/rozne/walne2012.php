﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Walne Zgromadzenie Członków Polskiej Federacji Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Walne Zgromadzenie Członków Polskiej Federacji Scrabble</h1>

<p>Szanowni Członkowie Polskiej Federacji Scrabble,<br><br>
W imieniu Zarządu PFS serdecznie zapraszam do udziału w Walnym Zgromadzeniu, które odbędzie się <b>9 czerwca w Piastowie</b>, przy okazji <a href="../turniej.php?id=655">XVI Mistrzostwa Piastowa</a>. Podsumujemy dwa lata działalności Zarządu, omówimy bieżącą sytuację oraz plany federacji na przyszłość, ale przede wszystkim wyłonione zostaną nowe władze PFS.<br> Będzie oczywiście okazja podzielić się pomysłami i propozycjami z innymi członkami PFS.<br><br> Zapraszamy do aktywnego udziału w Walnym Zgromadzeniu.<br><br> Szczegóły w tej sprawie reguluje poniższa uchwała.<br><br>

<i>Prezes Polskiej Federacji Scrabble <br>
Krzysztof Sporczyk</i></p>

<p style="font-weight:bold;border:2px solid black;padding: 15px;">
	Uwaga! Ciąg dalszy Walnego Zgromadzenia odbędzie się 23 czerwca, godzina 19:00 (pierwszy termin), 19:15 (drugi termin). Miejsce: Ełk, pub WARKA, ul. Pułaskiego 24. Walne Zgromadzenie będzie kontynuować obrady od punktu 13.
</p>

<p><b>Uchwała nr 11/2012 z dnia 23.05.2012 w sprawie zwołania Walnego Zgromadzenia Członków PFS</b>
<p>Zarząd Polskiej Federacji Scrabble działając na podstawie §20 Statutu PFS postanawia zwołać Walne Zgromadzenie w terminie 9 czerwca 2012 roku w Piastowie.<br>
Miejsce - Gimnazjum nr 2 im. Bohaterów Powstania Warszawskiego - Piastów, Aleja Tysiąclecia 5.<br>
Pierwszy termin - godz. 18:50, drugi termin 19:05.</p>
Proponowany porządek obrad:<br>
1. Otwarcie i wybór przewodniczącego Walnego Zgromadzenia,<br>
2. Wybór protokolanta oraz Komisji: Mandatowo-Skrutacyjnej i Wnioskowej,<br>
3. Zatwierdzenie porządku obrad,<br>
4. Sprawozdanie Komisji Mandatowej,<br>
5. Sprawozdanie Zarządu,<br>
6. Sprawozdanie Komisji Rewizyjnej,<br>
7. Pytania do Zarządu i dyskusja nad sprawozdaniem Zarządu,<br>
8. Głosowanie nad sprawozdaniem Zarządu i udzieleniem absolutorium,<br>
9. Sprawozdanie Sądu Koleżeńskiego,<br>
10. Dyskusja programowa,<br>
11. Wybór Prezesa Zarządu,<br>
12. Wybór Zarządu,<br>
13. Wybór Komisji Rewizyjnej,<br>
14. Wybór Sądu Koleżeńskiego,<br>
15. Sprawozdanie Komisji Wnioskowej i podjęcie Uchwał,<br>
16. Sprawozdanie Komisji Mandatowo-Skrutacyjnej,<br>
17. Wolne wnioski,<br>
18. Zamknięcie obrad.<br>
</p>

<br><br>
<A href="pelnomocnictwo.doc">Wzór pełnomocnictwa (doc)</a><br>
<A href="pelnomocnictwo.pdf">Wzór pełnomocnictwa (pdf)</a>

<?require_once "../files/php/bottom.php"?>
</body>
</html>

