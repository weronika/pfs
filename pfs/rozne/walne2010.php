<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Walne Zgromadzenie Członków Polskiej Federacji Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Walne Zgromadzenie Członków Polskiej Federacji Scrabble</h1>

<p>Szanowni Członkowie Polskiej Federacji Scrabble,<br><br>
W imieniu Zarządu PFS serdecznie zapraszam do udziału w Walnym Zgromadzeniu, które odbędzie się <b>24 kwietnia w Łodzi</b>, przy okazji <a href="../turniej.php?id=532">turnieju z okazji XIII-lecia Polskiej Federacji Scrabble</a>. Podsumujemy dwa lata działalności Zarządu, omówimy bieżącą sytuację oraz plany federacji na przyszłość, ale przede wszystkim wyłonione zostaną nowe władze PFS. Będzie oczywiście okazja podzielić się pomysłami i propozycjami z innymi członkami PFS. Zapraszamy do aktywnego udziału w Walnym Zgromadzeniu. Szczegóły w tej sprawie reguluje poniższa uchwała.<br><br>
<i>Prezes Polskiej Federacji Scrabble <br>
Karol Wyrębkiewicz</i></p>


<p>Zarząd Polskiej Federacji Scrabble działając na podstawie §19 Statutu PFS postanawia zwołać Walne Zgromadzenie w terminie 24 kwietnia 2010 roku w Łodzi. Miejsce - Poleski Ośrodek Sztuki, ul. Krzemieniecka 2. Pierwszy termin - godz. 18:50, drugi termin 19:05.</p>
<p>Proponowany porządek obrad:
<ol>
<li>Otwarcie i wybór przewodniczącego Walnego Zgromadzenia</li>
<li>Wybór Prezydium, protokolanta oraz Komisji: Mandatowo-Skrutacyjnej i Wnioskowej</li>
<li>Zatwierdzenie porządku obrad</li>
<li>Sprawozdanie Komisji Mandatowej</li>
<li>Sprawozdanie Komisji Rewizyjnej</li>
<li>Sprawozdanie Zarządu, głosowanie absolutorium</li>
<li>Sprawozdanie Sądu Koleżeńskiego i rozpatrzenie wniosków zgłoszonych przez Sąd</li>
<li>Sprawozdanie Komisji Językowej</li>
<li>Dyskusja programowa</li>
<li>Wybór Prezesa Zarządu</li>
<li>Wybór Zarządu, Komisji Rewizyjnej i Sądu Koleżeńskiego</li>
<li>Sprawozdanie Komisji Wnioskowej i podjęcie Uchwał</li>
<li>Sprawozdanie Komisji Wyborczo-Skrutacyjnej</li>
<li>Wolne wnioski</li>
<li>Zamknięcie obrad</li>
</ol>
</p>


<?require_once "../files/php/bottom.php"?>
</body>
</html>

