<html>
	<title>Polska Federacja Scrabble :: Scrabble w szkole</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<link rel="shortcut icon" href="../pliki_strony/img/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../pliki_strony/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../pliki_strony/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../pliki_strony/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../pliki_strony/styleswitcher.js"></script>
	<script type="text/javascript" src="../pliki_strony/java.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../pliki_strony/pngfix.js"></script><![endif]-->
</head> 
 
<body>
<?include "../pliki_strony/menu.php"?>
<h1>Zbiórka publiczna</h1>

<a href="http://www.wszkole.scrabble.info.pl/" title="Zagraj z nami - Scrabble w szkole" target="_blank">
<img alt="Program edukacyjny Scrabble w szkole" src="../pliki_strony/img/sws_logo.jpg" class="onleft"></a>

Z dniem 17 kwietnia 2008r. rozpoczynamy zbiórkę publiczną której celem jest zebranie funduszy i darów rzeczowych wspomagających nasz program i poszczególne projekty. Uzyskaliśmy pozwolenie Ministra Spraw Wewnętrznych  i Administracji na prowadzenie zbiórki na terenie całego kraju nr 49/2008
(pozwolenia: <a href="mswia_1.pdf">cz.1</a>, <a href="rozne/mswia_2.pdf">cz.2</a>).<br />
Zbiórkę prowadzimy w formie dobrowolnych wpłat na konto programu:<br /><br />
<b>VW Bank Direct<br />33 2130 0004 2001 0386 3883 0002</b><br /><br />
Liczy się każda złotówka!<br /><br />


<a href="http://www.mirelka.pl/catalog/index.php?cPath=23" title="Nudzisz się zagraj w scrabble..." target="_blank"><img alt="Wszystko dla scrabblisty" 
src="http://www.mirelka.pl/catalog/images/banners/scrabble.jpg" class="onright"></a>


Poza tym na każdym turnieju scrabblowym będziemy zbierać datki do puszek kwestarskich oraz dary rzeczowe:
komplety do gry, zegary oraz przedmioty nadające się na nagrody w turniejach dla dzieci. W&nbsp;pozwoleniu znajdziecie
szczegółowy wykaz: jakie przedmioty wolno nam przyjmować. Kluby zrzeszone w Polskiej Federacji Scrabble,
będą prowadzić zbiórkę do skarbonek stacjonarnych w swoich miastach.<br>
Dziękujemy za wszystkie datki.<br /><br />
<div class="aligncenter" style="width: 100%;"><b>UCZYMY MYŚLEĆ. DZIĘKI TOBIE!</b></div>

<br><br><br>
<h1>Zbiórka publiczna - sprawozdanie końcowe</h1>

Sprawozdanie końcowe z ogólnopolskiej zbiórki publicznej<br>
za okres 7 kwietnia 2008 r. - 31 grudnia 2008 r.<br>
z przeznaczeniem środków na wsparcie programu edukacyjnego "Scrabble w szkole"<br><br>

<ol>
<li>Zbiórkę przeprowadzono na podstawie decyzji Ministra Spraw Wewnętrznych i Administracji nr 49/2008 z 07 kwietnia  2008 r.<br>
<li>Od 7 kwietnia do 31 grudnia 2008 r. zebrano  zł.1064,15 (słownie: tysiąc sześćdziesiąt cztery zł 15 groszy)<br>
<li>Zbiórka była prowadzona w formie:<br>
    <ul><li>dobrowolnych wpłat na konto bankowe - 80zł<br>
    <li>do puszek kwestarskich - 834,78zł<br>
    <li>skarbonek stacjonarnych - 149,37zł</ul>
<li>Nie zebrano żadnych darów rzeczowych z powodu braku darczyńców.<br>
<li>Na organizację zbiórki poniesiono koszt zakupu skarbonek stacjonarnych: 47,94 zł<br>
<li>Wydatki. Zebrana kwota 1016,21 zł będzie wydana zgodnie z uchwałą Walnego Zgromadzenia Polskiej Federacji Scrabble  nr 3 z dnia 30 maja 2009r. na zakup zegarów dla kół zarejestrowanych w programie.<br>
<li>Dochód razem: 1064,15zł, wydatki razem: 47,94 zł. Saldo: 1016,21 (słownie: jeden tysiąc szesnaście zł 21 groszy).<br>
<li>Zebrane fundusze zostaną wydatkowane, zgodnie z zezwoleniem, na terytorium Rzeczypospolitej Polskiej.</ol><br> <br>

Sprawozdanie sporządziła: Anna Arana<br>
Sprawdził: Rafał Wesołowski<br><br>


<?include "../pliki_strony/bottom.php"?>
</body>
</html>