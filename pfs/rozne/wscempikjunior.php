﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wydarzenia towarzyszące WSC 2011 :: Scrabble w empik Junior</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>

<?require_once "../files/php/menu.php"?>
<h1>Wydarzenia towarzyszące WSC 2011 :: Scrabble w empik Junior</h1>

<center>
<img src="http://pfs.org.pl/rozne/wsc_empik_junior.jpg">
</center>

<div>
<p><b>Wszyscy entuzjaści Scrabble będą mieli okazję porozmawiać i posłuchać najlepszych polskich scrabblistów. Wielokrotny Mistrz Polski w Scrabble – Tomasz Zwoliński oraz dwójka reprezentantów Polski na nadchodzących Mistrzostwach Świata w Scrabble (Wojciech Usakiewicz i Rafał Dominiczak) opowiedzą o swojej przygodzie z układaniem słów na planszy oraz o tym, czym różni się ich profesjonalne podejście od czystej rozrywki. </b></p>

<p>Wszyscy traktujący Scrabble wyłącznie, jako niezobowiązującą rozrywkę będą mogli posłuchać wskazówek, dzięki którym przy następnej grze rozłożą swoich znajomych na „scrabblowe” łopatki. Goście opowiedzą też, jak wyglądają profesjonalne przygotowania do międzynarodowego turnieju i w jaki sposób Polakom udało się zakwalifikować do światowej czołówki w rozgrywce prowadzonej wyłącznie w języku angielskim. Zwieńczeniem spotkania będzie możliwość przetestowania świeżo zdobytej wiedzy podczas rozgrywki w Scrabble z naszymi reprezentantami! Tak, tak – każdy będzie mógł spróbować swoich sił w grze z profesjonalnym scrabblistą! </p>

<b><p>10 października 2011, godz. 17:00<br>
EMPiK Junior<br>
ul. Marszałkowska 116/122, Warszawa</b>

</div>








<?require_once "../files/php/bottom.php"?>
</body>
</html>

