﻿Klasyfikacja turnieju siodemki

       Lp. Nr       Imię             Nazwisko          Miasto     Rank    DP    MP    Skalp Różn. 
           start                                                                            MP
      1.    2 Mirosław     Uglik                 Warszawa        144.87   4,5    28   832    13
      2.    6 Karol        Wyrębkiewicz          Łódź            136.06     4    25   811     7
      3.    4 Stanisław    Rydzik                Warszawa        142.59     3    25   682     5
      4.    8 Jakub        Błażejewski           Poznań          122.72     3    20   695     2
      5.    9 Szymon       Dąbrowski             Warszawa        116.29     3    16   703    -3
      6.    3 Mariusz      Wrześniewski          Kraków          142.94   2,5    20   586     0
      7.    5 Krzysztof    Obremski              Legnica         141.29   2,5    19   596     3
      8.    7 Rafał        Wesołowski            Warszawa        131.12   2,5    18   608     0
      9.    1 Dawid        Pikul                 Poznań          151.37   2,5    15   543     1
     10.   11 Józef        Burzyński             Ełk             107.68     2    15   574    -4
     11.   12 Sylwia       Łada                  Inowrocław      107.46     2    14   581    -4
     12.   10 Dariusz      Jarosławski           Ełk             112.61   1,5    15   533    -1
     13.   13 Krzysztof    Zawadzki              Sampława        107.01     1    14   468    -5
     14.   14 Paweł        Jankowski             Nowe Miasto     100.00     0     5   316   -14
       
