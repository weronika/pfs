<html>
	<title>Polska Federacja Scrabble :: Walne Zgromadzenie Członków Polskiej Federacji Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<link rel="shortcut icon" href="../files/img/favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../files/css/style.css" type="text/css"  title="default"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../files/css/java.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<style>
	p{
		margin: 15px 0;
	}
	</style>
</head> 
 
<body>
<?include "../files/php/menu.php"?>
<h1>Walne Zgromadzenie Członków Polskiej Federacji Scrabble</h1>

<p>Zgodnie z par. 19 statutu PFS Zarząd postanawia zwołać Walne Zgromadzenie Członków PFS, na którym przekaże sprawozdanie ze 
swoich działań i osiągnięć w ciągu ostatniego roku. Będzie to również okazja do przedyskutowania ewentualnych sugestii i propozycji, które pojawiły się od ostatniego Walnego Zgromadzenia, a także wskazania kierunków działania do ich realizacji.</p>

<p><b>Walne odbędzie się 30 maja w Łodzi (Bałucki Dom Kultury, Ośrodek Kultury "Rondo", ul. Limanowskiego 166).<br>
Pierwszy termin - godzina 18:05, drugi termin 18:20.</b></p>

<p>Zebranie odbędzie się po pierwszym dniu rozgrywek turnieju Wielka Łódka 2009 "Wiosna", na który również gorąco zapraszamy - szczegóły turnieju są dostępne na naszej stronie internetowej.</p>
<p>W związku z tym serdecznie zapraszamy Wszystkich Członków PFS do przybycia na Walne Zgromadzenie.
<br>Więcej szczegółów oraz proponowany porządek obrad reguluje poniższa uchwała Zarządu PFS.</p>

<h2>Uchwała nr 21/2009 z dnia 14.05.2009 w sprawie zwołania Walnego Zgromadzenia Członków PFS</h2>
<p>Zarząd Polskiej Federacji Scrabble działając na podstawie §19 Statutu PFS postanawia zwołać Walne Zgromadzenie w terminie 30 maja 2009 roku w Łodzi. <br>Miejsce - Bałucki Dom Kultury, Ośrodek Kultury "Rondo", ul. Limanowskiego 166. Pierwszy termin - godz. 18:05, drugi termin 18:20.</p>

Proponowany porządek obrad:
<ol>
	<li>Otwarcie i wybór przewodniczącego Walnego Zgromadzenia,</li>
	<li>Wybór Prezydium, protokolanta oraz Komisji: Mandatowo-Skrutacyjnej i Wnioskowej,</li>
	<li>Zatwierdzenie porządku obrad,</li>
	<li>Uchwalenie regulaminu obrad, sprawozdanie Komisji Mandatowej,</li>
	<li>Sprawozdanie Zarządu - przyjęcie sprawozdania Zarządu i udzielenie absolutorium,</li>
	<li>Sprawozdanie Komisji Rewizyjnej,</li>
	<li>Dyskusja programowa,</li>
	<li>Sprawozdanie Komisji Wnioskowej i podjęcie Uchwał,</li>
	<li>Sprawozdanie Komisji Mandatowo-Skrutacyjnej,</li>
	<li>Wolne wnioski,</li>
	<li>Zamknięcie obrad.</li>
</ol>


<?include "../files/php/bottom.php"?>
</body>
</html>