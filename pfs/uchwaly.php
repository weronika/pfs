<?php
include_once "files/php/funkcje.php";
$sql_conn = pfs_connect ();
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Uchwały : Uchwały Zarządu Polskiej Federacji Scrabble</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("pfs","uchwaly");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style>
        .resolution div {
            display: none;
            border: 1px solid #7F7F7F;
            margin: 10px 0;
            padding: 8px;
        }
        .resolution span {
            cursor: pointer;
            font-weight: bold;
            line-height: 1.7em;
        }
        .resolution span:hover {
            text-decoration: underline;
        }
        ul.resolution {
            list-style: none;
            margin-left:-30px;
        }
    </style>
</head>

<body>
<?php require_once "files/php/menu.php"; ?>

<h1><script>naglowek("Uchwały Zarządu PFS")</script></h1>
<p class="nieaktualne">Kolorem czerwonym oznaczone są uchwały, które nie obowiązują na skutek przyjęcia nowych ustaleń w tej samej sprawie.<br><br></p>
<div id="tabs" style="display:none;">
<?
$act_year = date('Y');
print '<ul>';
for ($year = $act_year; $year >= 2004; $year--) {
    print '<li><a href="#tabs-'.$year.'">' . $year . '</a></li>';
}
print '</ul>';

for ($year = $act_year; $year >= 2004; $year--){
    $result = mysql_query("SELECT * FROM $DB_TABLES[resolutions] WHERE data BETWEEN '$year-01-01' AND '$year-12-31' ORDER BY data, CAST( numer AS DECIMAL (3))");
    $num_rows = mysql_num_rows($result);

    if($num_rows){
        print "
            <div id='tabs-$year'>
                <ul class='resolution'>";

        while ($row = mysql_fetch_array($result)) {
            $temp  = explode("-", $row['data']);
            $data  = "$temp[2].$temp[1].$temp[0]";

            print "<li>
                <span ". ($row['nieaktualna'] ? "class='nieaktualne'" : "") ." onclick='$(this).next ().slideToggle (300);'>$row[numer]/$year z dnia $data $row[sprawa]</span>
                <div>$row[tresc]</div>
            </li>";
        }
        print "</ul></div>";
    }
}
?>
</div>
<?php require_once "files/php/bottom.php"; ?>
</body>
</html>
