<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Jolanta_Sylwestrzuk</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Odeszła Jolanta Sylwestrzuk</h1>

<center><img src="../rozne/jsylwestrzuk.jpg"></center><br> <br>
Z przykrością informujemy, że odeszła od nas wieloletnia scrabblistka, Jolanta Sylwestrzuk. Zapamiętamy ją jako skromną, miłą i życzliwą osobę. Jola była żoną zasłużonego dla federacji sędziego Mariusza Sylwestrzuka, znaną z wielkiej pasji do fotografowania ptaków.<br><br> Pogrzeb odbędzie się 7 kwietnia o godz. 14:00 w kościele św. Marka Ewangelisty przy ul. Zamiejskiej 6 w Warszawie. 


<?require_once "../files/php/bottom.php"?>
</body>
</html>
