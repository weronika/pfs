<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Przepisy : Regulamin Sądu Koleżeńskiego</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","statut");</script>
  <style type="text/css">
	p.paragraf{
		text-align:center;
		font-size:12px;
		margin-top: 16px;
		margin-bottom: 6px;
	}
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Regulamin Sądu Koleżeńskiego")</script></h1>

<div class="alignright"><a href="statut.php">Statut PFS</a></div>

<p class="paragraf">§ 1.</p>
<ol>
	<li>Sąd Koleżeński rozpatruje sprawy, przewidziane w Statucie Federacji oraz Regulaminach. W szczególności do kompetencji Sądu należy rozpatrywanie każdego pisemnego wniosku Członka Stowarzyszenia, dotyczącego spraw Stowarzyszenia i jego Członków, poza wnioskami i skargami wniesionymi na Władze Stowarzyszenia.</li>
    <li>Sąd Koleżeński może rozpatrywać również sprawy dotyczące osób niebędących członkami Polskiej Federacji Scrabble, o ile wszystkie zainteresowane osoby wyrażą na to zgodę.</li>
</ol>
<p class="paragraf">§ 2.</p>
Sąd Koleżeński składa się z 3 osób powołanych przez Walne Zgromadzenia Członków na dwuletnią kadencję. Na pierwszym posiedzeniu Sąd Koleżeński wybiera ze swego składu Przewodniczącego.
<p class="paragraf">§ 3.</p>
Sąd rozpatruje skargi wniesione przez Członków PFS na zasadach i w trybie określonym niniejszym regulaminem.
<p class="paragraf">§ 4.</p>
Sąd podejmuje decyzje w pełnym składzie.
<p class="paragraf">§ 5.</p>
Pozwanemu przysługuje prawo do obrony, w szczególności może on przybrać sobie obrońcę spośród Członków Federacji.
<p class="paragraf">§ 6.</p>
<ol>
	<li>Wniosek do Sądu Koleżeńskiego powinien być sporządzony na piśmie i powinien zawierać:
    	<ul>
        	<li>dane osobowe składającego;</li>
            <li>dane osobowe pozwanego;</li>
            <li>szczegółowy opis zdarzenia;</li>
            <li>wniosek o ukaranie;</li>
            <li>ewentualne wskazanie dowodów.</li>
        </ul>
    </li>
    <li>Wnioski składa się Przewodniczącemu Sądu Koleżeńskiego.</li>
</ol>
<p class="paragraf">§ 7.</p>
Wniosek powinien  zostać rozpatrzony przez Sąd w terminie 1 miesiąca od daty wpłynięcia.
<p class="paragraf">§ 8.</p>
<ol>
	<li>Sąd Koleżeński może orzec następujące kary:
    	<ul>
    		<li>upomnienie;</li>
        	<li>naganę;</li>
            <li>zakaz udziału w turniejach PFS na czas określony nie dłuższy niż 3 miesiące;</li>
            <li>karę pieniężną, podlegającą w przypadku jej nieuiszczenia zamianie na zakaz udziału w turniejach.</li>
		</ul>
    </li>
    <li>Sąd może wnioskować do Walnego Zgromadzenia Członków PFS o wykluczenie członka z Federacji.</li>
    <li>Kara pieniężna nie może być wyższa niż roczna składka członkowska w Federacji.</li>
</ol>
<p class="paragraf">§ 9.</p>
Od orzeczeń Sądu Koleżeńskiego stronom przysługuje odwołanie do Walnego Zgromadzenia Członków PFS.
<p class="paragraf">§ 10.</p>
Regulamin niniejszy wchodzi w życie z dniem uchwalenia z późniejszą zmianą z 23.06.2012).

<?require_once "files/php/bottom.php"?>
</body>
</html>
