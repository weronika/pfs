﻿<?require_once "files/php/funkcje.php"?>
<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Grand Prix 2011</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","gp2011")</script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Grand Prix 2011")</script></h1>

<h2>Turnieje Grand Prix 2011</h2>
Cykl Grand Prix 2011 obejmuje 9 turniejów:
<table class="linki ramkadolna">
    <tr><td>1.</td><td><a href="../turniej.php?id=550" target="_blank">IV Walentynki w Sulęcinie</a></td></tr>
    <tr><td>2.</td><td><a href="../turniej.php?id=570" target="_blank">XIII Mistrzostwa Szczecina "Zanim zakwitną magnolie"</a></td></tr>
    <tr><td>3.</td><td><a href="../turniej.php?id=569" target="_blank">III Mistrzostwa Puław</a></td></tr>
    <tr><td>4.</td><td><a href="../turniej.php?id=559" target="_blank">II Mazurski Turniej o Puchar Prezydenta Miasta Ełku</a></td></tr>
    <tr><td>5.</td><td><a href="../turniej.php?id=548" target="_blank">XI Otwarte Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</a></td></tr>
    <tr><td>6.</td><td><a href="../turniej.php?id=555" target="_blank">XV Otwarte Mistrzostwa Krakowa w Scrabble</a></td></tr>
    <tr><td>7.</td><td><a href="../turniej.php?id=546" target="_blank">XV Mistrzostwa Ziemi Słupskiej</a></td></tr>
    <tr><td>8.</td><td><a href="../turniej.php?id=562" target="_blank">VI Mistrzostwa Jaworzna w Scrabble</a></td></tr>
    <tr><td>9.</td><td><a href="../turniej.php?id=571" target="_blank">VII Mistrzostwa Wrocławia w Scrabble</a></td></tr>
</table>
<br><br>

<h2>Zasady punktacji</h2>
<ol>
    <li>Zawodnicy, którzy zajęli w turniejach Grand Prix miejsca od pierwszego do dwudziestego, zdobywają punkty do klasyfikacji łącznej Grand Prix - od 20 za I miejsce (ze zniżką o 1 punkt za każde kolejne) do 1 pkt za 20 miejsce. Zdobywcy 3 czołowych miejsc otrzymują dodatkowo: 5 punktów za miejsce pierwsze (razem 25), 3 punkty za miejsce drugie (razem 22), 1 punkt za miejsce trzecie (razem 19)
     <li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2011 do końcowej klasyfikacji zalicza się siedem najlepszych wyników.</li>
    <li>Przy równej liczbie punktów o kolejności decyduje wyższe miejsce w najlepszym dla danego gracza turnieju, a jeśli to nie da efektu (gracze mieli identyczne najwyższe miejsce) - w kolejnym najlepszym turnieju, itd. Jeżeli w ten sposób nie uda się ustalić kolejności, decyduje losowanie.
    </li>
</ol>

<h2>Nagrody</h2>
Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
Pula na nagrody w cyklu wynosi <b>1800 zł</b>.<br />
Nagrody w Grand Prix 2011 wynoszą:
<ul>
    <li>za I miejsce 700 zł</li>
    <li>za II miejsce 500 zł</li>
    <li>za III miejsce 300 zł</li>
    <li>za IV miejsce 200 zł</li>
    <li>za V miejsce 100 zł</li>
</ul>

<h2>Klasyfikacja cyklu Grand Prix 2011</h2>
<table class="klasyfikacja">
    <tr>
        <td></td>
        <td>Imię i nazwisko</td>
        <td class='suma'>SUMA</td>
        <td>Sulęcin</td>
        <td class='lighter'>Szczecin</td>
        <td>Puławy</td>
        <td class='lighter'>Ełk</td>
        <td>Ostróda</td>
        <td class='lighter'>Kraków</td>
        <td>Słupsk</td>
        <td class='lighter'>Jaworzno</td>
        <td>Wrocław</td>
    </tr>
<?
$sql_conn = pfs_connect ();

$result = mysql_query ("SELECT * FROM `gp2011` ORDER BY `suma` DESC");
$i          = 1;
$pop_suma   = 0;

while ($row = mysql_fetch_array ($result)) {
    print "<td class='lp'>" . ($pop_suma == $row['suma'] ? '&nbsp;' : $i) ."</td>";
    $i++;
    $pop_suma = $row['suma'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td class='suma'>".$row['suma']."</td>";
    print "<td>".$row['sulecin']."</td>";
    print "<td class='lighter'>".$row['szczecin']."</td>";
    print "<td>".$row['pulawy']."</td>";
    print "<td class='lighter'>".$row['elk']."</td>";
    print "<td>".$row['ostroda']."</td>";
    print "<td class='lighter'>".$row['krakow']."</td>";
    print "<td>".$row['slupsk']."</td>";
    print "<td class='lighter'>".$row['jaworzno']."</td>";
    print "<td>".$row['wroclaw']."</td></tr>";
}

mysql_free_result($result);
mysql_close($sql_conn);
?>
</table>

<?include "files/php/bottom.php"?>
</body>
</html>
