<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Prawa autorskie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Prawa autorskie")</script></h1>

<h2>Wszelkie prawa zastrzeżone</h2>
Wszystkie materiały oraz artykuły znajdujące się na stronie są własnością PFS i ich autorów (chyba, że zaznaczono inaczej).<br><br>
Kopiowanie treści z witryny PFS (w tym zamieszczenie na innym serwerze WWW artykułów/relacji pochodzących z witryny PFS) jest dozwolone tylko za zgodą autora.
W celu uzyskania zgody prosimy o kontakt na adres <a onClick="sendMail('pfs','pfs.org.pl')">pfs@pfs.org.pl</a>.<br><br>

W przypadku zamieszczenia materiałów pochodzących z witryny PFS na innym serwisie, należy w nagłówku umieścić wyraźną informację o źródle wraz&nbsp;z&nbsp;<b>aktywnym odnośnikiem</b>:<br>
<div class="aligncenter"><i>„Ten artykuł pochodzi ze strony <a href="http://www.pfs.org.pl">www.pfs.org.pl</a>”</i></div>

<div class="alignright" style="margin-top:30px;"><a href="http://www.winter.pl/internet/prawa.html" target="_blank">Więcej o prawach autorskich w internecie</a></div>
<?require_once "files/php/bottom.php"?>
</body>
</html>

