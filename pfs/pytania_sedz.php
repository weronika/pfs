<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Przepisy : Pytania egzaminacyjne</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","regulaminsedziowski");</script>
  <style type="text/css">
	div.odp{
		font-style: italic;
	}
	li{
		margin: 14px 0;
	}
	div.wzor{
		margin: 12px;
		height: 60px;
		width: 180px;
	}
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Pytania egzaminacyjne")</script></h1>

Zestaw pytań oparty jest na regulaminie turniejowym PFS oraz regulaminie sędziowskim PFS. Zawartość pytań może ulec zmianie, po wcześniejszej akceptacji nowych pytań przez Zarząd PFS.
<ol>
	<li>W jakim wypadku dozwolone jest obracanie planszy podczas partii turniejowej?
		<div class="odp">ODP: Jeśli zgodę wyrazi obu graczy.</div></li>
	
	<li>Czy gracz podczas partii może prowadzić zapiski dowolnej treści?
		<div class="odp">ODP: Tak, ale dopiero po jej rozpoczęciu.</div></li>
	
	<li>Co oznacza zatrzymanie ruchu i kiedy ono następuje?
		<div class="odp">ODP: To chwila, kiedy nie można zmienić ułożenia płytek na planszy ani odwołać decyzji o wymianie lub pauzie. Następuje ono w momencie zatrzymania zegara lub - w wypadku gry bez zegara - po podaniu wartości punktowej ruchu.</div></li>
	
	<li>Za jaką literę należy uznać blanka w sytuacji spornej, gdy żaden z graczy nie zapisał jaką literę on zastępuje?
		<div class="odp">ODP: Żadną. W takiej sytuacji blank staje się martwą płytką do końca gry.</div></li>
		
	<li>Kiedy gracz zgłaszający wymianę traci ruch?
	<div class="odp">ODP: Wtedy, gdy dokonał już trzech wymian bądź w woreczku znajduje się mniej niż siedem płytek.</div></li>

	<li>Ile kolejnych pauz oznacza koniec partii?
	<div class="odp">ODP: Cztery.</div></li>

	<li>Co musi nastąpić, aby można było sprawdzić poprawność wyrazu położonego przez gracza?
	<div class="odp">ODP: Gracz wykonujący ruch musi zatrzymać zegar. Żądanie sprawdzenia zgłoszone przed zatrzymaniem zegara nie ma mocy.</div></li>
	
	<li>Co następuje, jeśli gracz wykonujący ruch zapisał jego wartość punktową i przystępuje do losowania płytek, ale nie wyłączył zegara? 
	<div class="odp">ODP: Wówczas ma obowiązek - na żądanie przeciwnika - wrzucić dobrane płytki do worka lub wszystkie, jeśli zaczął układać je na stojaku. Następnie zatrzymuje zegar.</div></li>
	<li>Co następuje, jeśli gracz dobierający płytki na stojak dobrał część płytek i położył na stojak po czym dobierając kolejne wylosował ich zbyt dużą ilość ?
<div class="odp">ODP: Przeciwnik losuje nadmiarowe płytki ze wszystkich jakie ma gracz dobierający i wrzuca je do worka.</div></li>

	<li>Kiedy przeciwnik ma prawo obejrzeć nadmiarowe płytki wrzucane do worka przez gracza, który dobierając płytki wylosował ich nadmierną ilość, ale nie położył ani jednej na stojak?
	<div class="odp">ODP: Kiedy zawodnik dobierający płytki widział choć jedną z dobieranych płytek.</div></li>
	
	<li>Kiedy niedopuszczalne jest wykreślanie liter na wykreślance podczas partii?
	<div class="odp">ODP: Wówczas, gdy zegar jest zatrzymany lub gracz nie dobrał brakującej ilości płytek z worka po wykonaniu ruchu.</div></li>

	<li>Ile łącznie strat z rzędu kończy partię?
	<div class="odp">ODP: Sześć.</div></li>

	<li>Do którego momentu gra sam drugi z graczy, jeśli pierwszemu skończył się czas?
	<div class="odp">ODP: Do momentu, gdy wyczerpie wszystkie litery z woreczka i stojaka lub spadnie mu czas lub zgłosi dwie kolejne pauzy lub zanotuje trzy kolejne ruchy będące przemieszaniem strat i pauz.</div></li>
	
	<li>Wyjaśnij oznaczenia A, B, N, M we wzorze na wyrówanie: <div class="wzor"></div>
	<div class="odp">ODP:<br />A = miejsce zajmowane przed ostatnim wycofaniem,<br />B = miejsce odpowiadające numerowi startowemu (dla debiutantów liczba ta = część całkowita z liczby 0,7*liczba uczestników),<br />N = liczba rund granych przed ostatnim wycofaniem,<br />M = liczba rund opuszczonych od ostatniego wycofania,</div></li>
	
	<li>Jaka jest graniczna liczba uczestników do poszerzania grup w turnieju przeprowadzanym systemem połówkowym  i o ile maksymalnie wówczas takie poszerzenia mają zachodzić?
	<div class="odp">ODP: 90. Dla więcej niż 90 osób maksymalne poszerzenie to 2,5pkt, a dla co najwyżej 90 osób 2pkt.</div></li>
	
	<li>Czy może zajść sytuacja, gdy przeciwnik zdejmie graczowi słowo, które ten ułożył choć znajduje się ono w słowniku, w którym było sprawdzane?
	<div class="odp">ODP: Tak, gdy do sprawdzenia pójdzie tylko jeden zawodnik. Wówczas zawodnik, który został przy stole nie ma prawa kwestionować wyniku sprawdzenia.</div></li>
	
	<li>Który moment ruchu jest ostatecznym momentem, po którym nie można już zakwestionować słowa ułożonego przez przeciwnika?
	<div class="odp">ODP: Zapisanie przez gracza ruchu przeciwnika.</div></li>
	
	<li>Kiedy można skorygować błędy w obliczeniach wartości ruchu lub sumy punktów w partii?
	<div class="odp">ODP: W każdym momencie partii, gdy zegar jest zatrzymany lub po partii do czasu podpisania protokołu przez obu graczy.</div></li>
	
	<li>Do którego momentu trwania turnieju możliwa jest poprawka wyniku zgodnie potwierdzonego przez obu graczy, aczkolwiek błędnie wpisanego do protokołu?
	<div class="odp">ODP: Do czasu rozstawienia przez sędziego następnej rundy (lub do chwili oficjalnego zakończenia turnieju, jeśli błąd dotyczy ostatniej rundy), a za zgodą sędziego do zakończenia danego dnia zawodów.</div></li>
	
	<li>Kiedy podczas partii wolno zatrzymać zegar? 
	<div class="odp">ODP: W celu dania znaku, że wykonany ruch jest ostateczny lub usunięcia ze stojaka nadmiernej ilości płytek lub wezwania sędziego lub wezwania osoby do policzenia płytek w woreczku.</div></li>
	
	<li>Jak należy postąpić w sytuacji, gdy gracz sprawdzający wpisze do komputera wyraz w innej formie niż ta, która znajduje się na planszy?
	<div class="odp">ODP: Ukarać sprawdzającego odebraniem możliwości sprawdzenia danego wyrazu.</div></li>
	
	<li>W jakim stosunku przyznaje się walkower w wypadku wykluczenia jednego z graczy?
	<div class="odp">ODP: 400:1, chyba że gracz niewykluczony uzyskał już w momencie wykluczenia więcej niż 400 punktów, wówczas zachowuje taką ich ilość jaką miał w momencie wykluczenia.</div></li>
	
</ol>

<?require_once "files/php/bottom.php";?>
</body>
</html>

