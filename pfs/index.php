<? require "files/php/funkcje.php";
$month_mianownik  = array ("styczeń", "luty",  "marzec","kwiecień","maj", "czerwiec","lipiec","sierpień","wrzesień","październik", "listopad",  "grudzień");
$month_dopelniacz = array ("stycznia","lutego","marca", "kwietnia","maja","czerwca", "lipca", "sierpnia","września","października","listopada", "grudnia");

function calendar_date_small ($d) {
    global $month_mianownik, $month_dopelniacz;

    $od     = explode ("-", $d);
    $data   = mktime (0, 0, 0, $od[1], $od[2], $od[0]);
    $wynik  = strftime ("<span class='big'>%e</span><br />%B", $data);
    return str_replace ($month_mianownik, $month_dopelniacz, $wynik);
}

function calendar_date_big ($d1, $d2) {
    global $month_mianownik, $month_dopelniacz;

    $od         = explode ("-", $d1);
    $do         = explode ("-", $d2);
    $data_od    = mktime (0, 0, 0, $od[1], $od[2], $od[0]);
    $data_do    = mktime (0, 0, 0, $do[1], $do[2], $do[0]);

    $wynik = ($data_od != $data_do
        ? strftime ("<b>%e", $data_od) . "-" .strftime("%e %B</b>", $data_do)
        : strftime ("<b>%e %B</b>", $data_od)
    );

    return str_replace($month_mianownik, $month_dopelniacz, $wynik);
}

$live_tours = pfs_select (array (
    fields  => array ('id', 'nazwa'),
    table   => $DB_TABLES[tours],
    where   => array ( '!show_live' => '', '>=data_do' => 'ADDDATE(NOW(), -2)')
));

$next_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( '>=data_od' => 'DATE(NOW())', '<rank' => $TOUR_STATUS[vacation]),
    order   => array ( 'data_od' ),
    limit   => 3
));

$tab_stories = pfs_select (array (
    table => $DB_TABLES[tours],
    where => array ( 'status' => 'short' ),
    order => array ( '!data_do' )
));

$stories = pfs_select (array (
    table => $DB_TABLES[tours],
    where => array ( 'status' => 'main' ),
    order => array ( '!data_do' )
));

$gp_top = pfs_select (array (
    fields  => array ('osoba', 'suma'),
    table   => $DB_TABLES[gp2013],
    order   => array ( '!suma' ),
    limit   => 3
));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Polska Federacja Scrabble - oficjalna witryna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style-index.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script type="text/javascript">
        jSubmenu("glowna","aktualnosci"); jKalendarz();

        $(document).ready(function(){
            var switches = $('.rel_switch');
            var current = 0;

            switches.bind ('click', function(){
                current = switches.index (this);
                $('.rel_container').hide ();
                $('#relacja' + current).show ();
            });
            $('#relacja' + current).show();
        });
    </script>
</head>

<body>
<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4&appId=1608158549459689";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<? include_once "files/php/menu.php"; ?>

<div id="boxy">
    <?
    foreach ($live_tours as $tour) {
        print "<div class='box' style='width:165px;'><a href='http://www.pfs.org.pl/live/index.php?id=$tour->id'>NA ŻYWO!<br />$tour->nazwa</a></div>";
    }
    ?>
<!--
<div class='box1' style='width:150px;'><a href='....php'>... <br>...</a></div> -->
<!--
<div class='box' style='width:170px;'><a href='http://www.pfs.org.pl/rozne/walne2015.pdf'>Walne Zgromadzenie<br>Członków Polskiej Federacji Scrabble</a></div>
-->
<!--
<div class='box' style='width:170px;'><a href='http://www.pfs.org.pl/rozne/eng2015.pdf'>Eliminacje do Mistrzostw Świata<br>w Scrabble po angielsku<br></a></div>
-->
<!--
<div class='box' style='width:135px;'><a href='osps_aktualizacje.php'>OSPS wersja 2.2 (UPDATE 32)</a></div>
-->


</div>

<!--NAJBLIŻSZE TURNIEJE-->
<div id="nextTours">
    <?
    $i = 1;
    foreach ($next_tours as $tour) {
        print "
            <div class='nextTour' onclick='location.href=\"turniej.php?id=$tour->id\"'>
                <div class='karteczka small$i'>
                    <span class='miasto'>$tour->miasto</span>" .
                    ($tour->data_od == date("Y-m-d")
                        ? "<span class='dzien'><span class='big'>już<br />dziś!</span></span>"
                        :"<span class='dzien'>". calendar_date_small ($tour->data_od) ."</span>" ) .
                "</div>
                <div class='kartka big".$i++."'>
                    <span class='miasto'>$tour->miasto</span>
                    <span class='dzien'>". calendar_date_big ($tour->data_od, $tour->data_do) ."</span>
                    <span class='nazwaturnieju'>$tour->nazwa</span>" .
                    ($tour->rank == $TOUR_STATUS[gp]
                        ? "<span class='gp'>Grand&nbsp;Prix&nbsp;". substr ($tour->data_do, 0, 4) ."</span>"
                        : "" ) .
                    ($tour->region != ''
                        ? "<span class='mr'>Mistrzostwa&nbsp;Regionów&nbsp;". substr ($tour->data_do, 0, 4) ."</span>"
                        : "" ) .
                "</div>
            </div>";
    }
    ?>
</div>

<div id='panellewy'>

<!--<div class='relacja' style='clear:both;'>
    <img class='onleft' src='rozne/kartkabn.jpg' alt='Wesołych Świąt!' />
     <div style='clear:both;margin-top:25px;'></div>
</div>
   -->

    <div class="fb-page" data-href="https://www.facebook.com/PolskaFederacjaScrabble" data-width="320" data-height="70" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/PolskaFederacjaScrabble"><a href="https://www.facebook.com/PolskaFederacjaScrabble">Polska Federacja Scrabble</a></blockquote></div></div>

    <div class='naglowekrelacji'><span class='tytul'>Zmiany w przepisach i regulaminach turniejowych obowiązują od 3 sierpnia!<span class='nazwa'></span></span></div>

    <div>
   Zarząd PFS uchwałą 13/2015 przyjął nowe regulaminy rozgrywek: turniejowy, sędziowski oraz rankingowy. Po otrzymaniu zgłoszeń i uwag wprowadzone zostały uchwałą 15/2015 kosmetyczne
   poprawki nie wpływające na całokształt i rozpoczęcie obowiązywania przepisów. Nowe przepisy obowiązywać będą od najbliższych turniejów w Wałczu.
<br><br>
   Poniżej obowiązujące regulaminy (zieloną czcionką oznaczono wzmiankowane wprowadzone poprawki)
<br><br>
1. <a href="rrregtur2015.pdf">Regulamin turniejowy</a>,<br>
2. <a href="rrregsed2015.pdf">Regulamin sędziowski</a>,<br>
3. <a href="rrregrank2015.pdf">Regulamin rankingowy</a>.
    <br><br></div>


  <div style='clear:both;margin-top:25px;'>

        <!--RELACJE GŁÓWNE-->

		<?
        print "<div class='rel_switch' title='".$stories[0]->nazwa."'>".$stories[0]->miasto."</div>";
        foreach ($tab_stories as $tour) {
            print "<div class='rel_switch' title='$tour->nazwa'>$tour->miasto</div>";
        }

        $stories = array_merge ($stories, $tab_stories);
        $tab     = 0;
        foreach ($stories as $story) {
            if ($story->status != 'main')
                print "</div>";

            if ($story->status != 'main' || !$tab)
                print "<div class='rel_container' id='relacja". $tab++ ."'>";

            menuTurnieju ($story, true);
            pokazRelacje ($story, ($tab == 1));
        }
        print "</div>";
        ?>
    </div>


</div>

<div id='panelprawy'>
    <div class='panel'>

        <div><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="200" height="40" id="nowoscilink" align="middle"><param name="allowScriptAccess" value="sameDomain" /><param name="allowFullScreen" value="false" /><param name="movie" value="files/img/nowosci.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="bgcolor" value="#ffffff" /><embed src="files/img/nowosci.swf" quality="high" wmode="transparent" bgcolor="#ffffff" width="200" height="40" name="nowoscilink" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://get.adobe.com/flashplayer/otherversions/" /></object></div>
        <div><a href="http://www.scrabblewszkole.pl/" target="_blank"><img src="rozne/sws.jpg" alt="Scrabble w szkole"></a></div>

        <div><a href="http://www.kurnik.pl/literaki/" target="_blank"><img src="rozne/kurnik.gif" alt="Kurnik"></a></div>
        <div><a href="http://www.zagraj.pl/" target="_blank"><img src="rozne/zagraj.gif" alt="Zagraj"></a></div>
        <!--  <div class="box" style='width:175px;'>
            <a href="wolontariatWSC.php">Zostań wolontariuszem podczas Mistrzostw Świata po angielsku</a>
        </div><br>  -->
        <div><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="200" height="72" id="siodemka" align="middle"><param name="allowScriptAccess" value="sameDomain" /><param name="allowFullScreen" value="false" /><param name="movie" value="files/img/siodemka.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="bgcolor" value="#ffffff" />  <embed src="files/img/siodemka.swf" quality="high" wmode="transparent" bgcolor="#ffffff" width="200" height="72" name="siodemka" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://get.adobe.com/flashplayer/otherversions/" /></object></div>

         <div><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="200" height="54" id="anagramator" align="left"><param name="allowScriptAccess" value="sameDomain" /><param name="allowFullScreen" value="false" /><param name="movie" value="files/img/anagramator.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="bgcolor" value="#ffffff" /><embed src="files/img/anagramator.swf" quality="high" wmode="transparent" bgcolor="#ffffff" width="200" height="54" name="anagramator" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://get.adobe.com/flashplayer/otherversions/" />  </object></div>


</div>


     <div class='panellong'>

        <!--AKTUALNA CZOŁÓWKA RANKINGU-->
        <div class="text alignleftinfo">
	<a href='ranking.php'><h2>Czołówka rankingu</h2></a>
	<? print wyciagnijCzolowke (3); ?></div>

        <!--AKTUALNA CZOŁÓWKA GRAND PRIX-->
        <div class="text alignleftinfo">
	<a href='gp2015.php'><h2>Grand Prix 2015</h2></a>
	            <table  width=100% align=right>
        <tr style='color:#E4CC1F'><td width=80%>Michał Alabrudziński</td><td>96</td></tr>
        <tr style='color:#DDDAD5'><td>Juliusz Czupryniak</td><td>63</td></tr>
        <tr style='color:#AA0000'><td>Anita Bruska</td><td>34</td></tr>
        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		</table>
        </div>
        <!--AKTUALNA CZOŁÓWKA MISTRZOST REGIONÓW-->
        <div class="text alignleftinfo">
            <a href='mr2015.php'><h2>Mistrzostwa Regionów 2015</h2></a>
            <table width=100% align=right>
		<tr><td colspan=2><b>Pomorze</b></td></tr>
         <tr><td width=80%>1. Kazimierz Merklejn</td><td>75</td></tr>
         <tr><td colspan=2><b>Południe</b></td></tr>
         <tr><td>1. Łukasz Bobowski</td><td>48</td></tr>
         <tr><td colspan=2><b>Centrum i Mazury</b></td></tr>
         <tr><td>1. Michał Alabrudziński</td><td>54</td></tr>
            </table>
        </div>
    </div>
</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
