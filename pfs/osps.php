<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Oficjalny Słownik Polskiego Scrabblisty</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","osps");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("OSPS")</script></h1>

<img src="files/img/osps2.jpg" class="onright" alt="osps" />
<b>Oficjalny Słownik Polskiego Scrabblisty</b> ukazał się w roku 1998 r. wspólnym nakładem Polskiej Federacji Scrabble oraz <a href="http://www.pwn.com.pl" target="_blank">Wydawnictwa Naukowego PWN</a>. Od czasu ukazania się zawartość słownika wielokrotnie ulegała zmianie. Wynikało to m.in. z dodania do kanonu dopuszczalnych źródeł słów w grze nowych słowników jak również z zauważania błędów zawartości słownika. Najnowszą aktualizację słownika poprzedniej wersji (już wyczerpanej) można znaleźć <a href="osps_aktualizacje.php">tu</a>.<br /><br />

Od 29 kwietnia 2005 roku można kupić <b>OSPS</b> w wersji 2.2, zawierający prawie milion słów więcej i wzbogacony o nowe funkcje.<br />
<i><b>Uwaga! Słownik jest dostępny jedynie w wersji elektronicznej na CD-ROM-ie</b>.</i><br /><br />

Słownik można zamówić przez Infolinię PWN (tel. 0-801 351 929)<br />
oraz w <a href="http://ksiegarnia.pwn.pl/3729_pozycja.html" target="_blank">internetowej księgarni PWN</a> lub na stronie <a href="http://www.mirelka.pl/sklep/product_info.php?products_id=129" target="_blank">www.mirelka.pl</a>

<div class="alignright">
<a href="osps_aktualizacje.php">Aktualizacje do OSPS</a><br />
<a href="zds.php">Zasady dopuszczalności słów</a><br />
<a href="scrabblenet.php#slowniki">Słowniki on-line</a><br />
</div>

<div style="clear:both;"></div>
Nowy OSPS składa się z następujących modułów: Arbiter, Lista, Anagramy, Przedłużki, Trener i Analizator.
<h2>ARBITER</h2>
<a href="files/img/osps-arbiter.jpg" target="_blank"><img src="files/img/osps-arbiter.jpg" class="onright" alt="arbiter" width="239" height="200" /></a>
Większa „mordka” i użycie myszki bezpośrednio na niej. Dwa tryby: normalny i turniejowy różniące się użyciem skrótów klawiszowych. W trybie turniejowym 
został zablokowany skrót Alt+S, zamiast niego funkcję sprawdzenia przejęła kombinacja Ctrl+Spacja. Podyktowane jest to możliwością brzemiennej w skutki pomyłki przy 
sprawdzaniu, w przypadku niejednoczesnego użycia klawisza S i Alt. Dodatkowo w trybie turniejowym jest możliwość rejestracji ilości i trafności sprawdzeń 
bezpośrednio na ekranie.

<h2>ANAGRAMY</h2>
<a href="files/img/osps-anagramy.jpg" target="_blank"><img src="files/img/osps-anagramy.jpg" class="onright" alt="anagramy" width="239" height="200"/></a>
Możliwości anagramowania zostały rozszerzone o funkcje ustawienia dowolnej litery na wybranym miejscu, wybrania kilku liter, samogłosek lub spółgłosek lub 
dowolnej kombinacji liter na jednym lub kilku miejscach. Moduł pozwala na anagramowanie również wyrazów krótszych niż liczba liter wpisana, oczywiście z użyciem wszystkich wpisanych liter. Wybieranie liter i ustawianie zadanych funkcji odbywa się w wygodnym trybie graficznym. Blank może być wybrany zarówno poprzez gwiazdkę (*) lub znak zapytania (?). Moduł spełnia również funkcję słownika rymów.

<h2>PRZEDŁUŻKI</h2>
Ten moduł został zmieniony o bardziej czytelne przedstawienie liter o które można przedłużyć wybrany wyraz.

<h2>TRENER</h2>
<a href="files/img/osps-trener.jpg" target="_blank"><img src="files/img/osps-trener.jpg" class="onright" alt="trener" width="239" height="200" /></a>
To jeden z dwu zupełnie nowych modułów w OSPS-ie pozwalający na naukę anagramowania poprzez: edycję losowo wybranych wyrazów od cztero- do dziewięcioliterowych, a następnie danie możliwości przestawiania płytek na stojaku co ułatwia znalezienie wyrazu w z góry zaplanowanym czasie. Dostępne są dwa tryby edycji; trening i pojedynek. W trybie treningowym znalezienie wyrazu odbywa się w nieograniczonym czasie i jest dostępne rozwiązanie. W trybie pojedynku istnieje ograniczony czas na znalezienie wyrazu oraz można grać w kilka osób które otrzymują do anagramowania te same zestawy liter.<br />
Dostępne opcje to ilość liter, czas oraz ilość zestawów. Z każdego zestawu można ułożyć jeden lub więcej wyrazów.

<h2>ANALIZATOR</h2>
<a href="files/img/osps-analiza.jpg" target="_blank"><img src="files/img/osps-analiza.jpg" class="onright" alt="analizator" width="239" height="200" /></a>
I drugi bardzo ciekawy i pożyteczny moduł pozwalający na analizę rozegranej partii. Posiadany zapis jpg można nanosić w trybie graficznym na 
planszę ruch po ruchu, wykorzystując również stojaki i analizować możliwości jakie zaistniały w każdym ruchu. Działanie modułu polega na wyedytowaniu wszystkich wyrazów które daje się ułożyć z zestawu liter na stojaku wraz z wartością punktowa oraz wskazaniu miejsca gdzie wybrany wyraz ułożyć (pojedyncze klikniecie myszki na wybranym wyrazie spowoduje podświetlenie miejsca na planszy). Powtórne kliknięcie myszki skutkuje przeniesieniem liter ze stojaka na planszę. Analiza własnej partii przy użyciu tego modułu może być nieco frustrująca, bo dowiemy się dokładnie ilu siódemek nie znaleźliśmy oraz jakie lepsze wyrazy można było w danym ruchu ułożyć.

><?require_once "files/php/bottom.php"?>
</body>
</html>

