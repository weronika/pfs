<? include_once "files/php/funkcje.php";

$tour = pfs_select_one (array ( 
    table   => $DB_TABLES[tours], 
    where   => array ( id => $_GET['id'] )
));
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: <?print $tour->nazwa;?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","aktualnosci");</script>
</head>

<body>
<?
require_once "files/php/menu.php";
pokazRelacje($tour);
require_once "files/php/bottom.php"
?>
</body>
</html>
