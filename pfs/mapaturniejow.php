<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Turnieje : Wielka Mapa Turniejów Scrabblowych</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","archiwum");</script>
 	<style type="text/css">
	#mapa{
		position:relative;
		text-align:center;
		margin: 0 auto;
		width: 830px;
		overflow: visible;
	}
	#mapa .miasto{
		position: absolute;
		background: url('files/img/miasto.png') no-repeat 0 0;
		text-align: left;
		cursor: pointer;
		min-width: 11px;
		min-height: 11px;
	}
	#mapa .miasto span{
		color: white;
		font-size: 9px;
		display: none;
		line-height: 14px;
		white-space: nowrap;
		padding: 3px 6px;
		height: 14px;
		border:1px solid #333;
		background: url('files/img/miasto-bkg.png') repeat-x #2C4C59;
		margin-left: 12px;
		margin-top: -10px;
		font-weight: bold;
	}
	#mapa div.lista{
		position: absolute;
		background: white;
		z-index:4;
		text-align:left;
		opacity:0.9;
		filter:alpha(opacity=90);
		padding-bottom: 20px;
		max-height: 350px;
		overflow: auto;
		border:1px solid #ddd;
		display:none;
	}
	#mapa div.lista div.close{
		float:right;
		width: 10px;
		height: 10px;
		background: red;
		display:none;
	}
	#mapa div.lista span, #mapa div.lista h3{
		white-space: nowrap;
		display: block;
		margin: 4px 20px;
	}
	#mapa div.lista h3{
		margin:20px;
		color: #1576AE;
		border-bottom: 1px solid #1576AE;
		text-align: center;
		padding-bottom:2px;
	}
 	</style>
	<script>
	$(document).ready(function(){
		var listy = $('.lista');
		var kulki = $('.miasto');

		listy.hide();
		kulki.bind('click', function(e){
			listy.hide('fast');
			$(this).children('span').hide('fast');
			$(this).css('z-index', 1);
			var lista = $(this).next();
			lista.slideDown('slow', ustaw($(this), lista));
		});
		kulki.bind('mouseenter', function(){
			$(this).css('z-index', 3);
			$(this).children('span').show('fast');
		});
		kulki.bind('mouseleave', function(){
			$(this).children('span').hide('fast');
			$(this).css('z-index', 1);
		});
		$('#mapa').bind('click', function(e){
			//var lista = $(this).parent();
			//lista.hide('fast');
			if(e.target.nodeName == "IMG")
				listy.slideUp('fast');
		});

		function ustaw(kulka, lista){
			x = kulka.position().left - (lista.width()/2);
			y = kulka.position().top + kulka.height() + 4;

			if(x + lista.width() > 830)
				x = 830 - lista.width();
			if(x < 0)
				x = 0;

			if(y + lista.height() > 740){
				margines = 45;
				(y - lista.height() - margines < 0)	?	y = 750 - lista.height() - margines	:	y = y - lista.height() - margines;
			}

			lista.css('left',x);
			lista.css('top',y);
		}
});

	</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Wielka Mapa Turniejów Scrabblowych")</script></h1>

<form action="mapaturniejow.php" method="post">

</form>

<div id="mapa">
	<img src="files/mapaturniejow.png" usemap="#mapa" alt="mapa" />
<?
$miasta = array('Ustka' => array(228, 25), 'Słupsk' => array(242, 42), 'Kościerzyna' => array(316, 89), 'Szczecin' => array( 35,176), 'Gorzów Wielkopolski' => array( 81,270), 'Sulęcin' => array( 71,306), 'Bydgoszcz' => array(323,217), 'Toruń'  => array(368,234), 'Poznań' => array(226,306), 'Władysławowo' => array(351,  0), 'Rumia'  => array(352, 31), 'Ostróda' => array(486,139), 'Iława' => array(452,152), 'Nowe Miasto Lubawskie' => array(455,172), 'Łomża'  => array(658,209), 'Bielsk Podlaski' => array(752,252), 'Nowy Dwór Mazowiecki' => array(543,304), 'Milanówek' => array(535,341), 'Warszawa' => array(572,327), 'Łódź'  => array(434,394), 'Kraków' => array(477,609), 'Zakopane' => array(478,703), 'Lublin' => array(693,457), 'Koszalin' => array(172, 84), 'Mielno' => array(158, 67), 'Inowrocław' => array(337,259), 'Wrocław' => array(238,471), 'Legnica' => array(168,457), 'Wójtowice' => array(203,574), 'Tarnów' => array(505,605), 'Katowice' => array(407,587), 'Jaworzno' => array(425,597), 'Bielsko-Biała' => array(402,642), 'Zabrze' => array(372,572), 'Kutno'  => array(434,332), 'Wisła'  => array(380,662), 'Burzenin' => array(387,423), 'Graboszyce' => array(440,621), 'Załęcze' => array(372,478), 'Piastów' => array(552,329), 'Sudomie' => array(307, 80), 'Wałcz'  => array(187,198), 'Szarlota' => array(309,100), 'Krzeszów' => array(158,522), 'Chojnów' => array(150,449), 'Kościan' => array(199,350), 'Kościelisko' => array(467,703), 'Jurata' => array(382, 12), 'Betlejem' => array(147,520), 'Lipiany' => array( 65,230), 'Częstochowa' => array(410,515), 'Tychy'  => array(405,607), 'Chorzów' => array(392,578), 'Gdynia' => array(365, 38), 'Olsztyn' => array(524,126), 'Biała Podlaska'  => array(739,354), 'Dębno Lubuskie'  => array( 41,265), 'Pruszków' => array(548,339), 'Grójec' => array(558,374), 'Tczew'  => array(379, 88), 'Karpacz' => array(126,510), 'Zawichost' => array(638,511), 'Rzeszów' => array(645,608), 'Opole'  => array(313,533), 'Mielec' => array(596,579), 'Radom'  => array(574,440), 'Płock'  => array(450,297), 'Puławy' => array(642,437), 'Kielce' => array(532,507), 'Białystok' => array(744,208), 'Włocławek' => array(404,273), 'Wałbrzych' => array(150,509), 'Kalisz' => array(319,391), 'Międzyzdroje' => array( 13,113), 'Zaniemyśl' => array(241,341));

$sql_conn = pfs_connect ();
$result = mysql_query("SELECT DISTINCT miasto FROM $DB_TABLES[tours] WHERE miasto<>''");

while($row = mysql_fetch_array($result)){
	$m = $row['miasto'];
	if (array_key_exists($m, $miasta)) {
		print "<div class='miasto' style='left:".$miasta[$m][0]."px;top:".$miasta[$m][1]."px;'><span>".$m."</span></div>";
		print "<div class='lista'><h3>".$m."</h3>";
		$res = mysql_query("SELECT data_od, nazwa, id FROM $DB_TABLES[tours] WHERE miasto='".$m."' ORDER BY data_od DESC");
		while($r = mysql_fetch_array($res)){
			print "<span><a href='turniej.php?id=".$r['id']."' target='_blank'>".$r['data_od']." — ".$r['nazwa']."</a></span>";
		}
		print "</div>";
	}
}
?>
</div>

<?require_once "files/php/bottom.php"?>
</body>
</html>
