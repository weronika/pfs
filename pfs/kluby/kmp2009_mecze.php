<html>
<head>
	<title>Polska Federacja Scrabble :: KMP'09 - mecze</title>
	<link rel="shortcut icon" href="../pliki_strony/img/favicon.ico">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="../pliki_strony/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../pliki_strony/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../pliki_strony/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../pliki_strony/styleswitcher.js"></script>
	<script type="text/javascript" src="../pliki_strony/java.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../pliki_strony/pngfix.js"></script><![endif]-->
	<style>
	.klasyfikacja td{
		text-align: left;
		padding: 10px !important;
	}
	.klasyfikacja .mecz, .klasyfikacja tr:first-child td{
		text-align: center !important;
		font-weight: bold;
	}
	</style>
</head>


<body>
<?include "../pliki_strony/menu.php"?>
<h1>KMP '09 - mecze</h1>

<table class="klasyfikacja">        
	<tr><td colspan="2">Piątek</td><td colspan="2">Sobota</td><td colspan="2">Niedziela</td></tr>
	<tr><td class="mecz">Mecz 1<br>(ok. 11:30)</td><td>
	Macaki I &mdash Anagram<br>
	GKS &mdash Same Premie<br>
	Szkrab   —   Ghost<br>
	ŁKMS II   —   ŁKMS I<br>
	Siódemka   —   Macaki II<br>
	<i>Mikrus   —   bye</i>
	</td> 
	<td class="mecz">Mecz 5<br>(ok. 9:30)</td><td>      
	Macaki I   —   Same Premie<br>
	Anagram   —   Ghost<br>
	GKS   —   Mikrus<br>
	Szkrab   —   ŁKMS I<br>
	ŁKMS II   —   Macaki II<br>
	<i>Siódemka   —   bye</i>
	</td>
	<td class="mecz">Mecz 9<br>(ok. 9:00)</td><td>      
	Macaki I   —   Szkrab<br>
	ŁKMS II   —   Anagram<br>
	Macaki II   —   Same Premie<br>
	Siódemka   —   Ghost<br>
	ŁKMS I   —   Mikrus<br>
	<i>GKS   —   bye</i>
	</td></tr>

	<tr><td class="mecz">Mecz 2<br>(ok. 13:30)</td><td>
	Macaki II   —   Macaki I<br>
	ŁKMS II   —   Siódemka<br>
	Mikrus   —   Szkrab<br>
	Ghost   —   GKS<br>
	Same Premie   —   Anagram<br>
	<i>ŁKMS I   —   bye</i>
	</td>
	<td class="mecz">Mecz 6<br>(ok. 11:30)</td><td>      
	Macaki I   —   GKS<br>
	Szkrab   —   Anagram<br>
	ŁKMS II   —   Ghost<br>
	Macaki II   —   Mikrus<br>
	Siódemka   —   ŁKMS I<br>
	<i>Same Premie   —   bye</i>
	</td>
	<td class="mecz">Mecz 10<br>(ok. 11:00)</td><td>        
	Macaki I   —   Ghost<br>
	Same Premie   —   Mikrus<br>
	Anagram   —   ŁKMS I<br>
	Siódemka   —   GKS<br>
	Szkrab   —   Macaki II<br>
	<i>ŁKMS II   —   bye</i>
	</td></tr>

	<tr><td class="mecz">Mecz 3<br>(ok. 16:30)</td><td>
	Szkrab   —   ŁKMS II<br>
	Macaki II   —   GKS<br>
	Anagram   —   Siódemka<br>
	ŁKMS I   —   Same Premie<br>
	Mikrus   —   Ghost<br>
	<i>Macaki I   —   bye</i>
	</td>
	<td class="mecz">Mecz 7<br>(ok. 14:30)</td><td>         
	ŁKMS I   —   Macaki I<br>
	Mikrus   —   Siódemka<br>
	Ghost   —   Macaki II<br>
	Same Premie   —   ŁKMS II<br>
	GKS   —   Szkrab<br>
	<i>Anagram   —   bye</i>
	</td>
	<td class="mecz">Mecz 11<br>(ok. 13:00)</td><td>        
	Macaki I   —   Mikrus<br>
	Ghost   —   ŁKMS I<br>
	Siódemka   —   Same Premie<br>
	Anagram   —   Macaki II<br>
	GKS   —   ŁKMS II<br>
	<i>Szkrab   —   bye</i>
	</td></tr>

	<tr><td class="mecz">Mecz 4<br>(ok. 18:30)</td><td>      
	Macaki I   —   Siódemka<br>
	ŁKMS I   —   Macaki II<br>
	Mikrus   —   ŁKMS II<br>
	Same Premie   —   Szkrab<br>
	Anagram   —   GKS<br>
	<i>Ghost   —   bye</i>
	</td>
	<td class="mecz">Mecz 8<br>(ok. 16:30)</td><td>        
	Macaki I   —   ŁKMS II<br>
	Szkrab   —   Siódemka<br>
	ŁKMS I   —   GKS<br>
	Mikrus   —   Anagram<br>
	Ghost   —   Same Premie<br>
	<i>Macaki II   —   bye</i>
	</td>
	<td></td><td>        
	</td></tr>
</table>

<?include "../pliki_strony/bottom.php"?>
</body>
</html>

