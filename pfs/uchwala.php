<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Uchwały : Uchwały Zarządu Polskiej Federacji Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","uchwaly");</script>
</head>

<body>
<?$rok = $_GET['rok'];?>
<?require_once "files/php/menu.php"?>


<h1><script>naglowek("Uchwały Zarządu PFS")</script></h1>

<!--podjęte w roku <?print $rok;?>-->



<?
	$sql_conn = pfs_connect ();
	$result = mysql_query("SELECT * FROM $DB_TABLES[resolutions] WHERE data BETWEEN '".$rok."-01-01' AND '".$rok."-12-31' ORDER BY data ASC, numer ASC");
	$num_rows = mysql_num_rows($result);
	if($num_rows){
		while($row = mysql_fetch_array($result)){
			$arr = explode("-", $row['data']);
			$data = $arr[2].".".$arr[1].".".$arr[0];
			print "<div class='uchwala' id='uchwala".$row['numer']."'>";
			print "<strong>Uchwała nr ".$row['numer']."/".$rok." z dnia ".$data." ".$row['sprawa']."</strong>";
			if($row['nieaktualna'])		print "<div class='nieaktualne'>";
			print $row['tresc'];
			if($row['nieaktualna'])		print "</div>";
			print "</div>";
		}
	}
?>

<?require_once "files/php/bottom.php";?>
</body>
</html>
