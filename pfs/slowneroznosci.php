<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Scrabble : Słowne różności</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("scrabble","slowneroznosci");</script>
  <style type="text/css">
      table.linki td{
        padding-right: 50px;
        vertical-align: top;
    }
    h3{
        margin: 20px 0 10px 0;
        text-align: left;
    }
    table.slowa{
        border-spacing: 0;
        margin-top: 10px;
        border-collapse: collapse;
    }
    table.slowa td{
        padding: 10px 20px 10px 0;
        vertical-align: top;
    }
    table.slowa td span{
        font-family: "Verdana";
        font-size: 11px;
        font-style: italic;
        font-weight: normal;
        display: block;
    }
    table.slowa td:first-child{
        font-weight: bold;
        width: 120px;
    }
    div.rekord{
        margin: 16px 50px;
        text-align: center;
        color: #E7484F;
        font-size: 13px;
        font-style: normal;
        font-weight: bold;
        display: block;
        line-height: 20px;
    }
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Słowne różności")</script></h1>

<table class="linki">
    <tr>
        <td>
            <a href="#slowa">Najdroższe słowa</a><br />
            <a href="#otwarcie">Najlepsze otwarcie</a><br />
            <a href="#siedem">Najlepsze zestawy liter</a><br />
            <a href="#szesc-b">Najlepszy zestaw sześciu liter z blankiem</a><br />
            <a href="#siedem-k">Najlepszy zestaw siedmiu liter do krzyżowania</a><br />
            <a href="#slowo27">Najdroższe słowo na planszy</a><br />
            <a href="#ruch">Najwyżej punktowany ruch</a><br />
            <a href="#samotnik">Najwyższy wynik w samotnej grze</a><br />
            <a href="#niedasie">Słowa, których się nie da ułożyć</a><br />
            <a href="#bloki">Najszybsze bloki</a>
        </td>
        <td>
            <a href="#nazwywl">Nazwy niekoniecznie własne</a><br />
            <a href="#marki">Marki</a><br />
            <a href="#oczyo">O czy Ó</a><br />
            <a href="#wstretne">Wstrętne zestawy siódemkowe</a><br />
            <a href="#tryplety">Trzy takie same litery</a><br />
            <a href="#multiplety">Cztery i więcej takich samych liter</a><br />
        </td>
    </tr>
</table>

<h2 id="slowa">Najdroższe słowa</h2>
<ul>
    <li><b>Dwuliterowe - 8 pkt.</b>: <span class="slowa">EŃ, FU, GĘ, HĘ, JĄ, OŃ, UF</span></li>
    <li><b>Trzyliterowe - 16 pkt.</b>: <span class="slowa">ŻĄĆ, ŻĘĆ</span></li>
    <li><b>Czteroliterowe - 23 pkt.</b>: <span class="slowa">PÓŹŃ</span></li>
    <li><b>Czteroliterowe - 19 pkt.</b>: <span class="slowa">BÓŚĆ, UŻĄĆ, UŻĘĆ, ŻÓŁĆ</span></li>
    <li><b>Pięcioliterowe - 24 pkt.</b>: <span class="slowa">BLUŹŃ, SPÓŹŃ</span></li>
    <li><b>Sześcioliterowe - 29 pkt.</b>: <span class="slowa">PÓŹŃŻE</span></li>
    <li><b>Sześcioliterowe - 27 pkt.</b>: <span class="slowa">GĘDŹBĄ</span></li>
    <li><b>Siedmioliterowe - 32 pkt.</b>: <span class="slowa">PÓŹŃMYŻ</span></li>
    <li><b>Siedmioliterowe - 30 pkt.</b>: <span class="slowa">BLUŹŃŻE, SPÓŹŃŻE</span></li>
    <li><b>Ośmioliterowe - 33 pkt.</b>: <span class="slowa">BLUŹŃMYŻ, SPÓŹŃMYŻ</span></li>
</ul>

<h2 id="otwarcie">Najlepsze otwarcie</h2>
<div class="rekord">132 punkty</div>
Taki wynik daje słowo <span class="slowa">PÓŹŃMYŻ</span>, ułożone na polach H2-H8 lub 8B-8H.<br />
To rozwiązanie zostało zgłoszone przez Sławomira Noska, a uznane przy opracowywaniu Oficjalnego Słownika Polskiego Scrabblisty na podstawie analogii do obocznej formy SPÓŹŃ.<br /><br />
Poprzednimi rozwiązaniami były słowa <span class="slowa">BLUŹŃŻE</span> (H8-H14 lub 8H-8N) zgłoszone 19 marca 1997 przez Macieja Czupryniaka i <span class="slowa">SPÓŹŃŻE</span> (H8-H14 lub 8H-8N) zgłoszone 22 października 1997 przez Sławomira Noska. Uwaga! Formę <span class="slowa">SPÓŹŃ</span> podaje jedynie Słownik Poprawnej Polszczyzny pod hasłem „czasownik” w rozdziale III (Tryby czasownika), punkcie 7d.<br /><br />
Poprzednim najlepszym rozwiązaniem tego problemu było słowo <span class="slowa">PÓŹNOŚĆ</span> (H2-H8 lub 8B-8H) za 118 punktów, znalezione w 1994 roku przez Jacka Szczapa. A jeszcze wcześniejszym - odkryte przez wielce zasłużonego dla popularyzacji SCRABBLE w Polsce Jacka Ciesielskiego - słowo <span class="slowa">ŻÓŁTOŚĆ</span> (H6-H12 lub 8F-8L). Ostatnie słowa wg starej punktacji.

<h2 id="siedem">Najlepsze zestawy liter</h2>
Zestawy, z których można ułożyć 11 siódemek:
<table width="100%" class="slowa">
    <tr>
        <td>E I P K A R S</td>
        <td>APRESKI, EPIRSKA, KIPERSA, PARSEKI, PASERKI, PISAREK, PRASKIE, SAPERKI, SIERPAK, SKARPIE, SPIKERA</td>
    </tr>
    <tr>
        <td>A E K N R T Y</td>
        <td>ANKERYT, KARNETY, KERATYN, KREATYN, KRETYNA, NAKRYTE, NEKTARY, RANKETY, TRAKENY, TRYKANE, TYRANEK</td>
    </tr>
    <tr>
        <td>E I K L N O W</td>
        <td>KLINOWE, KLOWNIE, KNELOWI, KOWELIN, LENIWKO, LENKOWI, LINEWKO, LINKOWE, NIKLOWE, NOWELKI, WELONIK</td>
    </tr>
</table>
<br /><br />
Zestawy, z których można ułożyć 10 siódemek:
<table width="100%" class="slowa">
    <tr>
        <td>A I K L N O W</td>
        <td>AKWILON, KLANOWI, KLINOWA, KNOWALI, KOWALNI, LINKOWA, NIKLOWA, WALONKI, WOKALNI, WOLNIAK</td>
    </tr>
    <tr>
        <td>A D E M N O R</td>
        <td>ARENDOM, ARDENOM, DAREMNO, DENAROM, MERDANO, MODERNA, NARODEM, NEMRODA, RADONEM, REDANOM</td>
    </tr>
    <tr>
        <td>A I K L M N O</td>
        <td>AKLINOM, ALKINOM, ALNIKOM, KALINOM, KLONAMI, KOLAMIN, LIMONKA, MALINKO, MANILKO, NAMOKLI</td>
    </tr>
    <tr>
        <td>A I K M O S T</td>
        <td>ASTIKOM, KOSMITA, MASTIKO, MOSKITA, OSTKAMI, SIATKOM, SITAKOM, SKOTAMI, STOKAMI, TOMISKA</td>
    </tr>
    <tr>
        <td>A I K O M R W</td>
        <td>KARMOWI, KARWIOM, KRAMOWI, KROWAMI, KWORAMI, MARKOWI, MAWORKI, MOKRAWI, ROWKAMI, WORKAMI</td>
    </tr>
</table>

<h2 id="szesc-b">Najlepszy zestaw sześciu liter z blankiem</h2>
<table width="100%" class="slowa">
    <tr>
        <td>A E I K L N *<br /><span>79 siódemek</span></td>
        <td>ALKANIE, ANIELKA, KALANIE, KANALIE, LENIAKA, ANIELKĄ, BLAKNIE, CELNIKA, KALENIC, KALINCE, NAKLEIĆ, DEKALIN, KANDELI, ALKENIE, ANIELEK, ANIELKĘ, LĘKANIE, FEKALNI, KEFALIN, HEINKLA, AKLINIE, ALKINIE, ANIELKI, KALINIE, KIELNIA, LENIAKI, KALINEK, KLIKANE, NAKLEIŁ, ALKINEM, KNELAMI, LENKAMI, MALINEK, MANELKI, MANILEK, LENNIKA, LNIANEK, NAKLNIE, ANIELKO, KOLANIE, KOLEINA, KOLENIA, OKLEINA, KLAPNIE, KLEPANI, LEPNIKA, NALEPKI, PALINEK, PANEKLI, PELIKAN, PILANEK, PINAKLE, ARLEKIN, SKALENI, SKLEINA, KLAŚNIE, LEŚNIKA, KLIENTA, LETNIAK, LETNIKA, TENAKLI, TINKALE, TKALNIE, KANIULE, KLAUNIE, KULANIE, KULENIA, LENIAKU,
 KELWINA, LENIWKA, LINEWKA, NALEWKI, ENKLIZA, ZAKLNIE, ANŻELIK, LEŻANKI, LEŻNIKA, ŻANKIEL, KANTELI</td>
    </tr>
</table>
Zestaw ten zgłosił 28 kwietnia 2004 roku Jacek Szczap.<br />
Poprzedni rekordowy zestaw <span class="slowa">A I K M O R *</span>, z którego można ułożyć 77 siódemek zgłosił 22 lutego 1998 Mateusz Żbikowski.

<h2 id="siedem-k">Najlepszy zestaw siedmiu liter do krzyżowania</h2>
<table width="100%" class="slowa">
    <tr>
        <td>N O R K A M I<span>70 wyrazów 8-literowych z 23 różnymi literami</span></td>
        <td>AKRANIOM AKRONIMU AKRONIMY ARIANKOM FIRANKOM GRONKAMI HARMONIK KARANIOM KARMINOM KARMIONA KARMIONE KARMIONO KARMIONY KARMIONĄ KARNIKOM KARPINOM KNOPRAMI KOMARNIC KONARAMI KONTRAMI KORAMINA KORAMINO KORAMINY KORAMINĄ KORAMINĘ KORANAMI KORONAMI KRANIKOM KRAŚNIOM KROSNAMI KRTANIOM MAKRONIE MANIERKO MANTIKOR MARCINKO MARKOTNI MARKOWNI MARLINKO MAROKINU MAROKINY MARYKINO MNOŻARKI NAKROIMY NIEMOKRA NITARKOM NORMALKI ORANKAMI ORKANAMI ORMIANEK ORMIANKA ORMIANKI ORMIANKO ORMIANKĄ ORMIANKĘ PARKINOM PARNIKOM RAMIONEK RAMIONKA RAMIONKO RAMIONKU REKONAMI ROCKMANI ROMANSIK RONDKAMI TARNIKOM TIRANKOM WARNIKOM WNORKAMI ZIARNKOM ŻARNIKOM</td>
    </tr>
</table>

<table width="100%" class="slowa">
    <tr>
        <td>W O L E N I A<span>70 wyrazów 8-literowych z 20 różnymi literami</span></td>
        <td>ALGINOWE ALKENOWI ALKINOWE BELOWANI CELOWANI DOLEWANI DOWALENI ELANDOWI ERLANOWI FILOWANE FLAWONIE GALENOWI ILANGOWE JOWIALNE KALINOWE KONWALIE LAWINOWE LAWSONIE LEGOWANI LEMANOWI LEPOWANI LEWKONIA LICOWANE LIKOWANE LIMANOWE LIZENOWA MALINOWE MANILOWE NALEWOWI NAOLIWŻE NEGOWALI NIEKLAWO NIEULOWA NIEWOLNA NOWELAMI OBLEWANI OBWALENI ODLEWANI ODLEWNIA ODWALENI OLEINOWA OLEWANIA OLEWANIE OLEWANIU OLŚNIEWA OPLWANIE OWLEKANI PANELOWI PLEWIONA POLEWANI POWALENI WALENIOM WANDELIO WAZELINO WCIELANO WCIELONA WELINOWA WELONAMI WELONIKA WERONALI WLECIANO WLEPIANO WLEPIONA WOKALNIE WOLANCIE WSOLENIA ZALEWNIO ZELOWANI ŻELOWANI ŻENOWALI</td>
    </tr>
</table>
Zestawy te zgłosił 8 maja 2010 roku Michał Makowski.<br />
Poprzedni rekordowy zestaw <span class="slowa">A I L K N O W *</span> z którego można ułożyć 65 wyrazów, zgłosił 28 kwietnia 2004 Jacek Szczap.<br />

Poprzedni rekordowy zestaw <span class="slowa">A E I K N O W *</span> z którego można ułożyć 57 wyrazów, zgłosił 22 października 1997 Andrzej Lożyński.

<h2 id="slowo27">Najdroższe słowo na planszy</h2>
<div class="rekord">1481 punktów</div>
Taki wynik dają słowa: <span class="slowa">PODŹWIGNĘŁYŚCIE, WYDŹWIGNĘŁYŚCIE</span> ułożone na jednym z czterech brzegów planszy.<br />
Kolejne słowa to <span class="slowa">UCIĄGNĘŁYBYŚCIE, UKOŃCZYŁYBYŚCIE (1373)</span>.<br /><br />
Jest to teoretycznie możliwe, jeśli między czerwonymi i niebieskimi polami leżą już pojedyncze litery, dwu- lub trzyliterówki, a my dokładamy ze stojaka siedem liter, zakrywając jednocześnie trzy pola potrójnej premii słownej i dwa pola podwójnej premii literowej.<br /><br />
Pierwsze trzy rozwiązania zgłosił Mariusz Skrobosz, a czwarte Małgorzata Kania - 28 stycznia 1998 roku.<br /><br />
Poprzednim najlepszym rozwiązaniem tego problemu było słowo <span class="slowa">ZAŻÓŁCIŁYBYŚCIE</span> za 1319 punktów, zgłoszone 16 listopada 1997 roku przez Jarosława Michalaka.

<h2 id="ruch">Najwyżej punktowany ruch</h2>
<div class="rekord">1750 punktów</div>
<a href="zapisy/maxruch.html" class="zapis"></a>Taki wynik zgłosili Dawid Pikul i Mateusz Żbikowski 25 kwietnia 2004 roku za ruch w którym powstały słowa: <span class="slowa">PODŹWIGNĘŁYŚCIE / PODERŻNĄŁBYM / ŹDZIARSKIEJ / IGREKÓW / NASTIKACH / ŚLUBOWAŃ / EFUZYJNYCH</span>.

<h2 id="samotnik">Najwyższy wynik w samotnej grze</h2>
<div class="rekord">3973 punkty</div>
<a href="zapisy/maxgra2.html" class="zapis"></a>Wynik 3973 punkty zgłosił Marek Reda 28 maja 2006 roku, jako autopoprawkę do własnego wyniku 3970 punktów z 27 maja 2006 roku (rozwiązania nie różnią się układem planszy).<br /><br />
<a href="zapisy/maxgra.html" class="zapis"></a>Poprzedni rekord, tj. 3883 punkty zgłosił Mateusz Żbikowski 23 kwietnia 2006 roku. Rozwiązanie to stanowiło poprawkę w postaci rozłożenia jednego ze skrabli w rozwiązaniu Dawida Pikula (25 kwietnia 2004 roku - 3880 punktów) na siedem ruchów po jednej płytce, co pozwoliło powiększyć liczbę punktów o 3.

<h2 id="niedasie">Słowa, których się nie da ułożyć</h2>
<div class="slowa">CHICHOCZĄCYCH, CHACHMĘCĄCYCH, CHICHOCĄCYCH</div>
Wszystkie słowa zawierają po pięć liter C i trzy litery H, natomiast w polskim zestawie liter są jedynie trzy litery C, dwie litery H i dwa blanki. Dwa pierwsze słowa zgłosił Wojciech Usakiewicz 2 maja 1998 roku. 24 października 1999 roku Grzegorz Wiączkowski dorzucił jeszcze słowo <span class="slowa">CHICHOCĄCYCH</span> - najkrótsze z „niemożliwych”, bo 12-literowe.

<h2 id="bloki">Najszybsze bloki</h2>
Istnieje możliwość szybkiego zablokowania partii bez użycia blanka - w 5 ruchach z użyciem 9 płytek i w 4 ruchach z użyciem 8 płytek:
<pre>

Ó S              B
S O I          B Ę C
  I D Ę          C E Ł
    U              Ł


</pre>
Pierwsze rozwiązanie zgłosił Wojciech Usakiewicz 2 maja 1998 roku, drugie zgłosił Jakub Szymczak 25 listopada 2008 roku.<br />
Natomiast najszybszy blok z użyciem blanka powszechnie znany był od dawna (blank zamiast jednej z liter I):
<pre>

I I I
I I I
I I I

</pre>

<h2 id="nazwywl">Nazwy niekoniecznie własne</h2>
W scrabble nie można układać wyrazów będących nazwami własnymi, ale wiele z nich jest także rzeczownikami pospolitymi, oznaczającymi np. tkaniny
(arizona, teksas), rośliny (eugenia, sudanka), zwierzęta (minorka, belg) itp. W wyniku odmiany wyrazów również można otrzymać nazwy własne (np. Paulina - dopełniacz rzeczownika PAULIN - zakonnik, Chiny - liczba mnoga od CHINA - drzewo). Poniżej znajdują się „nazwy własne”, które bezpiecznie można ułożyć w scrabble:

<h3>Nazwy geograficzne</h3>
<p class="slowa">AKRA AKROPOL ALASKA ALPY AMAZONKA AMUR ANGOLA ARDENY
ARIZONA ARKADIA ARKTYKA ATLANTA ATLAS ATTYKA BAKU BALI BASTYLIA BELMOPAN BENGAL BERLIN BERMUDY BOSTON BRISTOL BUG BYTOM BYTÓW CAYENNE CHILE CHINY CORDOBA CZAD DAKKA DAKOTA DAMASZEK DANIA DUKLA ELDORADO FILIPINY GENUA GHATY GIEWONT GŁOGÓW GRABÓW GROCHÓW GRODZISK GRUNWALD GWINEA HANOWER HAWAJE HAWANA HEL HELSINKI HIMALAJE HURON JANTAR JASŁO JAWA JELCZ JERSEY JORDAN KABATY KABUL KAJMANY KAMA KANADA KANDAHAR KANTON KARWIA KASZMIR KATAR KAZAŃ KENT KENTUCKY KINGSTON KOLA KOLONIA LABRADOR LEWANT LIBERIA LIMA LINCOLN LUBLIN LUCERNA ŁEBSKO ŁOSICE ŁÓDŹ MACAO MADERA MADISON MADRAS MALAGA MALI MAŁOPOLSKA MAMRY MANCZESTER MANILA MARIANY MARKIZY MARNA MARSYLIA MAZURY MEDYKA MEDYNA MEKKA MEKSYK MIELEC MIKOŁAJKI MINORKA MODENA NANKIN NOSAL NYSA ODRA OJCÓW OKSFORD OLDENBURG OLIMP OMAN OPOLE ORLEAN PAD PALATYNAT PAMPA PANAMA PANTEON PARNAS PERM PIŁA PISZ POLSKA PORTO POWIŚLE POZNAŃ PRETORIA PRZEMYŚL RABAT RADOM REDA REN REUNION RIWIERA RUDAWY RYGA RYSY SAJANY SALISBURY SALONIKI SAMOS SAN SANA SKOPIE SOMALI SOSNOWIEC SUCRE SUWA SYBIR SZANTUNG SZCZECIN SZCZYRK SZETLANDY ŚNIEŻKA TAG TARGOWICA TATRY TEKSAS TENERYFA TOGO TOLEDO TONGA TONKIN TRENT TRZEBNICA TYBET TYGRYS ULSTER URAL USTROŃ UTRECHT UZNAM WALENCJA WARNA WARSZAWA WARTA WDA WELLINGTON WERSAL WIELKOPOLSKA WIEŻYCA WIKTORIA WILIA WINCHESTER WIRGINIA WOLIN WOŁGA WORCESTER WRZEŚNIA YORK YORKSHIRE ZAGRZEB ZAIR ŻARNOWIEC ŻYWIEC</p>

<h3>Mieszkańcy krajów, krain itp.</h3>
<p class="slowa">ABISYNKA AMERYKANKA ANGIELKA ANGLIK ARAB ARABKA ARYJKA BALIJKA BANTAMKA BANTU BARBADOSKA BASKIJKA BAWARKA BEDUIN BELG BELGIJKA BUR BURGUND CELT CHIŃCZYK CYGAN CYGANKA CZERKIES CZERKIESKA DALMATYŃCZYK DOMINIKANKA ESKIMOSKA EUROPEJCZYK FILIPINKA FIN FINKA FLAMAND FRANCUZ GREK HEBRAJKA HELWETA HELWETKA HINDUS HINDUSKA HISZPANKA HOLENDER HOLENDERKA HUCUŁ INKA IZRAELITA IZRAELITKA JAPONKA JAWAJKA JORDANKA KAŁMUK KANDYJKA KARAIM KARAIMKA KASZUB KATARKA KOZACZKA KOZAK KREOL KUJAWIAK KURP LECHITKA LEZGINKA LIBERYJCZYK LIBERYJKA MALTAŃCZYK MAORYSKA MAZOWSZANIN MAZOWSZANKA MAZUR MAZURKA METYS MONGOŁ MURZYN MURZYNKA NORWEG ORMIANKA PERS PIAST PIGMEJ PIGMEJKA POLACZEK POLAK POLANIE POLKA PRUSAK RUSEK RUSKA SAMOJED SARDYNKA SAS SOWIET SUDANKA SZKOT SZWAB SZWABKA SZWAJCAR SZWED SZWEDKA TAMIL TATAR TATARKA WALONKA WANDAL WESTFALKA WĘGIERKA WŁOSZKA ŻYD ŻYDÓWKA</p>

<h3>Imiona</h3>
<p class="slowa">AFRODYTA ALA ALBERT ALFONS AMELIA ANDRZEJEK ANIELKA ANTEK APOLLO AREK AUGUST AUGUSTA BAŚKA BENIAMIN BERNARD BLANKA BRYGIDKA CARMEN CEZARY CZAREK DANIEL DANIELA DONALD DULCYNEA ELKA EUGENIA EWA FABIAN FILIP FRANK GILBERT HAMLET HERKULES HEROD HOMER HONORATKA IWAN JANOSIK JAREK JASIEK JAŚ JOLA JOLKA JONASZ JUDASZ KAJA KAJTEK KALINA KAROLEK KATARZYNKA KONSTANTY KORNEL LUDOLFINA LUDWIK LUDWIKA LUKRECJA MACIEK MAGDALENKA MAJKA MAKS MAŁGORZATKA MAŃKA MARCELINA MARCINEK MAREK MARIAN MARZANNA MELA MELCHIOR MESJASZ MICHAŁEK MIRANDA NAPOLEON OLEK OTELLO PAULINA REBEKA ROBINSON ROMEO SYLWESTER SYLWIN SZAWEŁ TARZAN TOMEK ULA WALENTYNKA WALERIAN WERONIKA WIKTORIA WIRGINIA WITEK ZEUS ZOŚKA</p>

<h3>Nazwiska scrabblistek i scrabblistów</h3>
<p class="slowa">BARAN BIAŁEK BIEGUN BOBOWSKI BOROWSKI BOŻEK BUKOWSKI CZERWONIEC DĄBROWSKI DĘBSKI FIGIEL FORTUNA FURMAN GASIK GÓRKA GRABOWSKI GRYZ JAWORSKI KACZOR KANIA KAPELA KLAR KOŚCIAŃSKA KOWALSKA KOZŁOWSKI KRAJ KRUK KRUPKA KUBIK KUCIA LUTER MAKOWSKI MAKUCH MORAWSKI MÓWKA NOSEK OCHOT ORZELSKI ORZEŁ OSTROWSKA OWSIANY PASZEK PIASECKI PIETRUSZKA PODLASKA POKOJSKI ROSÓŁ RUDNICKA RYŚ SAK SKWAREK SOJKA SOSZKA STĘPIEŃ SZCZAP SZOTA ŚLIWA TUSZYŃSKI WARZECHA WILK WÓJCIK WRÓBLEWSKI ZABAWA ZAJĄC ZALESKI ŻUŁAWSKA</p>

<h3>Znane osobistości</h3>
<p class="slowa">MAREK BELKA, CEZARY PAZURA, POLA RAKSA, MIKOŁAJ REJ, KORNEL UJEJSKI, SYLWESTER BRAUN ps.&nbsp;„KRIS”, ALBERT EINSTEIN, AUGUST COMTE, FIDEL CASTRO, ARIEL SZARON, AL GORE, HENRY FORD, NAT KING COLE, KIRK DOUGLAS, ZE DONG, BAJOR BEDNARSKI BOCHEŃSKI BRZECHWA CHŁOPICKI CHMIELNICKI CHMIELOWSKI DALTON DASZYŃSKI DĄBROWSKI DŁUGOSZ DUDEK DYZMA FRANKLIN GABLE GOŁOTA GÓRSKI GROBELNY KADŁUBEK KOLUMB KONOPNICKA KOSOWSKI KOWALEWSKI KOŹMIŃSKI KRAWCZYK KUCHARSKI KULCZYK LINCOLN LIPNICKA LUBAŃSKI LYNCH ŁOMNICKI MANN MŁYNARCZYK MŁYNARSKI NEWTON PELE PIASECKI PITT PIWOWSKI POTOCKI RAMBO ROKITA RYDZYK SŁONIMSKI SŁOWACKI SMOLEŃ SOJKA SPENCER SZAMOTULSKI SZYMBORSKA WAŁĘSA WYSOCKI ŻMUDA ŻÓŁKIEWSKI</p>

<h2 id="marki">Marki</h2>
Zgodnie z zasadami pisowni nazwy firm i marek piszemy wielką literą (samochód marki Polonez, zegarek „Omega”). Jednak nazwy różnego rodzaju wytworów przemysłowych, np. samochodów, motocykli, sprzętu AGD, zegarków, lekarstw, papierosów, odzieży itp, używane jako nazwy pospolite konkretnych przedmiotów (a nie jako nazwy marek i firm) piszemy małą literą. Jednak nie wszystkie marki i przedmioty danych marek znajdują się w słownikach języka polskiego (eh ta niekonsekwencja...). Poniżej znajduje się lista tych, które akceptuje OSPS:

<h3>Samochody i motocykle</h3>
<p class="slowa">ASTRA AUDI AUSTIN BENTLEY BUGATTI BUICK CADILLAC CHRYSLER CITROEN DACIA DAEWOO DAIHATSU DAIMLER DATSUN FERRARI FIAT FORD HARLEY HILLMAN HONDA HUMBER HUMMER HYUNDAI IKARUS JAGUAR JAWA JEEP JELCZ KAMAZ KAWASAKI KIA LANCIA LANOS LIAZ LINCOLN LOTUS LUBLIN ŁADA MASERATI MATIZ MAZDA MERCEDES MITSUBISHI MORRIS MOSKWICZ NISSAN NYSA OLDSMOBILE OPEL PEUGEOT PLYMOUTH POLONEZ PONTIAC PORSCHE PUNTO RENAULT SAAB SAMARA SAN SCANIA SEAT SEICENTO SIMCA SKODA STAR STEYR SUBARU SUZUKI SYRENA TARPAN TATRA TICO TOYOTA TRABANT URAL URSUS WARSZAWA WARTBURG WILLYS WOŁGA YAMAHA ZAPOROŻEC ZASTAWA ZETOR ZIŁ ZIS ŻIGULI ŻUK</p>

<h3>Inne marki</h3>
<p class="slowa">ADIDAS AGFA AMICA ANTONOW APACHE ARDO BASF BELL BECHSTEIN BOEING BOSCH BRAUN CALISIA CAMEL CANON CARMEN CASIO CESSNA CHESTERFIELD CITIZEN COMMODORE COMPAQ CONCORDE DAKOTA DIORA DORNIER DOUGLAS DUNHILL DUNLOP ELEMIS EPSON FENDER FOKKER FUJI GOLDSTAR GRUNDIG GUINNESS HEINEKEN HEINKEL HERCULES HITACHI HURRICANE ILJUSZYN IŁ INDESIT INTEL JUNKERS KAWAI KODAK LEE LEICA LEWISY MACINTOSH MAGGI MAGNUM MAJORETTE MARLBORO MARSHALL MARTENS MASMIX MESSERSCHMITT MICHELIN MOTOROLA NIKE NIKON OMEGA OMO OPTIMUS PAMPERS PANASONIC PARKER PATRIOT PEKAO PENGUIN PENTIUM PEPSI PERSIL PHILIPS POLARIS POLAROID POLLENA REEBOK REMINGTON SIEMENS SINGER SMIRNOFF SONY SPITFIRE SPORT STEINWAY STUKAS SWATCH TCHIBO TUPOLEW WARS WATERMAN WHISKAS WINCHESTER WRANGLERY</p>

<h2 id="oczyo">O czy Ó</h2>
Tryb rozkazujący czasowników kończących się na -ozić, -odzić, -orzyć, -ożyć to idealny przykład niekonsekwencji polszczyzny. Oto mała pomoc dla skrablistów (czasowniki z przedrostkami, np. dowozić, ukorzyć; mają taki sam tryb jak czasowniki podstawowe - wozić, korzyć. Wyjątki są uwzględnione poniżej):

<h3>Tylko O</h3>
<p class="slowa">BRODZIĆ CHODZIĆ SMRODZIĆ SZKODZIĆ<br />GAWORZYĆ<br />SROŻYĆ BARŁOŻYĆ CHĘDOŻYĆ PODROŻYĆ</p>

<h3>Tylko Ó</h3>
<p class="slowa">GODZIĆ WODZIĆ PŁODZIĆ ODMŁODZIĆ<br />ŁOŻYĆ MNOŻYĆ PŁOŻYĆ TRWOŻYĆ<br />TWORZYĆ UMORZYĆ CZWORZYĆ POMORZYĆ<br />ZABÓŚĆ PRZEBÓŚĆ -bódź<br />
SOLIĆ DOZWOLIĆ POZWOLIĆ WYZWOLIĆ ZEZWOLIĆ ZADOWOLIĆ PRZYZWOLIĆ NIEWOLIĆ ZESPOLIĆ</p>

<h3>O i Ó</h3>
<p class="slowa">WOZIĆ GROZIĆ MROZIĆ PŁOZIĆ<br />LODZIĆ RODZIĆ GŁODZIĆ GRODZIĆ SŁODZIĆ UWODZIĆ CHŁODZIĆ ŁAGODZIĆ WYGODZIĆ WYMODZIĆ OSWOBODZIĆ<br />
KORZYĆ MORZYĆ<br />DROŻYĆ STOŻYĆ UBOŻYĆ ZABARŁOŻYĆ<br />BÓŚĆ UBÓŚĆ DOBÓŚĆ POBÓŚĆ -bodź,bódź <br />GOLIĆ SMOLIĆ MOZOLIĆ SZKOLIĆ WYDOLIĆ ZNIEWOLIĆ</p>

<h2 id="wstretne">Wstrętne zestawy siódemkowe</h2>
Ładnie wyglądające zestawy, z których nie ma siódemek, ale tworzą wiele ósemek z blankiem:

<table width="100%" class="slowa ramkadolna">
    <tr>
        <td>CAPOSIE</td>
        <td>ASPORCIE CAMPOSIE KOPSACIE OPASACIE OPASZCIE OSPIANCE PASIONCE POCIESZA POSAPCIE POSRACIE POSTACIE PSOCENIA SPOCENIA</td>
    </tr>
    <tr>
        <td>CYKARDA</td>
        <td>AKARYCYD DAKARSCY KADARYCI KARDACZY KARDIACY MADRYCKA</td>
    </tr>
    <tr>
        <td>DELASKI</td>
        <td>DILERSKA GIDELSKA ODALISEK SKALDZIE</td>
    </tr>
    <tr>
        <td>GOLONIA</td>
        <td>ANALOGIO ANGOLOWI ENOLOGIA GALIONOM GALONOWI GAZOLINO GOLARNIO INDOLOGA IRANOLOG LASONOGI LIROGONA LOGONAMI MAGNOLIO
            NOGALOWI OGOLENIA SINOLOGA</td>
    </tr>
    <tr>
        <td>INTERIA</td>
        <td>ARTZINIE ELITARNI IGNITERA INERTAMI INTRACIE IRENISTA NIETARCI NITERAMI TARNINIE TRAFIENI TRAPIENI TRAWIENI TRIENNIA WIERTNIA</td>
    </tr>
    <tr>
        <td>KAMONIE</td>
        <td>AKINEZOM AKONITEM AKSENIOM ANEMIKOM ANIELKOM ANKIETOM BEKANIOM BEKMANIO BEKONAMI CMOKANIE DIAKONEM
            EKONOMIA EKSONAMI FONEMIKA GEKONAMI IKEBANOM KAMIONCE KAMIONEK KANIONEM
            KAOLINEM KARMIONE KATIONEM KAZEINOM KAŻENIOM KENOZAMI KESONAMI KETONAMI
            KOMNACIE KOMPANIE KONAKIEM KONANIEM KOPANIEM LEKOMANI LENIAKOM LIKAONEM ŁOMIANEK
            MAKRONIE MANIERKO MASKONIE MELONIKA MIELONKA MIKOTANE MONIECKA NACIEKOM
            NAMOKNIE NEONKAMI NIAMEJKO NIEMOKRA NIEOMSKA ODMIANEK ORMIANEK OŻENKAMI
            RAMIONEK REKONAMI SIEMANKO TOKENAMI ZAMOKNIE</td>
    </tr>
    <tr>
        <td>KAMOSIE</td>
        <td>AKMEISTO AKSENIOM BOSAKIEM
            CIESAKOM DOSIEKAM EKSONAMI KAEMISTO KAIROSEM KAISEROM KESONAMI KOMESAMI
            KOMPASIE KOSERAMI KOSMEAMI ŁOKASIEM ŁOMASKIE MAOISTEK MASKOCIE MASKONIE
            NIEOMSKA OBSIEKAM OKRESAMI OKSERAMI OMAŃSKIE OMIESZKA OSEŁKAMI OSESKAMI
            OSIKAŁEM PASIEKOM PIESAKOM POSIEKAM SARKOMIE SIEKAŁOM SIEMANKO TASIEMKO
            TOKSEMIA ZASIEKOM</td>
    </tr>

    <tr>
        <td>KAPECKI</td>
        <td>KAPECZKI KUCIAPEK KUPIECKA PAKIECIK</td>
    </tr>
    <tr>
        <td>KAPERIE</td>
        <td>KARPIELE NIEPAREK PAKERNIE PANIEREK PAPIEREK PIEKARCE PIEKAREK PIEKARZE</td>
    </tr>
    <tr>
        <td>KAROMIE</td>
        <td>AMORKIEM ARIETKOM BAROKIEM
            CERAMIKO CMOKIERA DEKORAMI DOKERAMI JOKERAMI KAIROSEM KAISEROM KAMEROWI
            KARIEROM KARMIONE KAWIOREM KIERATOM KODERAMI KOKERAMI KORABIEM KOSERAMI
            KOZERAMI KREOLAMI LAKIEROM MACIOREK MAGIERKO MAKRONIE MANIERKO MIODAREK
            MIOTAREK NIEMOKRA OBERKAMI OGARKIEM OKARMCIE OKARMIEŃ OKRAKIEM OKRESAMI
            OKSERAMI ORMIANEK POKARMIE POKERAMI RAKIETOM RAMIONEK REKONAMI ROBAKIEM
            RODAKIEM SARKOMIE TAROKIEM WEIMARKO</td>
    </tr>
    <tr>
        <td>KASIAMI</td>
        <td>AFIKSAMI AKSISAMI ALASKIMI ASTIKAMI CISAKAMI DIASKAMI FIASKAMI GASIKAMI INKASAMI
            PASIKAMI PIASKAMI PISAKAMI PSIAKAMI SALMIAKI SIANKAMI SIARKAMI SIATKAMI SITAKAMI SIWAKAMI</td>
    </tr>
    <tr>
        <td>KASIOWE</td>
        <td>AELOWSKI AKCESOWI AKOWSKIE
            ALOWSKIE ANEKSOWI APEKSOWI AZOWSKIE BEKASOWI ESOWNIKA IKSOWATE INKASOWE
            KARESOWI KASETOWI KOWIESKA KWASOCIE OBSIEWKA ODSIEWKA OŁAWSKIE OPAWSKIE
            ORAWSKIE OWSIANEK PIASKOWE PISAKOWE SAKIEWKO SEKOWALI SEKOWANI SERAKOWI
            SIARKOWE SIATKOWE SIELAWKO SOWIECKA SOWIETKA SPIEKOWA STEAKOWI WIOSEŁKA WIOSENKA</td>
    </tr>
    <tr>
        <td>KAZIKOM</td>
        <td>IKRZAKOM KIZIAKOM KOCZKAMI KOSZKAMI KOZACKIM KOZAKAMI KOZAKIEM KOZIKAMI KOZŁKAMI</td>
    </tr>
    <tr>
        <td>KILONIA</td>
        <td>ALKINOWI BALONIKI GALONIKI IZOKLINA KALINOWI KAOLINIE KAOLINIT
            KAULINIO KINOLAMI KONWALII LIKAONIE LIKOWANI LIMIANKO LINIAKOM LINIARKO LIONKAMI
            NAKOSILI NAKROILI NIKOLAMI OLIWNIKA SALONIKI WOLNIAKI</td>
    </tr>
    <tr>
        <td>KOLIOWA</td>
        <td>DOKOWALI KLOAKOWI KOCOWALI
            KODOWALI KOKILOWA KOLANOWI KOLAŻOWI KOLOKWIA KOŁOWALI KONWALIO KOPALOWI
            KORALOWI KOROWALI KOTOWALI KOWALOWI LIKOWAŁO LIKOWANO LOKAJOWI LOKALOWI
            LOKATOWI LOKOWALI LOKOWANI NOWOLAKI OKOWIELA OLIWKOWA POLAKOWI ROKOWALI
            TOKOWALI WIELOOKA WOKALIZO WOKALOWI WOLAKOWI ZAKOLOWI</td>
    </tr>
    <tr>
        <td>KOWALIM</td>
        <td>ALKOWAMI ALMIKOWI ALOWSKIM
            KALAMOWI KALWILOM KALWINOM KILIMOWA KLAMPOWI KLAMROWI KLIWAŻOM KLOWNAMI KOWALAMI KWILAJOM LAKOWYMI
            LIGAWKOM LIMAKOWI LITWAKOM LIZAWKOM MLASKOWI OLIWKAMI SIKLAWOM SLAWIKOM WALIZKOM WIŚLAKOM WOALKAMI
            WOKALAMI WOKALIZM WOLAKAMI WOLAKIEM</td>
    </tr>
    <tr>
        <td>LISOWCA</td>
        <td>SCHOWALI SMALCOWI SOLWACIE WCIOSALI</td>
    </tr>
    <tr>
        <td>MADOSIE</td>
        <td>ADONISEM DIASTEMO DIASTEOM DOMIESZA DOSIAŁEM DOSIEKAM DOSIEWAM EIDOSAMI MESODAMI MIDASOWE MODESTIA
            ODSIAŁEM ODSIEWAM OSIADŁEM OSIEDLAM</td>
    </tr>
    <tr>
        <td>MARLOWI</td>
        <td>ALARMOWI KLAMROWI LAMEROWI MARALOWI
            MARGLOWI MARSLOWI MORALIÓW MOROWALI MURALOWI MUROWALI RAMOLOWI RYMOWALI WALORAMI WGRAMOLI WILDAMOR</td>
    </tr>
    <tr>
        <td>MINERWO</td>
        <td>MINEROWI MINOROWE MORENOWI MORWINEM MORWINIE NERWICOM NIEMRAWO NUMEROWI ONERWIEM ONERWIOM RAMOWNIE RZEWNIOM WORANIEM
            WRONIEMU WRZENIOM</td>
    </tr>
    <tr>
        <td>MOCZNIA</td>
        <td>CALIZNOM CENOZAMI CZADNIOM CZKANIOM CZŁONAMI CZNIAŁOM KOMICZNA MILCZANO MOCZANIE MOCZARNI MOCZENIA
            MOCZNICA MOCZNIKA MROCZNIA OMOCZNIA PANICZOM PONCZAMI ROZCINAM</td>
    </tr>
    <tr>
        <td>MORAZIE</td>
            <td>ATOMIZER DOMIARZE DOMIERZA EROZJAMI ILORAZEM IZOBAREM IZOMERAZ IZOMERIA IZOTERMA KOZERAMI MACIORZE
                MAZEROWI MIERZONA MIZEROTA MOHAIRZE MORZENIA OBMIARZE OBMIERZA ODMIERZA
                ODZIERAM OMARZNIE OMIERZŁA OPIERZAM ORGAZMIE POMIARZE POZERAMI REMIZOWA
                REZOLAMI REZONAMI ROZETAMI ROZŁAMIE ROZMAITE ROZMAZIE ZABIOREM ZACIEROM
                ZAMROZIE ZAROIŁEM ZETORAMI ZORANIEM</td>
    </tr>
    <tr>
        <td>NATOWEJ</td>
        <td>AJENTOWI ETANOWEJ FANTOWEJ JENOTOWA KANTOWEJ LNOWATEJ NAFTOWEJ STANOWEJ TRANOWEJ WANTOWEJ WMOTANEJ</td>
    </tr>
    <tr>
        <td>NERWISA</td>
        <td>ARSENAWI ARSENOWI INWERSJA
            NAREWSKI NARWIESZ SEROWNIA SIRWENTA SKWARNIE SPRAWNIE UNIWERSA WERNISAŻ WYSRANIE</td>
    </tr>
    <tr>
        <td>NIETRZA</td>
        <td>ARTZINEM ARTZINIE INTERFAZ INWERTAZ NIETURZA NIEZRYTA PRZETAIN TARZANIE TRANSZEI TRZAŚNIE TRZEPANI TRZEŚNIA WIETRZNA</td>
    </tr>
    <tr>
        <td>NIKOSCY</td>
        <td>KONIŃSCY KOSTNICY SCYNKOWI SKNOCIŁY SKNOCIMY SKOŚNICY</td>
    </tr>
    <tr>
        <td>NIKOZJA</td>
        <td>AKINEZJO OKAZYJNI POZNIKAJ ZAMOKNIJ ZNAJOMKI</td>
    </tr>
    <tr>
        <td>NORWALI</td>
        <td>DRWALNIO ERLANOWI KLAROWNI
            LORANOWI LWIARNIO NAROWILI NOROWALI ROLOWANI ROLOWNIA ROZWALIN RULOWANI
            RYWANOLI WALTORNI WERONALI</td>
    </tr>
    <tr>
        <td>PADERKI</td>
        <td>KIDNAPER SPARDEKI</td>
        </tr>
    <tr>
        <td>PALNISZ</td>
        <td>LAPNIESZ NALEPISZ NAPALISZ NAPYLISZ PALNIESZ SZPILMAN SZPINDLA</td>
    </tr>
    <tr>
        <td>PANOCEK</td>
        <td>KOMPANCE KOPCENIA NAKOPCEŃ NAKOPCIE OKAPNICE OKOPCANE PANCERKO PANKOWCE PANOCZEK PONIECKA</td>
    </tr>
    <tr>
        <td>PANODZE</td>
        <td>DOCZEPNA ODCZEPNA OPĘDZANE PANDORZE PODZIANE ZAPODANE ZDEPTANO</td>
    </tr>
    <tr>
        <td>PARYSIE</td>
        <td>KAPRYSIE PARYSKIE PASIERBY PRYMASIE SPIERAŁY SPIERAMY SPIERANY</td>
    </tr>
    <tr>
        <td>PASTNIE</td>
        <td>INSPEKTA NASTĘPIE NIEPSTRA NIEPSUTA NIEPTASI NIEPUSTA NIESPITA PASTEWNI PIENISTA
            POSTANIE SPARTEIN SPĘTANIE SPYTANIE STAPIANE STĄPANIE STĘPIANE SZEPTANI WAPNISTE</td>
        </tr>
    <tr>
        <td>PIKULOM</td>
        <td>DUPLIKOM KOMPILUJ KOPULAMI KUMPLOWI LITKUPOM PLUSIKOM POKULIMY POKUMALI PUBLIKOM PUPILKOM SUPLIKOM</td>
    </tr>
    <tr>
        <td>PODERKI</td>
        <td>KREPIDOM KROPIDEŁ KROPIDLE PERIODYK</td>
    </tr>
    <tr>
        <td>POLNICA</td>
        <td>CHLIPANO NAPOCILI PALNOŚCI PALTOCIN PILONACH PINOLACH PLECIONA PLWOCINA POCHLANI POLECANI POLONICA PRALNICO</td>
    </tr>
    <tr>
        <td>RACZYMI</td>
        <td>CARYZMIE CZARNYMI KRZYCAMI MACIERZY MARZYCIE MINCARZY MIZRACHY PRYCZAMI ROZMYCIA RYCZKAMI RZYCIAMI ZARYCIEM ZARYCIOM ZARYCKIM
            ZRYCIAMI</td>
    </tr>
    <tr>
        <td>RAJOWNI</td>
        <td>AJRANOWI JAROWANI RAJCOWNI</td>
    </tr>
    <tr>
        <td>REGINKA</td>
        <td>GALERNIK GARNKIEM KATERING NIGERSKA TANGERKI ZGINAREK</td>
    </tr>
    <tr>
        <td>ROZALKI</td>
        <td>KOKILARZ KRIOLIZA LICZARKO OKURZALI ORLICZKA ORZEKALI PROKLIZA ROZKWILA ZAKROILI ZAKROPLI ZIELARKO ZROŚLAKI</td>
    </tr>
    <tr>
        <td>RUSOWIE</td>
        <td>ASUROWIE DUSEROWI REBUSOWI REZUSOWI ROZSIEWU SIEWRUGO SUPEROWI SUROWICE SUROWIEC SUROWIEĆ SUROWIEJ SUROWIEŃ SUROWNIE UKRESOWI
            UREUSOWI WIRUSOWE</td>
    </tr>
    <tr>
        <td>RYDZIKA</td>
        <td>DZIKARZY IDOKRAZY</td>
    </tr>
    <tr>
        <td>SAMOCIE</td>
        <td>BOSMACIE CAMPOSIE CIESAKOM CIOSAŁEM COMESAMI MAOISTCE MASKOCIE MASORECI MASOWIEC NACIOSEM OBCESAMI OSMALCIE OSMAŻCIE SECAMOWI
            SZAMOCIE ZACIOSEM</td>
    </tr>
    <tr>
        <td>SEMANTA</td>
        <td>AMNESTIA DASTANEM MANSZETA MARTENSA NASTAŁEM SENATAMI SZATANEM</td>
    </tr>
    <tr>
        <td>SKROBIM</td>
        <td>DOBRSKIM KROBSKIM MIKROBUS OBORSKIM SKROBIOM SKUMBRIO</td>
    </tr>
    <tr>
        <td>SKRONIA</td>
        <td>ANDORSKI ANGORSKI BORNASKI
            GARSONKI ISKRZONA KARNISTO KONSPIRA KORNISZA KROSNAMI NAKROISZ NAROLSKI
            NASTROIK NIEORSKA NIESKORA OSKÓRNIA ROMANSIK SARDONIK SENIORKA SKARNOWI
            SKIATRON SKRAJNIO SKROBANI SNOWARKI SPORNIKA</td>
    </tr>
    <tr>
        <td>SLAMOWI</td>
        <td>ALOWSKIM ISLAMOWI LAMUSOWI
            LASOWYMI LIASOWYM MARSLOWI MASOWALI MELASOWI MELISOWA MLASKOWI MOLASOWI MUSOWALI PSALMOWI SALOWYMI
            SAMOWOLI SAMPLOWI SIELAWOM SIKLAWOM SLAMSOWI SLAWIKOM SMALCOWI STALIWOM SUMOWALI SZLAMOWI SZMALOWI WSOLIŁAM</td>
    </tr>
    <tr>
        <td>STAWNIK</td>
        <td>KOSTNAWI KOSTNIWA NASTAWKI NATOWSKI SAWANTKI STANIKÓW WTASKANI</td>
    </tr>
    <tr>
        <td>WEPRALI</td>
        <td>PRAWIDLE PRZEWALI WPIERALI</td>
    </tr>
    <tr>
        <td>WILSONA</td>
        <td>ANOLISÓW GLANSOWI
            LANOSOWI LASOWANI LAWSONIA LAWSONIĄ LAWSONIE LAWSONIĘ LAWSONII LAWSONIJ
            LAWSONIO LIANOSÓW LISTOWNA LOSOWANI NOSALOWI OSNUWALI SALONOWI SALWINIO
            SANOWALI SILANOWI SINGLOWA SLANGOWI SNOWALNI STALOWNI SWAWOLNI SZWALNIO
            WSOLENIA WYSILANO WYSILONA</td>
    </tr>
    <tr>
        <td>WORZENI</td>
        <td>CZERWONI DRZEWINO KRZEWINO
            NEWROZIE PRZEWINO REZONOWI REZUNOWI ROZLENIW ROZLEWNI ROZWIANE ROZWINIE
            RZEWNIOM SWORZNIE TWORZENI WIERZONO WIETRZNO WIZJONER WRODZENI WRZECION
            WRZENIOM WRZEŚNIO WZROŚNIE ZEROWNIK ZIARNOWE ZWIERANO ZWROTNIE</td>
    </tr>
    <tr>
        <td>ZAKIPIE</td>
        <td>CZEPIAKI KAPIZMIE PAZIKIEM PIKADZIE ZAKIPCIE ZAKIPIEĆ ZAKIPIEŃ ZAKPICIE ZAPIECKI ZAPIEKLI</td>
    </tr>
    <tr>
        <td>ZAPITOL</td>
        <td>IZOPLETA ZATOPILI</td>
    </tr>
    <tr>
        <td>ZEWKAMI</td>
        <td>KRZEWAMI MAZIÓWEK SZEWKAMI WOZAKIEM WZMIANEK ZAIMKOWE ZAKWITEM ZLEWKAMI</td>
    </tr>
</table>

<h2 id="tryplety">Trzy takie same litery</h2>
Poniżej znajduje się lista słów, jakie można ułożyć mając do dyspozycji trzy takie same litery. Jak widać, dla jednej litery może to być już trzyliterowe słowo, z inną z kolei nie istnieje takie słowo (oczywiście nie dłuższe niż 15-literowe). Nie zawsze nadmiar tych samych liter musi skutkować ich wymianą; a często dzieje się tak, gdyż podświadomie sądzimy, że pewnie nic z takich liter nie ma. Warto więc chwilkę pomyśleć, mając taki urodzaj na stojaku, nim wykonamy ruch.<br />
W nawiasie podano długość najkrótszego słowa z wielokrotnością litery.<br /><br />

<p class="slowa litera">A (5): ABAKA ABATA ACANA AGADA AGAMA AGAPA AGAWA ARABA ARATA ASANA KAKAA SAABA</p>
<p class="slowa litera">Ą (6): DĄŻĄCĄ MĄCĄCĄ</p>
<p class="slowa litera">B (5): BOBBY</p>
<p class="slowa litera">C (5): CACCI CHCĄC CHCIC CUCĄC</p>
<p class="slowa litera">Ć (0): brak></p>
<p class="slowa litera">D (6): ADDEND</p>
<p class="slowa litera">E (5): ESEJE HEWEE</p>
<p class="slowa litera">Ę (0): brak</p>
<p class="slowa litera">F (10): FOTOOFFSET</p>
<p class="slowa litera">G (7): JOGGING</p>
<p class="slowa litera">H (8): CHICHACH CHUCHACH</p>
<p class="slowa litera">I (3): III</p>
<p class="slowa litera">J (6): OJEJEJ OJOJOJ</p>
<p class="slowa litera">K (6): KOKSIK KUKLIK</p>
<p class="slowa litera">L (5): ALLEL ALLIL</p>
<p class="slowa litera">Ł (11): BŁOGOSŁAWIŁ</p>
<p class="slowa litera">M (5): MAMMY MAMOM MIMEM MIMOM</p>
<p class="slowa litera">N (6): HENNIN NANKIN NOWENN RENNIN YUNNAN</p>
<p class="slowa litera">Ń (0): brak</p>
<p class="slowa litera">O (5): OBORO OKOŁO OKOWO OPOKO OPONO OPORO OSOBO OSOKO</p>
<p class="slowa litera">Ó (9): PÓŁCHÓRÓW PÓŁWÓZKÓW</p>
<p class="slowa litera">P (7): POPAPLA POPAPRZ POPPERS</p>
<p class="slowa litera">R (6): HORROR KARRAR RURARZ TERROR</p>
<p class="slowa litera">S (6): CASSIS CISSUS MISSIS PASSUS SARISS SASEKS SEPSIS SSIESZ STRESS</p>
<p class="slowa litera">Ś (10): ŚCIEŚNIŁAŚ ŚCIEŚNIŁEŚ ŚCIEŚNIŁOŚ ŚCIŚLIWOŚĆ UŚCIŚLAŁEŚ UŚCIŚLAŁAŚ UŚCIŚLAŁOŚ UŚCIŚLIŁAŚ UŚCIŚLIŁEŚ UŚCIŚLIŁOŚ</p>
<p class="slowa litera">T (5): TUTTI</p>
<p class="slowa litera">U (5): UŁUSU USUSU USUWU UZUSU</p>
<p class="slowa litera">W (6): WIEWÓW WLEWÓW WWIEWA WWOZÓW WYWIEW</p>
<p class="slowa litera">Y (5): GYYZY</p>
<p class="slowa litera">Z (5): ZRZEZ</p>
<p class="slowa litera">Ź (0): brak</p>
<p class="slowa litera">Ż (7): ZAŻEŻŻE</p>


<h2 id="multiplety">Cztery i więcej takich samych liter</h2>
Trzy takie same litery to już problem, a więcej? To dopiero ból głowy! Poniżej lista słów, które mogą się przydać w takiej sytuacji. Część z nich może przydarzyć się dość często (np. 4A), część prawie nigdy (np. 5C). Tak czy siak, takie słowa to prawdziwe rarytasy.<br />
W nawiasie podano długość najkrótszego słowa z wielokrotnością litery.<br /><br />

<p class="slowa litera">A (7): AMALAKA ANAFAZA ANANASA APADANA APARATA ATAMANA AWATARA KATAMARANA NAZADAWAŁA NAZADAWANA ZAAMBARASOWANA ZAAMBARASOWAŁA</p>
<p class="slowa litera">C (8): CACCIACH CHCĄCYCH CHCICACH CUCĄCYCH CHICHOCĄCYCH</p>
<p class="slowa litera">E (7): PEERELE EWENEMENCIE EWENEMENTEM NIECEREGIELENIE</p>
<p class="slowa litera">I (7): INIURII WIGILII BIBLIOFILII INFINITIWIE PIĘCIOLINII NIEFILIPIŃSKIMI NIEIMIELIŃSKIMI NIEKISIELICKIMI</p>
<p class="slowa litera">J (11): JAJOBÓJCZEJ</p>
<p class="slowa litera">K (13): KICKBOKSERSKA KICKBOKSERSKĄ KICKBOKSERSKI KICKBOKSERSKU KILKOKARTKOWA KILKOKARTKOWĄ KILKOKARTKOWE KILKOKARTKOWI KILKOKARTKOWY KILKUKARTKOWA KILKUKARTKOWĄ KILKUKARTKOWE KILKUKARTKOWI KILKUKARTKOWY</p>
<p class="slowa litera">L (13): ALKALICELULOZ</p>
<p class="slowa litera">M (8): HAMMAMEM HAMMAMOM MAMMOGRAMEM MAMMOGRAMOM MAMMOTOMIOM</p>
<p class="slowa litera">N (9): ŻNINIANIN NIENONSENSOWNA NIENONSENSOWNĄ NIENONSENSOWNE NIENONSENSOWNI NIENONSENSOWNY</p>
<p class="slowa litera">O (7): OGOLONO OKOCONO OKOLONO OOLOGIO OOSPORO OSOLONO ZOONOZO OGOŁOCONO OKOŁOPORODOWA OKOŁOPORODOWĄ OKOŁOPORODOWE OKOŁOPORODOWI OKOŁOPORODOWY OSOBOWOŚCIOWO PROTOZOOLOGIO PROTOZOOLOGOM ZOOSOCJOLOGIO ZOOSOCJOLOGOM OKOŁOPORODOWEGO</p>
<p class="slowa litera">R (11): HURRAREFORM</p>
<p class="slowa litera">S (7): SUSSEKS BESSERWISSERSCY BESSERWISSERSKA BESSERWISSERSKĄ BESSERWISSERSKI BESSERWISSERSKU BESSERWISSERSTW</p>
<p class="slowa litera">T (10): STRATOSTAT</p>
<p class="slowa litera">U (8): CUMULUSU TUMULUSU</p>
<p class="slowa litera">W (8): WYWIEWÓW</p>
<p class="slowa litera">Y (8): WYBYŁYBY WYMYŁYBY WYRYŁYBY WYŻYŁYBY WYBYCZYŁYBY WYBYŁYBYŚMY WYMYŁYBYŚMY WYRYŁYBYŚMY WYTYCZYŁYBY WYŻYŁYBYŚMY WYBYCZYŁYBYŚMY WYTYCZYŁYBYŚMY</p>
<p class="slowa litera">Z (9): ZARZĘZISZ ZASZCZURZ ZASZCZYSZ ZESZCZYSZ ZISZCZASZ ZRZĄDZASZ ZRZĄDZISZ ZRZESZASZ ZRZESZYSZ ZRZĘDZISZ ZASZCZURZYSZ ZBEZCZESZCZA ZBEZCZESZCZĄ ZBEZCZESZCZĘ ZBEZCZESZCZASZ</p>

<?require_once "files/php/bottom.php";?>
</body>
</html>
