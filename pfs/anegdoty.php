<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Różności : Anegdoty scrabblowe</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","anegdoty");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Anegdoty skrablowe")</script></h1>

Życie skrablowe obfituje w wiele niecodziennych, nierzadko zabawnych sytuacji. Oto niektóre z nich.<br />
Znasz anegdotę? <a onClick="WyslijMail('pfs', 'pfs.org.pl')">Napisz!</a>

<h2>Rachunek prawdopodobieństwa</h2>
Podczas Maratonu w Katowicach (2005), partia Ireny Sołdan i Jakuba Nalepy. Jakub po 2 ruchach dostał skład <span class="slowa">ADKOZU*</span>. Było miejsce tylko na jednego skrabla — <span class="slowa">ODKURZA</span>. Jakub położył swojego skrabla, powiedział za jaką literę wykorzystuje blanka, i przeczytal słowo „odkurza”. Grająca obok Halina Roszak poczerwieniała, popatrzyła na swój stojak i położyła skrabla... <span class="slowa">ODKURZA</span> z blankiem za R! :)<br />
Jak widać gra w scrabble jest nieprzewidywalna — jaka jest szansa, ze dwoje graczy dostanie taki sam skład, położy tego samego skrabla i... gra na sąsiednich stołach?! :)

<h2>Opadnięty</h2>
- Jest „opadnięty”? — pyta Łukasz.<br />
- Łukasz, to jest twój problem — odparł ze śmiechem Irek.<br />
- Nie wiemy, nie patrzymy w tę stronę — wtórował Irkowi Kuba.<br />
- Monika, sprawdź — ignorując kolegów poprosił Łukasz Monikę.<br />
- Ja nie sprawdzę, ja mam męża — zaśmiała się Monika.<br />
- Jest! — zawołał Łukasz po sprawdzeniu<br />
- Wszedł, mimo że opadnięty<br />


<h2>Jak w pokerze</h2>
Podczas gry Kazik nagle zrezygnowanym głosem szepcze pod nosem: <br />
- Jak w pokerze, jak w pokerze..... <br />
Zainteresowany gracz ze stolika obok pyta:<br />
- Dlaczego jak w pokerze? <br />
Kazik pokazuje swój stojak, na którym ma zdublowane litery i mówi: <br />
- Ciągle tylko mam po dwie pary albo full... 

<h2>Małżeńskie rozterki</h2>
Monika i Asia, jadąc na swój debiutancki turniej, postanowiły powtórzyć sobie trzyliterówki: <br />
- Tau, tąp, tąż.....??...TĄŻ?!? Co to jest tąż?! — pyta Monika <br />
- No wiesz, to takie wzmocnienie, tą — tąż — wyjaśnia Asia <br />
- Aha, rozumiem — po czym zasugerowana tym tokiem myślenia Monika pyta: — a co to jest MĄŻ?

<h2>Bajka o napięciu</h2>
Zdzisław Szota XIII Mistrzostw Polski 2005 nie zaliczy do udanych. Gdy ktoś pytał jak mu poszło, odpowiadał również pytaniem, czy chce usłyszeć bajkę o napięciu — „Skończyłem na pięciu”.<br />
Po jednej z partii miał równie ciekawą odpowiedź na pytanie o wynik:<br />
- Jak Zdzichu, co zrobiłeś?<br />
- Trzy straty w pierwszych trzech ruchach...

<h2>Słowne nieporozumienia</h2>
Na wczasach w Szarlocie, w blitzu (partie 7-minutowe) Alicja Bielecka grała z Filipem Warzechą. Po partii pochwaliła się Grażynie Wesołowskiej:<br />
- Zobacz, położyłam siódemkę za 140 pkt. debilowi!<br />
Grażyna popatrzyła na nią zmieszana, nie wiedząc skąd ten nagły brak szacunku dla przeciwnika.<br />
- A jaką siódemkę? — zapytała.<br />
- No, <span class="slowa">DEBILOWI</span> — odpowiedziała Alicja.

<h2>Cycusie</h2>
Mecz międzyklubowy Ostróda-Trójmiasto (maj 2005). Irena Sołdan (sirenka) gra z Krzysztofem Langiewiczem:<br />
„Leży sobie na planszy <span class="slowa">CYCU</span> a ja mam na stojaku <span class="slowa">ŚLĄCESZ</span>. Bardzo zadowolona kładę <span class="slowa">CYCUŚ / ŚLĄCE</span> przy czym to drugie jest na linii x3. Przeciwnik sprawdza oba słowa i ku mojemu zdziwieniu mordka jest czerwona. Z wrodzoną sobie inteligencją :) , po szybkim przemyśleniu kładę w następnym ruchu do CYCUSIA — ŚLESZ, bo uznałam że nie ma ŚLĄCE :) Krzysiek litościwie sprawdza tym razem jedno słowo <span class="slowa">CYCUŚ</span> i okazuje się, że nie ma. Na szczęście dla mnie, bo pewnie w następnym ruchu wymyśliłabym ślę, byłam pewna na 100%, że nieszczęsny cycuś musi być :))) Po powrocie do domu wysłałam wiadomość na gg do Maćka Czupryniaka, że spadły mi dzisiaj cycusie i niech coś z tym zrobi. Odpisał mi: <i>A co ja, chirurg plastyczny!?!</i>”<br /><br />
A Krzysiek rozpowiada teraz, że sprawdzał sirence cycusie 2 razy :-)

<h2>„Wykrakał..” inaczej</h2>
Podczas jednej z partii Tomek Żbikowski miał ładny zestaw <span class="slowa">KRAKALI</span>, jednak nie mógł go nigdzie położyć. Na linii x3 leżało wolne A, ale przez to też nic nie wymyślił. Postanowił więc zrezygnować z siódemki i położyć przez to A samo <span class="slowa">KRAKALI</span>. Niestety efekt kombinowania był taki, iż na planszy zamiast <span class="slowa">KRAKALI</span> położył <span class="slowa">KARAKALI</span>. Zaskoczony przeciwnik chciał sprawdzić nieznany wyraz, ale 
Tomek zobaczywszy swój błąd zdjął płytki z planszy, mówiąc, że nie ma po co sprawdzać, bo to pomyłka — miało być <span class="slowa">KRAKALI</span>. Po partii postanowił sprawdzić w anagramatorze, czy miał z tego zestawu jakieś siódemki. Mocno się zdziwił, gdy OSPS pokazał... <span class="slowa">KARAKALI</span>.<br />
<i>Karakal — ryś stepowy zamieszkujący pustynno-stepowe obszary Afryki i Azji</i>

<h2>Words don't come easy...</h2>
Podczas niedawnych świąt (Wielkanoc 2005) Krzysztof Mówka namówił na jedna partyjkę mamę i siostrę, które dotąd nie miały okazji 
zagrać w scrabble. W połowie partii w radiu została puszczona piosenka „Words don't come easy” FR Davida. Na to siostra Krzyśka, Joanna, cicho pod nosem — „Oj rzeczywiście, don't come easy...”

<h2>Arbiter</h2>
Podczas gry dwie scrabblistki przekomarzają się:<br />
- Co tak długo myślisz? Pewnie znowu myślisz jaką by tu niecną siódemkę zagrać — żartuje pierwsza.<br />
- Ależ skąd, ja jestem grzeczna — odpowiada druga. <br />
- Chyba grzeszna — dokucza pierwsza. <br />
Na to do rozmowy włącza się kibicujący kolega:<br />
- Ona jest taka GRZEZNA z blankiem...

<h2>Pocieszyciel</h2>
Na jednym ze spotkań Szczecińskiego Klubu Scrabble Kazik, będący w wieku poborowym, siedział z markotną miną, bo dowiedział się, że ma poprawkę z egzaminu. Ubolewając nad tym, że przedmiot jest trudny i będzie musiał się sporo natrudzić, żeby go zdać — usłyszał pocieszający głos kolegi:<br />
- Co się martwisz, zawsze możesz zostać scrabblowym Mistrzem Wojska Polskiego...

<h2>Skąd się biorą ksywki</h2>
Podczas wczasów skrablowych w Szarlocie (czerwiec 2002) Staszek Rydzik przedłużył do czerwonego pola ułożone przez przeciwnika słowo <span class="slowa">PAPKA</span> tworząc wyraz <span class="slowa">KOPAPKA</span>. Bardzo dumny z siebie, głośno oznajmił:<br />
- Chyba jestem geniuszem!<br />
Na to ktoś z boku przygadał:<br />
- Taaak, Eugeniuszem.<br />
Przeciwnik nie omieszkał sprawdzić słowa <span class="slowa">KOPAPKA</span>, którego, rzecz jasna, w słowniku nie było i w tym momencie wyjaśniło się, że Staszek, źle spojrzawszy, dopatrzył się na planszy słowa <span class="slowa">PARKA</span>, które oczywiście dawałoby się przedłużyć do <span class="slowa">KOPARKA</span>.<br />
Niemniej do końca wczasów Staszek pozostał już Eugeniuszem Kopapką.

<h2>Wirtualna nierzeczywistość</h2>
Jeden z najbardziej utalentowanych nastolatków w polskim Scrabble, Sergiusz Puchalski, często grywa na Croniksie. Któregoś razu po zakończonej partii chciał podziękować za grę, pisząc zwyczajowe „dzięki”. Niestety, palce szybsze od głowy (przypadłość chyba właściwa jego pokoleniu) wygenerowały słowo „dziwki”.<br />
- ??? — pojawiło się od zdumionego przeciwnika.<br />
Przeproś go, napisz, że „dzięki” miało być. — poradził siedzący obok Sergiusza kuzyn.<br />
Trrrrach, palce znów pomknęły po klawiaturze, enter. Niestety, zamiast „dzięki miało być” na ekranie ukazało się „dziwki malo buc”.<br />
- Sam jesteś buc! — odpisał urażony rywal i wyszedł ze stołu.<br />

<h2>Znikąd pomocy</h2>
Jedna ze skrablistek, poszukująca właśnie zatrudnienia, grała w domu partię z programem Networdz, który nie dysponuje pełnym, dopuszczalnym słownikiem.<br />
Po ułożeniu przez nią słowa PRACY, program wyrzucił na ekran komunikat:<br />
„Brak PRACY w słowniku!”

<h2>Koneksje skrablistów</h2>
W klubie warszawskim grupa graczy rozmawiała, jak to skrabliści, o różnych ciekawych słowach. Jedni opowiadali, jakie to dziwne nazwy roślin można znaleźć w słownikach, drudzy o tkaninach, trzeci o minerałach, a jeszcze inni zaczęli o rybach.<br />
W tym momencie Krzysztof Bukowski, oderwawszy się od gry, entuzjastycznie obwieścił:<br />
- A wiecie? Ja fajną rybę ostatnio poznałem!<br />

<h2>Trójbój</h2>
Jeden z czołowych skrablistów, niedawny lider rankingu — Leszek Nowak, podczas 1/16 finałów mistrzostw Polski w 2000 roku miał „swój dzień”. Najpierw, wespół z rywalem, tak zawzięcie sprawdzał przed grą płytki z zestawu, że... partię rozegrano 98 literami. Bez obu blanków, które odnalazły się później za zegarem. W drugiej partii wykonał akcję, której nie przewidzieli twórcy instrukcji i regulaminów do gry — położył pierwsze słowo SZCZUR tak, że nie zakrywało środkowego pola planszy. Osłupiały przeciwnik, Darek Puton, nie wiedząc, co z tym fantem zrobić, odwołał się do sędziego, który orzekł stratę ruchu. Po wyłożeniu przez Darka pierwszego słowa, Leszek pomieszał płytki i... wyłożył je wszystkie w słowie RUSZCZAŁ. Rywal tylko bąknął:<br />
- Po co mi to było?<br />
W przerwie Leszek usłyszał, jak ktoś opowiadał, że położył kiedyś słowo <span class="slowa">SFENOWY</span> (nienotowany w słownikach przymiotnik od minerału sfenu) i miał stratę, a przecież mógł ułożyć piękną siódemkę <span class="slowa">OFENSYW</span>. Po przerwie, w trzeciej partii grupowej, Leszek dostał na stojak układ <span class="slowa">FASOWNE</span>. Widział to słowo, ale mając w pamięci strzępy opowieści z przerwy, przeanagramował je i w droższym miejscu ułożył... <span class="slowa">SFENOWA</span>.<br />
Niestety, przegrał partię i nie awansował dalej, a szkoda, bo ta opowieść mogłaby być dłuższa i zabawniejsza.

<h2>Gdyby Pytia pisała słowniki...</h2>
Jedna z czołowych skrablistek, Gosia Kania, z racji swego stażu szaradziarskiego, zna znaczenie różnych słów-dziwolągów pojawiających się na planszy. Jednak zagadnięta o to, zwykła udzielać wyjaśnień w sposób wielce enigmatyczny.<br />
Zapytana niegdyś: „Człowiek mi przedłużył <span class="slowa">JAM</span> przez <span class="slowa">JAMS</span>, nie wiesz, co to jest?”, odparła bez namysłu:<br />
- Jasne, że wiem — „jams” to to samo, co „pochrzyn”.<br />
Kiedyś ktoś inny poskarżył się:<br />
- Wiem, że jest „fit”, np. brydżowy, ale „fita” rodzaju żeńskiego? Przeciwnik mi ułożył <span class="slowa">FITĘ</span> i była...<br />
- „Fita”? — zdziwiła się Gosia — Pewnie, że jest, to to samo, co „klupa”!<br />
Po dłuższej przerwie, w kwietniu 2000, Gosia zagrała w turnieju w Grójcu. Wyniki osiągnęła, hm, takie sobie, ale dawna forma słownikowa pozostała.<br />

Omawiano w przerwie jakąś partię:<br />
- ...i potem zagrał <span class="slowa">KENAF</span> na potrójności... — relacjonował ktoś.<br />
- A co to jest „kenaf”? — padło pytanie.<br />
- „Kenaf”? Nie wiecie? — wtrąciła niezawodna Gosia — To to samo, co „ketmia”... albo „gambo” — dodała po chwili.

<h2>Krzyś-Chytrusek</h2>
Podczas turnieju w Łodzi (listopad '99) Krzysztof Bukowski zasiadł do partii z Pawłem Stefaniakiem. Partię rozpoczynał Paweł, który wyciągnął już litery z woreczka i zastanawiał się nad ruchem. W tym czasie litery wylosował też Krzysztof, popatrzył na nie, ułożył na stojaku <span class="slowa">BAŚNIOW</span> i pomyślał:<br />
„Dobra nasza! Niemal dowolna samogłoska wyłożona przez przeciwnika, poza Ę, Ó i U, daje mi siódemkę.”<br />
Niestety, Paweł zdecydował się na wymianę, więc Krzysztof postanowił zastosować zagranie psychologiczne. Położył na planszy słowo BOŚ, licząc na to, że przeciwnik je zakwestionuje, zdejmie i zagra coś sam, wystawiając upragnioną samogłoskę.<br />
Ku jego przerażeniu jednak, Paweł BOŚ nie zakwestionował, lecz od leżącego na środkowym polu O położył skrabla ORĘDUJŻE za skromne... 122 punkty.

<h2>Nareszcie po polsku!</h2>
Wojtek Usakiewicz przed swym debiutanckim występem na mistrzostwach świata w wersji angielskojęzycznej (listopad '99) przyniósł do klubu zestaw do gry po angielsku, pragnąc poćwiczyć jeszcze nieco przed wyjazdem. Do jednej z partii zasiadł z Pawłem Stefaniakiem. Po kilku ruchach Paweł z radością wykrzyknął: „O! Nareszcie coś po polsku!”<br />
Jego spontaniczna reakcja dotyczyła słowa... GHI.<br />
<i>GHI — słowo pochodzące z języka hindi, oznaczające masło z mleka bawolic, dopuszczalne zarówno w polskim jak i angielskim Scrabble.</i>

<h2>Coś dla obdżektorów</h2>
Staszek Rydzik podczas klubowej partii z Pawłem Stanoszem położył na planszy siódemkę <span class="slowa">OPINACZ</span>. Paweł, rzecz jasna, zakwestionował ją, co z kolei zdziwiło Staszka:<br />
- „Opinacz” sprawdzasz? Przecież to oczywiste. Termin wojskowy. Co Ty, w wojsku nie byłeś?<br />
Paweł nic nie odpowiedział, jednak po sprawdzeniu, że słowniki nie notują jednak słowa „opinacz”, skomentował:<br />
- Widzisz? I po co Ci to było? Straciłeś dwa lata i ruch.<br />

<h2>Kisiel ujmuje istotę Scrabble</h2>
W drugiej rundzie turnieju w Inowrocławiu (kwiecień '98) spotkali się Maciek Czupryniak i ośmioletni Krzyś Usakiewicz, znany jako „Kisiel”. W pewnym momencie Maciek zagrał słowo <span class="slowa">ZYMAZĄ</span> za 44 pkt. Kisiela zamurowało, co Maciek zauważył i zapytał:<br />
- Co, pewnie chcesz sprawdzić „zymazę” w słowniku?<br />
- Nieee — odparł po chwili Kisiel — to brzmi tak idiotycznie, że na pewno jest.

<h2>Umysł skrablisty</h2>
W 11. rundzie III Mistrzostw Warszawy (luty '98) przy pierwszym stole spotkali się Tomasz Zwoliński i Dariusz Banaszek. Partię rozegrali szybko i wyszli na korytarz, zwyczajowo pozostawiając niezłożoną planszę, na górze której wyróżniało się słowo <span class="slowa">ZASTANÓW</span>.<br />
Stopniowo ci, którzy kończyli swoje partie, podchodzili do pierwszego stołu, by zobaczyć, jaki padł wynik, może podpatrzeć jakieś ciekawe słowa. Jako pierwszy popisał się skrablista nieprzeciętny, zdobywca piątego miejsca w tym turnieju — Robert Duczemiński. Rzucił okiem na planszę, a zauważywszy <span class="slowa">ZASTANÓW</span>, z niepokojem zapytał:<br />
- A co to jest „zastan”? W którym słowniku?<br />
Robert był pierwszy, ale nie jedyny. Lekko licząc, połowa krajowej czołówki wpadła w pułapkę schematycznego myślenia: „Skoro na planszy leży ZASTANÓW, to znaczy, że musi istnieć jakiś „zastan”. A dlaczego ja go nie znam?”.<br />
Wyjaśnienie, że to tryb rozkazujący od „zastanowić (się)” tylko częściowo uspokoił tuzy polskiego Scrabble.

<h2>To se ne wrati</h2>
Paweł Stanosz to niewątpliwie jeden z najlepszych polskich skrablistów. Prywatnie zaś zdeklarowany, by nie rzec wojujący, ateista, narzeka, że wiele razy przeciwnicy (zapewne jacyś fundamentaliści religijni) w ostatniej chwili blokowali mu miejsce na ułożenie siódemek takich jak <span class="slowa">ATEIZMU</span> czy <span class="slowa">HEREZJE</span>.<br />
Po „Turnieju Frustratów” (listopad '97) Paweł relacjonował przy piwie, rozegraną kilka godzin wcześniej rekordową partię, w której osiągnął aż 637 punktów.<br />
By udowodnić, że nie był to ewidentny samograj, zapisał na serwetce zestaw liter, jaki dostał na początku partii i podał pozostałym biesiadnikom ze słowami:<br />
- Chyba ze trzy minuty myślałem, zanim znalazłem z tego premię! Znajdziecie?<br />
Serwetkę wziął Darek Banaszek, popatrzył na zapisane literki (<span class="slowa">A E I S T Y *</span>), pomyślał chwilę, pokiwał ze zrozumieniem głową i powiedział:<br />
- No tak. Miałeś oczywiste <span class="slowa">ATEISTY</span>. Nareszcie, co?<br />
Ale Pawła jakby piorun strzelił, wyrwał serwetkę, wpatrywał się w nią dłuższy czas z niedowierzaniem, po czym zrezygnowany mruknął:<br />
- Nie. Położyłem NIESYTA.

<h2>Nie ze mną te numery</h2>
W szóstej rundzie turnieju nawet najlepsi przestają kojarzyć znaczenie oczywistych słów, pojawiających się na planszy. Podczas turnieju w Chorzowie (październik '97), w ostatniej, sobotniej partii, poźniejszy zwycięzca turnieju Paweł Stanosz spotkał się z ówczesnym wicemistrzem Polski Dariuszem Putonem. Gdy na planszy pojawiło się, ułożone przez Darka słowo <span class="slowa">CIELI</span>, Paweł, zwłaszcza, że ruch był niezbyt drogi, zapisał go bez namysłu. Po chwili jednak z przerażeniem pomyślał:<br />
- O rany! Jak ja mogłem sobie pozwolić wcisnąć taki kit? Przecież CIĘLI, a nie żadne CIELI!<br />
Nie uszło natomiast jego uwadze, że <span class="slowa">CIELI</span> zaczyna się tuż za granatową, więc ewentualne przedłużenie go do <span class="slowa">ŚCIELI</span> dałoby ruch przynajmniej za 40 punktów.<br />
- Dobra, dobra. Niech no tylko spróbuje mi tam wcisnąć to Ś, to mu ze słownikiem w ręku wyjaśnię, jak się pisze <span class="slowa">ŚCIĘLI</span>! A może to <span class="slowa">CIELI</span> się przedłuża? Ale jak? Przecież nie przez <span class="slowa">WCIELI</span>.<br />
Złośliwy los przydzielił jednak literkę Ś Pawłowi, stawiając go przed iście szekspirowskim dylematem: kłaść czy nie kłaść?<br />
- Może on naprawdę nie wie, jak się to pisze? Może warto zaryzykować? Ale nie, za dobry jest. Specjalnie mnie podpuścił, żeby teraz złapać na przedłużce. A jeśli nawet położył <span class="slowa">CIELI</span> w jakimś pijanym widzie, to na pewno już się zorientował, że to bzdura, zdejmie mi to <span class="slowa">ŚCIELI</span> i wstyd będzie podwójny.<br />
Po tych przemyśleniach wykorzystał Ś na znacznie tańszy ruch w innym miejscu planszy.<br />
Dopiero przed ostatnim ruchem Paweł, głowiąc się nad tym, gdzie położyć posiadaną na stojaku siódemkę BORDOWA, doznał olśnienia i zakończył partię ruchem <span class="slowa">BORDOWA/OCIELI</span>.

<h2>Kisiel pocieszyciel</h2>
Ośmioletni Krzyś Usakiewicz, znany też jako „Kisiel”, pokonał w Chorzowie (październik '97) kilkakrotnie starszą od siebie przeciwniczkę, która po zakończeniu partii z markotną miną pozostała przy planszy. W końcu porażka z tak młodym graczem, choćby bardzo utalentowanym, to żaden powód do dumy.<br />
Zauważywszy to i zapewne chcąc ją jakoś pocieszyć, „Kisiel” na odchodne odezwał się w te słowa:<br />
- Niech się pani nie martwi, wygrywałem już z lepszymi od pani!

<h2>Materiał szkoleniowy</h2>
W czwartej rundzie turnieju w Łodzi (wrzesień '97) przy pierwszym stole spotkali się Tomasz Zwoliński i Maciek Czupryniak. Przed rozpoczęciem partii do stolika podszedł jeden z debiutantów i zapytał, czy może się poprzyglądać.<br />
- Ależ oczywiście! A co, przeciwnik uciekł?<br />
- Nie, teraz mam pauzę — stało się jasne, że zajmuje on po trzech partiach jedno z ostatnich miejsc — więc chciałem popatrzeć, jak grają najlepsi.<br />
Mile to połechtało obu graczy, zwłaszcza Maćka — rzadko grywającego przy pierwszym stole, więc się sprężyli i przystąpili do gry. I co zobaczył łaknący nauki debiutant?<br />
Pierwszy ruch Tomka — <span class="slowa">WYZ</span> — 8 pkt. Pierwszy ruch Maćka — <span class="slowa">RYS</span> — 4 pkt. Drugi ruch Tomka — strata (<span class="slowa">WYZĘ</span>). Drugi ruch Maćka — wymiana. Stan po dwóch kolejkach 8:4 dla mistrza Polski.<br />
Z coraz większym zdumieniem debiutant wpatrywał się to w jeden, to w drugi stojak (oba z niezłymi literami), a jego w jego coraz większych i coraz bardziej okrągłych oczach widać było jeden wielki znak zapytania. To tak się gra na pierwszym stole?<br />
<i>P.S. Trzeba jednak odnotować, że później partia się rozkręciła, a debiutant w następnej rundzie odniósł pierwsze zwycięstwo.</i>

<h2>Tajemnicze słowo</h2>
Niektórym kojarzenie znaczenia słów zaczyna szwankować już podczas pierwszej partii. W pierwszej rundzie turnieju w Olsztynie (sierpień '97) doświadczona skrablistka Małgorzata Kania spotkała się z nastoletnim debiutantem.<br />
W pewnej chwili młody skrablista ułożył na planszy słowo <span class="slowa">KLIK</span>. Małgorzata, chcąc już sprawdzać słowo, pouczyła przeciwnika:<br />
- Na Twoim miejscu bym nie ryzykowała. To przecież nowe, komputerowe słowo, może go jeszcze nie być w słownikach.<br />
- Ale... ja chciałem... to miało być od „klika”... „tych klik” — wybąkał speszony debiutant.

<h2>Wielki eksperymentator</h2>
Podczas jednego z turniejów rozgrywanych w ramach wczasów skrablowych nad jeziorem Sudomie (czerwiec '97) doszło do pojedynku między dwoma czołowymi skrablistami Zdzisławem Szotą i Pawłem Stanoszem. Zdzicho wyraźnie miał „swój dzień”, więc dostawszy na początku zestaw literek <span class="slowa">ACDEWYZ</span>, postanowił zacząć od mocnego uderzenia i położył na planszy siódemkę <span class="slowa">WYCEDZA</span>. Paweł, rzecz jasna, sprawdził i okazało się, że — ku rozżaleniu Zdzicha — czasownika „wycedzać” nie ma w słownikach.<br />
Zdzicho zdjął więc <span class="slowa">WYCEDZA</span>, a gdy Paweł położył <span class="slowa">GILZIE</span>, pomieszał literki na stojaku i, śmiejąc się, pokazał grającym przy sąsiednich stolikach przygotowane do ułożenia słowo <span class="slowa">WYGDACZE</span>. Wszyscy uznali to za dobry dowcip, ale Zdzichowi w miarę wpatrywania się w stojak słowo wydawało się coraz bardziej sensowne. I w końcu je położył!<br />
Paweł aż parsknął śmiechem i ze słowami „Zdzichu, przestań się wygłupiać!” zakwestionował je. Grających obok dobiegły jeszcze słowa udającego się w stronę słowników Zdzicha: „Jeśli można coś wykrakać, to czemu nie można wygdakać? O! Na przykład kura może wygdakać jajko!”.<br />
Niestety, twórcy słowników nie mieli tak bujnej wyobraźni jak Zdzicho, który, choć znów zanotował stratę, nie poddał się. Gdy przyszła kolej na jego trzeci ruch, dojrzał na planszy wolną literę S, pomieszał literki na stojaku i położył kolejną premię <span class="slowa">DYSZAWCE</span>.<br />
- A to co znowu? — spytał Paweł.<br />
- No co? Jest „zadyszka”, to powinna być i „dyszawka”. — z wiarą w głosie odparł Zdzicho.<br />
W tym momencie ocknął się grający obok Andrzej Lożyński:<br />
- Dyszawce? Nie ma co sprawdzać — to takie ryby.<br />
- Jakie ryby? — zainteresowało się już kilka stolików.<br />
- No, nie wiecie? Takie... działające na zasadzie odrzutu... — wyjaśnił prezes PFS.<br />
- Sprawdzam! — uciął Paweł i poszedł ze Zdzichem do słowników.<br />
Niestety, autorzy słowników po raz kolejny okazali się ignorantami — trzecia strata Zdzicha.<br />
Natomiast Paweł po swoim kolejnym ruchu zakomunikował sąsiednim stolikom:<br />
- Wystawiłem mu Ń tak, że może od niego zacząć. Ciekawe, co wymyśli?<br />
Jednak Zdzicho zawiódł oczekiwania wszystkich i położył w końcu normalne słowo. A brzmiało ono: <span class="slowa">DZIWACY</span>.

<h2>Uprzejmość mistrza</h2>
Podczas I Mistrzostw Kaszub w Kościerzynie (czerwiec '97) Tomasz Zwoliński w pierwszej rundzie spotkał się z graczem, biorącym pierwszy raz udział w turnieju Scrabble. Debiutant, jak to debiutant, miał jeszcze kłopoty ze słowami, które dla doświadczonych skrablistów są oczywiste, więc gdy na planszy pojawiła się, ułożona przez mistrza, trzyliterówka III, zapytał:<br />
- Jest takie coś? Co to znaczy?<br />
- To jest taki wykrzyknik, oznaczający lekceważenie. Można go na przykład użyć w zwrocie „Iii, co z niego za gracz” . — wyjaśnił uprzejmie trzykrotny mistrz Polski.

<h2>Między teorią a praktyką</h2>
Podczas turnieju w Opolu (maj '97) jeden ze skrablistów ze ścisłej krajowej czołówki po przerwie przeznaczonej na posiłek pożalił się organizatorom:<br />
- <b>Poszłem</b> w lewo tak, jak mówiliście, ale tam nie było nic do jedzenia!<br />
Nikt ze słyszących te słowa nie miał najmniejszych wątpliwości, że ów gracz sam nie położyłby na planszy słowa <span class="slowa">POSZŁEM</span>, a każdemu przeciwnikowi bezwzględnie by je zdjął.

<h2>Skromność mistrza</h2>
Po drugiej rundzie turnieju w Poznaniu (maj '97) trzykrotny wówczas mistrz Polski Tomasz Zwoliński z niezbyt szczęśliwą miną wyszedł z sali, gdzie toczyły się rozgrywki.<br />
- Co Tomku? Jak poszło? — posypały się pytania.<br />
- Hm, przeciwnik był dobry, a mnie w ogóle nie szły literki... — zaczął się tłumaczyć mistrz.<br />
- I co, i co? — sensacja zawisła w powietrzu.<br />
- No cóż, wygrałem ze 150 punktów.<br />

<?require_once "files/php/bottom.php"?>
</body>
</html>

